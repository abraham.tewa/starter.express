/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable import/no-extraneous-dependencies */
const nxPreset = require('@nx/jest/preset').default;

const rootDir = __dirname;

const config = {
  ...nxPreset,
  collectCoverage: true,
  coverageDirectory: `${rootDir}/reports/tests/coverage`,
  coverageReporters: [
    'cobertura',
    'json',
    'lcov',
    'text',
    'text-summary',
  ],
  modulePathIgnorePatterns: [
    'build/',
  ],
  reporters: [
    'default',
    ['jest-junit',
      {
        outputDirectory: `${rootDir}/reports/tests/unit`,
        outputName: 'junit.xml',
      }],
    ['jest-html-reporters',
      {
        filename: 'index.html',
        publicPath: `${rootDir}/reports/tests/unit`,
      }],
  ],
  rootDir: __dirname,
  testEnvironment: 'node',
  verbose: true,
};

// ============================================================
// Exports
module.exports = config;
