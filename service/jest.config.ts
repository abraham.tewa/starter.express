import type { Config } from 'jest';

const config: Config = {
  displayName: '@process/service',
  preset: '../jest.config.base.js',
  setupFiles: ['./src/testTools/setup.ts'],
};

export default config;
