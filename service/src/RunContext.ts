// ============================================================
// Imports
import { MongoDbContext } from '@internal/runtime';
import type { log } from '@internal/runtime';
import { type DbContext, type LogCategory } from './types/service';
import type { Config } from './config';
import type ServiceRuntime from './ServiceRuntime';

// ============================================================
// Class
class RunContext {
  readonly db: DbContext;

  readonly logger: log.Logger<LogCategory>;

  readonly runtime: ServiceRuntime;

  static #latestId: 0;

  constructor(runtime: ServiceRuntime, db: DbContext, extraLogTag?: log.Tag) {
    this.runtime = runtime;
    this.db = db;

    RunContext.#latestId += 1;
    const id = RunContext.#latestId;

    this.logger = this.runtime.logger.child({
      ...extraLogTag,
      id,
    });
  }

  /**
   * Parameter: session
   *  if "keep":
   *    Keep the same session as the current one.
   *    If current context doesn't have a session, then no session is created.
   *
   *  if "new":
   *    Create a new session, regardless the existing parent session
   *
   *  if "ensure":
   *    Reuse the parent session if exists or create a new one
   */
  child(
    newDbSession: 'keep' | 'new' | 'ensure' = 'keep',
    extraLogTags?: log.Tag,
  ) {
    const context = new RunContext(
      this.runtime,
      this.db.child(newDbSession, extraLogTags),
      extraLogTags,
    );

    return context;
  }

  static async createContext(runtime: ServiceRuntime, config: Config) {
    const logger = runtime.logger.child({
      id: this.#latestId,
      type: 'dbContext',
    });

    const dbContext = await MongoDbContext.create<LogCategory>(
      config.db,
      logger,
    );

    return new RunContext(runtime, dbContext);
  }
}

// ============================================================
// Exports
export default RunContext;
