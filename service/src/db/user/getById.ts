// ============================================================
// Imports
import { ObjectId } from 'mongodb';

import { getCollection } from './utils';
import type {
  DbContext,
  UserWithPasswordDocument,
} from '../../types';

// ============================================================
// Functions
async function getById(
  dbContext: DbContext,
  id: ObjectId | string,
): Promise<UserWithPasswordDocument | null> {
  const collection = getCollection(dbContext);

  const user = await collection.findOne({
    _id: typeof id === 'string' ? new ObjectId(id) : id,
  });

  if (!user) {
    return null;
  }

  return user;
}

// ============================================================
// Exports
export default getById;
