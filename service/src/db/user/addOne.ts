// ============================================================
// Imports
import { utils } from '@internal/helpers';
import { ObjectId } from 'mongodb';
import { getCollection } from './utils';

import {
  type DbContext,
  LogCategory,
  UserAccountStatus,
  type UserRole,
  type UserWithPasswordDocument,
} from '../../types';

// ============================================================
// Functions
async function addOne(
  dbContext: DbContext,
  user: UserCreate,
): Promise<UserWithPasswordDocument> {
  const authInfo = await utils.auth.generateAuthInfo(user.auth.password);

  const doc: UserWithPasswordDocument = {
    _id: new ObjectId(),
    account: {
      creationDate: new Date(),
      email: user.account.email,
      status: UserAccountStatus.created,
    },
    auth: {
      algorithm: authInfo.algorithm,
      nbFailedAttempts: 0,
      passwordHash: authInfo.passwordHash,
      passwordSalt: authInfo.passwordSalt,
    },
    profile: {
      firstName: user.profile.firstName,
      lastName: user.profile.lastName,
    },
    roles: user.roles,
  };

  const collection = getCollection(dbContext);
  const { acknowledged } = await collection.insertOne(doc, {
    session: dbContext.session,
  });

  if (!acknowledged) {
    throw dbContext.logger.error({
      category: LogCategory.user,
      label: 'Error while create user',
      message: 'collection.insertOne failed',
      moreInfo: { acknowledged },
    });
  }

  return doc;
}

// ============================================================
// Types
type UserCreate = {
  account: {
    email: string;
    emailValidationToken: string,
  },
  auth: {
    password: string;
  };
  profile: {
    firstName: string;
    lastName: string;
  };
  roles: UserRole[],
};

// ============================================================
// Exports
export default addOne;
