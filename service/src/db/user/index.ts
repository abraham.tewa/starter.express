export { default as addOne } from './addOne';
export { default as delete } from './delete';
export { default as getById } from './getById';
export { default as getByLogin } from './getByLogin';
export { default as list } from './list';
export { default as update } from './update';
