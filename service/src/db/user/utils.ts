// ============================================================
// Imports
import { type Collection as DbCollection } from 'mongodb';

import { Collection } from '../../enum';
import type {
  DbContext,
  UserWithPasswordDocument,
} from '../../types';

// ============================================================
// Functions
function getCollection({
  client,
}: DbContext): DbCollection<UserWithPasswordDocument> {
  return client.db().collection(Collection.users);
}

// ============================================================
// Exports
export { getCollection };
