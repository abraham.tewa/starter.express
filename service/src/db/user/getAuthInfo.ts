// ============================================================
// Imports
import { ObjectId } from 'mongodb';

import { getCollection } from './utils';
import type {
  DbContext,
  UserAuthInfo,
} from '../../types';

// ============================================================
// Functions
async function getAuthInfo(
  dbContext: DbContext,
  id: ObjectId | string,
): Promise<UserAuthInfo | null> {
  const collection = getCollection(dbContext);

  const data = await collection.findOne({
    _id: typeof id === 'string' ? new ObjectId(id) : id,
  });

  return data?.auth ?? null;
}

// ============================================================
// Exports
export default getAuthInfo;
