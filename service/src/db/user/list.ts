// ============================================================
// Imports
import type { Filter, FindCursor } from 'mongodb';
import { getCollection } from './utils';
import type {
  DbContext,
  UserWithPasswordDocument,
} from '../../types';

// ============================================================
// FUnctions
async function list(
  dbContext: DbContext,
  query: Query,
  skip?: number,
): Promise<FindCursor<UserWithPasswordDocument>> {
  const collection = getCollection(dbContext);

  const cursor = collection.find(query as Filter<UserWithPasswordDocument>, {
    session: dbContext.session,
    skip,
  }) as FindCursor;

  return cursor;
}

// ============================================================
// Types
type Query = Filter<Omit<UserWithPasswordDocument, '_rev'>>;

// ============================================================
// Exports
export default list;
export type { Query };
