// ============================================================
// Imports
import { getCollection } from './utils';
import type {
  DbContext,
  UserWithPasswordDocument,
} from '../../types';

// ============================================================
// Functions
async function getByLogin(
  dbContext: DbContext,
  email: string,
): Promise<UserWithPasswordDocument | null> {
  const collection = getCollection(dbContext);

  const user = await collection.findOne({
    'account.email': email,
  });

  return user ?? null;
}

// ============================================================
// Exports
export default getByLogin;
