// ============================================================
// Imports
import { ObjectId } from 'mongodb';

import { getCollection } from './utils';
import type {
  DbContext,
} from '../../types/service';

// ============================================================
// Functions
async function deleteUser(
  dbContext: DbContext,
  id: string | ObjectId,
): Promise<number> {
  const collection = getCollection(dbContext);

  const { deletedCount } = await collection.deleteOne(
    {
      _id: new ObjectId(id),
    },
    {
      session: dbContext.session,
    },
  );

  return deletedCount;
}

// ============================================================
// Exports
export default deleteUser;
