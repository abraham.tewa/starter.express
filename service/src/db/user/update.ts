// ============================================================
// Imports
import { utils } from '@internal/helpers';
import {
  type ObjectId,
  type UpdateFilter,
} from 'mongodb';

import { getCollection } from './utils';
import type {
  DbContext,
  UserWithPasswordDocument,
} from '../../types';

// ============================================================
// Functions
async function update(
  parentContext: DbContext,
  docId: string | ObjectId,
  updateFilter: UpdateFilter<UserWithPasswordDocument>,
  options: {
    returnDocument: 'before' | 'after',
  } = { returnDocument: 'before' },
): Promise<UserWithPasswordDocument | undefined> {
  const dbContext = parentContext.withinSession();

  const objectId = utils.mongodb.toObjectId(docId);

  const collection = getCollection(dbContext);

  const updated = await collection.findOneAndUpdate(
    {
      _id: objectId,
    },
    updateFilter,
    {
      returnDocument: options.returnDocument,
      session: dbContext.session,
    },
  );

  return updated ?? undefined;
}

type UserUpdate = UpdateFilter<UserWithPasswordDocument>;

// ============================================================
// Exports
export default update;
export type {
  UserUpdate,
};
