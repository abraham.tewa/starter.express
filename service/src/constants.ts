// ============================================================
// Imports
import path from 'node:path';
import { utils } from '@internal/helpers';
import type { MetaInfo } from '@internal/runtime';

// ============================================================
// Module's constants and variables
const MIN_PASSWORD_LENGTH = 8;
const MAX_PASSWORD_LENGTH = 1024;
const MAX_AUTHENTICATION_ATTEMPT_ALLOWED = 5;

const USER_ACCOUNT_LOCK_DURATION = 1 * 60 * 60 * 1000;

const PROJECT_ROOT = utils.fs.findUpSync(
  __dirname,
  'package.json',
  'directory',
) as string;

const CONFIG_DIR = path.resolve(PROJECT_ROOT, 'config');

const META_INFO: MetaInfo = {
  service: {
    name: '@service/workspace',
    version: '0.1.0',
  },
};

// ============================================================
// Exports
export {
  CONFIG_DIR,
  MAX_AUTHENTICATION_ATTEMPT_ALLOWED,
  MAX_PASSWORD_LENGTH,
  META_INFO,
  MIN_PASSWORD_LENGTH,
  PROJECT_ROOT,
  USER_ACCOUNT_LOCK_DURATION,
};
