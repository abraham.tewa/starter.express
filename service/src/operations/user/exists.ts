// ============================================================
// Imports
import type RunContext from 'service/src/RunContext';
import * as db from '../../db';

// ============================================================
// Functions
async function exists(
  ctx: RunContext,
  email: string,
): Promise<boolean> {
  const user = await db.user.getByLogin(
    ctx.db,
    email,
  );

  return Boolean(user);
}

// ============================================================
// Exports
export default exists;
