// ============================================================
// Imports
import { utils } from '@internal/helpers';
import { LogCategory } from '@process/service';
import { EmailAlreadyUsedError } from './errors';
import exists from './exists';
import * as db from '../../db';
import type RunContext from '../../RunContext';
import type {
  UserRole,
  UserWithPasswordDocument,
} from '../../types';

// ============================================================
// Functions
async function create(
  ctx: RunContext,
  userToCreate: UserCreate,
): Promise<UserWithPasswordDocument> {
  if (await exists(ctx, userToCreate.account.email)) {
    throw new EmailAlreadyUsedError();
  }

  const validationToken = utils.string.randomString(60);

  const user = await db.user.addOne(
    ctx.db,
    {
      ...userToCreate,
      account: {
        ...userToCreate.account,
        emailValidationToken: validationToken,
      },
    },
  );

  ctx.logger.success({
    category: LogCategory.user,
    label: 'User creation',
    message: `User "${user._id.toHexString()}" created`,
  });

  return user;
}

// ============================================================
// Types
type UserCreate = {
  account: {
    email: string;
  },
  auth: {
    password: string;
  };
  profile: {
    firstName: string;
    lastName: string;
  };
  roles: UserRole[],
};

// ============================================================
// Exports
export default create;
export {
  EmailAlreadyUsedError as EmailAlreadyUsed,
};
export type {
  UserCreate,
};
