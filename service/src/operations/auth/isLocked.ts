// ============================================================
// Imports
import type { UserWithPasswordDocument } from '../../types';

// ============================================================
// Functions
function isLocked(
  user: UserWithPasswordDocument,
): boolean {
  if (!user.account.lock) {
    return false;
  }

  return user.account.lock.until > new Date();
}

// ============================================================
// Exports
export default isLocked;
