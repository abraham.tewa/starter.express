import {
  MAX_PASSWORD_LENGTH,
  MIN_PASSWORD_LENGTH,
} from '../../constants';

function validatePasswordFormat(password: string): string[] {
  const errors: string[] = [];

  if (password.length < MIN_PASSWORD_LENGTH) {
    errors.push(`Password too short (min: ${MIN_PASSWORD_LENGTH} characters)`);
  }

  if (password.length > MAX_PASSWORD_LENGTH) {
    errors.push(`Password too long (max: ${MAX_PASSWORD_LENGTH} characters)`);
  }

  if (/^\s/.test(password)) {
    errors.push('Password cannot starts with a space character');
  }

  if (/\s$/.test(password)) {
    errors.push('Password cannot ends with a space character');
  }

  if (!/\d/.test(password)) {
    errors.push('Password must contains at least one number');
  }

  if (!/[a-z]/.test(password)) {
    errors.push('Password must contains at least one lowercase character');
  }

  if (!/[A-Z]/.test(password)) {
    errors.push('Password must contains at least one uppercase character');
  }

  if (!/[^a-zA-Z\d]/.test(password)) {
    errors.push('Password must contains at least one special character');
  }

  return errors;
}

export default validatePasswordFormat;
