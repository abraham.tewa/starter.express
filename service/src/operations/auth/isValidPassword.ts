// ============================================================
// Imports
import { utils } from '@internal/helpers';
import type { UpdateFilter } from 'mongodb';
import isLocked from './isLocked';
import {
  MAX_AUTHENTICATION_ATTEMPT_ALLOWED,
  USER_ACCOUNT_LOCK_DURATION,
} from '../../constants';
import * as db from '../../db';
import {
  UserAccountLockReason,
  type UserWithPasswordDocument,
} from '../../types';
import type RunContext from '../../RunContext';

// ============================================================
// Functions
async function isValidPassword(
  ctx: RunContext,
  login: string,
  password: string,
): Promise<[true, UserWithPasswordDocument] | [false, UserWithPasswordDocument | undefined]> {
  const user = await db.user.getByLogin(ctx.db, login);

  if (!user) {
    return [false, undefined];
  }

  if (isLocked(user)) {
    return [false, user];
  }

  const isValid = await utils.auth.isValidPassword(
    password,
    user.auth,
  );

  let updateQuery: UpdateFilter<UserWithPasswordDocument>;

  if (isValid) {
    updateQuery = {
      $set: {
        'auth.nbFailedAttempts': 0,
      },
    };
  } else {
    updateQuery = {
      $inc: {
        'auth.nbFailedAttempts': 1,
      },
    };

    if (user.auth.nbFailedAttempts > MAX_AUTHENTICATION_ATTEMPT_ALLOWED - 1) {
      const since = new Date();

      updateQuery.$set = {
        'account.lock': {
          reason: UserAccountLockReason.toManyAuthFailed,
          since,
          until: new Date(since.getTime() + USER_ACCOUNT_LOCK_DURATION),
        },
      };
    }
  }

  const updatedUser = await db.user.update(
    ctx.db,
    user._id,
    updateQuery,
  ) as UserWithPasswordDocument;

  return [isValid, updatedUser];
}

// ============================================================
// Exports
export default isValidPassword;
