import type RunContext from '../RunContext';

async function checkUser(context: RunContext, userId: string) {
  return Boolean(context) && Boolean(userId);
}

export default checkUser;
