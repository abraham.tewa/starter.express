/* eslint-disable import/first */
// ============================================================
// Imports
import { utils } from '@internal/helpers';
import type { DeepPartial } from '@internal/helpers';

if (!process.env.NODE_CONFIG_DIR) {
  process.env.NODE_CONFIG_DIR = utils.project.path('config');
}

import nodeConfig from 'config';

import schema from './schema';
import { TasksOperation } from './tasks';
import { DevTools } from './types';
import type {
  AcceptableConfig,
  Config,
  JobQueuesConfig,
} from './types';

// ============================================================
// Functions
function getConfig(
  configDir: string,
  env?: string,
  ...configOverrides: DeepPartial<AcceptableConfig>[]
): Config {
  const loadedConfig = loadConfig(
    configDir,
    env,
  );

  const allConfigs = [loadedConfig, ...configOverrides].map(toDeepPartialConfig);

  const config = mergeConfig(...allConfigs);

  const validatedConfig = schema.validate(config);

  if (validatedConfig.error) {
    throw validatedConfig.error;
  }

  return validatedConfig.value;
}

function loadConfig(
  configDir: string,
  env?: string,
) {
  const prevEnv = process.env.NODE_CONFIG_ENV;
  process.env.NODE_CONFIG_ENV = env ?? prevEnv;

  const loadedConfig = nodeConfig.util.loadFileConfigs(configDir) as AcceptableConfig;

  process.env.NODE_CONFIG_ENV = prevEnv;

  return loadedConfig;
}

function toDeepPartialConfig(
  { devTools, ...config }: DeepPartial<AcceptableConfig>,
): DeepPartial<Config> {
  const newConfig: DeepPartial<Config> = {
    ...config,
  };

  if (devTools === '*') {
    newConfig.devTools = Object.values(DevTools);
  } else if (devTools === 'none') {
    newConfig.devTools = [];
  } else if (typeof devTools === 'string') {
    newConfig.devTools = [devTools];
  }

  if (typeof newConfig.jobsQueues === 'undefined') {
    newConfig.jobsQueues = Object.fromEntries(
      Object
        .values(TasksOperation)
        .map((operation) => [
          operation,
          {
            workers: 0,
          } as JobQueuesConfig,
        ]),
    ) as Record<TasksOperation, JobQueuesConfig>;
  }

  return newConfig;
}

function mergeConfig(
  ...overrides: DeepPartial<Config>[]
): DeepPartial<Config> {
  const newConfig: Config = nodeConfig.util.extendDeep(
    {},
    ...overrides,
  ) as Config;

  return newConfig;
}

function validateConfig(config: Config) {
  const { error } = schema.validate(
    config,
    {
      allowUnknown: false,
      convert: false,
      dateFormat: 'iso',
    },
  );

  if (error) {
    throw error;
  }
}

// ============================================================
// Exports
export {
  getConfig,
  mergeConfig,
  validateConfig,
};
