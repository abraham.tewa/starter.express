// ============================================================
// Imports
import { utils } from '@internal/helpers';
import type { Config } from './types';

// ============================================================
// Functions
async function areServiceAccessible(
  config: Config,
) {
  const [
    amqp,
    db,
    session,
    tasks,
  ] = await Promise.all([
    utils.amqp.isAccessible(config.amqp),
    utils.mongodb.isAccessible(config.db),
    utils.redis.isAccessible(config.session.store),
    utils.redis.isAccessible(config.tasks.store),
  ]);

  return {
    amqp,
    db,
    session,
    tasks,
  };
}

// ============================================================
// Exports
export default areServiceAccessible;
