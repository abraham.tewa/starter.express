// ============================================================
// Imports
import type { utils } from '@internal/helpers';
import type {
  SessionConfig,
  TaskConfig,
  log,
} from '@internal/runtime';
import type { TasksOperation } from './tasks';

// ============================================================
// Types
enum DevTools {
  dashboard = 'dashboard',
  documentation = 'documentation',
  openapi = 'openapi',
  taskAdmin = 'taskAdmin',
}

type Config = {
  amqp: utils.amqp.ConnectInfo & {
    queues: {
      user: string,
    }
  },
  db: utils.mongodb.ConnectInfo,
  /**
   * Should the server expose toolings pages ?
   */
  devTools: DevTools[],
  host: string,
  jobsQueues: Record<TasksOperation, JobQueuesConfig>,
  log: log.Config,
  port: number,
  production: boolean,
  session: SessionConfig,
  tasks: TaskConfig,
};

type AcceptableConfig = Omit<Config, 'devTools'> & {
  devTools: DevTools[] | '*' | 'none'
};

type RedisConnectInfo = {
  host: string,
  port: number,
};

type JobQueuesConfig = {
  workers: number,
};

// ============================================================
// Exports
export type {
  AcceptableConfig,
  Config,
  JobQueuesConfig,
  RedisConnectInfo,
};

export {
  DevTools,
};
