// ============================================================
// Imports
import { utils } from '@internal/helpers';
import { SessionConfigSchema, TaskConfigSchema, log } from '@internal/runtime';
import Joi from 'joi';

import { TasksOperation } from './tasks';
import {
  type Config,
  DevTools,
  type JobQueuesConfig,
} from './types';

// ============================================================
// Schema
const jobQueuesEntries: [string, Joi.ObjectSchema][] = Object
  .values(TasksOperation)
  .map((name) => [
    name,
    Joi.object<JobQueuesConfig>({
      workers: Joi.number().integer().min(0).default(0),
    }),
  ]);

const schema = Joi.object<Config>({
  amqp: utils.amqp.Schema.required().append({
    queues: Joi.object<Config['amqp']['queues']>({
      user: Joi.string().required(),
    }).required(),
  }),
  db: utils.mongodb.Schema.required(),
  devTools: Joi.alternatives(
    Joi.array().items(
      Joi.string().allow(...Object.values(DevTools)),
    ),
    Joi.string().allow('*', 'none'),
  ).optional(),
  host: Joi.alternatives().try(
    Joi.string().hostname(),
    Joi.string().ip(),
  ).required(),
  jobsQueues: Joi.object<Config['jobsQueues']>(
    Object.fromEntries(jobQueuesEntries),
  ),
  log: Joi.object<Config['log']>({
    format: Joi.string().allow(...Object.values(log.Format)),
    level: Joi.string().allow(...Object.values(log.Level)),
    output: Joi.string().allow(...Object.values(log.Output)),
  }).required(),
  port: Joi.number().port(),

  production: Joi.boolean(),

  session: SessionConfigSchema.required(),

  tasks: TaskConfigSchema.required(),
});

// ============================================================
// Exports
export default schema;
