// ============================================================
// Imports
import {
  type DeepPartial,
  utils,
} from '@internal/helpers';
import {
  LogLevel,
  type ProcControl,
  type ProcManager,
} from '@internal/runtime';

import { Command } from 'commander';
import type { ChildProcessWithoutNullStreams } from 'child_process';
import { initialize } from './utils';
import { TasksOperation } from '../../config';
import { DevTools } from '../../config/types';
import { LogCategory } from '../../types';
import type { Config } from '../../config';
import type ServiceRuntime from '../../ServiceRuntime';

// ============================================================
// Module's constants and variables
const STARTUP_MESSAGE = 'Server started successfully!';

const commandName = 'start';

// ============================================================
// Function
function makeCommand() {
  const program = new Command('start');

  program
    .description('Start the service')
    .argument('[host]', 'Hostname to server')
    .argument('[port]', 'Port to server')
    .option('-e, --env, --environment <env>', 'environment name (default: NODE_CONFIG_ENV')
    .option('-c, --configDir <configDir>', 'directory where to find config files')
    .action(async (...args: CliArgs) => {
      await action(args);
    });

  return program;
}

async function action(
  [argHost, argPort, options]: CliArgs,
  configOverride: DeepPartial<Config> = {},
): Promise<ProcControl> {
  if (options.env) {
    process.env.NODE_CONFIG_ENV = options.env;
  }

  const argConfigOverride: DeepPartial<Config> = {
    ...configOverride,
  };
  const port = argPort && !Number.isNaN(Number(argPort)) ? Number(argPort) : undefined;

  if (argHost) {
    argConfigOverride.host = argHost;
  }

  if (argPort) {
    const numPort = Number(argPort);

    if (Number.isNaN(numPort) || !Number.isInteger(numPort) || numPort <= 0) {
      throw new Error(`Invalid port number: ${argPort}`);
    }

    argConfigOverride.port = port;
  }

  const [runtime, config] = await initialize(
    'start',
    options,
    argConfigOverride,
  );

  await runtime.httpServer.start();

  runtime.task.startWorkers(
    TasksOperation.check,
    config.jobsQueues[TasksOperation.check].workers,
  );

  const serverUrl = `http://${runtime.httpServer.host}:${runtime.httpServer.port}`;

  await displayServicesInformation(config, runtime);

  displayDevToolsInfo({
    config,
    runtime,
    serverUrl,
  });

  runtime.logger.info({
    category: LogCategory.cli,
    label: 'start',
    message: STARTUP_MESSAGE,
  });

  runtime.logger.success({
    category: LogCategory.cli,
    label: 'start',
    message: `Server: ${serverUrl}`,
  });

  return {
    endPromise: runtime.endPromise,
    kill: async () => {
      await runtime.stop();
    },

    stop: async () => {
      await runtime.stop();
    },
  };
}

async function displayServicesInformation(
  config: Config,
  runtime: ServiceRuntime,
) {
  const accesses = await runtime.serviceAccessibility();

  let level: LogLevel;
  let message: string;
  let connectString: string;

  // MongoDB
  level = accesses.db ? LogLevel.info : LogLevel.error;
  message = accesses.db
    ? 'MongoDB accessible'
    : 'MongoDB not accessible';

  connectString = utils.mongodb.getConnectString(config.db);

  runtime.logger.log({
    category: LogCategory.cli,
    label: 'start',
    level,
    message: `${message}: ${connectString}`,
  });

  // Task's redis
  level = accesses.tasks ? LogLevel.info : LogLevel.error;
  connectString = utils.redis.getConnectString(config.tasks.store);
  message = accesses.tasks
    ? 'Task\'s redis accessible'
    : 'Task\'s redis not accessible';

  runtime.logger.log({
    category: LogCategory.cli,
    label: 'start',
    level,
    message: `${message}: ${connectString})`,
  });
}

function displayDevToolsInfo({
  config,
  runtime,
  serverUrl,
}: {
  config: Config,
  runtime: ServiceRuntime,
  serverUrl: string,
}): void {
  const devToolsEnabled = config.devTools.length > 0;

  if (!devToolsEnabled) {
    runtime.logger.info({
      category: LogCategory.cli,
      label: 'start',
      message: 'All devTools disabled',
    });
    return;
  }

  const dashboard = config.devTools.includes(DevTools.dashboard)
    ? `${serverUrl}${runtime.toolingRoutes.dashboard}`
    : 'disabled';

  const taskAdmin = config.devTools.includes(DevTools.taskAdmin)
    ? `${serverUrl}${runtime.toolingRoutes.taskAdmin}`
    : 'disabled';

  const documentation = config.devTools.includes(DevTools.documentation)
    ? `${serverUrl}${runtime.toolingRoutes.documentation}`
    : 'disabled';

  const openapi = config.devTools.includes(DevTools.openapi)
    ? `    UI  : ${serverUrl}${runtime.toolingRoutes.openapi.ui}
    json: ${serverUrl}${runtime.toolingRoutes.openapi.json}
    yml : ${serverUrl}${runtime.toolingRoutes.openapi.yaml}`
    : 'disabled';

  const devToolsMessage = `DevTools enabled:
  Documentation: ${documentation}
  Dashboard:     ${dashboard}
  Task Admin:    ${taskAdmin}
  OpenAPI:\n${openapi}
`;

  runtime.logger.info({
    category: LogCategory.cli,
    label: 'start',
    message: devToolsMessage,
  });
}

const procManager: ProcManager = {
  async detectEndInitialization(p: ChildProcessWithoutNullStreams) {
    await utils.process.waitForText(p, STARTUP_MESSAGE);
  },
  initializationTimeout: 3000,
};

// ============================================================
// Types
type CliArgs = [
  host: string | undefined,
  port: string | undefined,
  options: Options,
];

type Options = {
  configDir?: string,
  env?: string;
};

// ============================================================
// Exports
export {
  action,
  commandName as name,
  makeCommand,
  procManager,
};
