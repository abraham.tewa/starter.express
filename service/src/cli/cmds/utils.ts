// ============================================================
// Imports
import type { DeepPartial } from '@internal/helpers';
import {
  type Config,
  getConfig,
} from '../../config';
import { CONFIG_DIR } from '../../constants';
import ServiceRuntime from '../../ServiceRuntime';
import {
  LogCategory,
} from '../../types';

// ============================================================
// Functions
function configSourceToString(sources: string[]): string {
  return `Config sources:\n  - ${sources.join('\n  - ')}`;
}

async function initialize(
  commandName: string,
  options?: DefaultCliOptions,
  configOverride: DeepPartial<Config> = {},
): Promise<[ServiceRuntime, Config]> {
  // Building runtime
  const config = getConfig(
    CONFIG_DIR,
    options?.env,
    // No need to start devTools
    { devTools: 'none' },
    configOverride,
  );

  const runtime = new ServiceRuntime(config);

  runtime.logger.notice({
    category: LogCategory.cli,
    label: commandName,
    message: `Config directory: ${CONFIG_DIR}`,
  });

  await runtime.initialize();

  return [runtime, config];
}

// ============================================================
// Types
type DefaultCliOptions = {
  configDir?: string,
  env?: string,
};

// ============================================================
// Exports
export {
  configSourceToString,
  initialize,
};
