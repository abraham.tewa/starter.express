// ============================================================
// Imports
import { Command } from 'commander';
import Joi from 'joi';
import type { DeepPartial } from '@internal/helpers';

import { type Config } from '../../../config';
import * as ope from '../../../operations';
import { UserRole } from '../../../types';
import { initialize } from '../utils';

// ============================================================
// Module's constants and variables
const commandName = 'user create';

// ============================================================
// Function
function makeCommand() {
  const program = new Command('create');

  program
    .description('Create a user')
    .argument('email', 'Email of the user')
    .argument('password', 'Password of the user')
    .option('--admin', 'Create an admin user', false)
    .option('--user', 'Create an user', false)
    .option('--firstName <firstName>', 'First name of the user')
    .option('--lastName <lastName>', 'Last name of the user')
    .option('-e, --env, --environment <env>', 'config file to use')
    .option('-c, --configDir <configDir>', 'directory where to find config files')
    .action(async (...args: CliArgs) => {
      await action(args);
    });

  return program;
}

async function action(
  [email, password, options]: CliArgs,
  configOverride: DeepPartial<Config> = {},
): Promise<undefined> {
  // Parsing and checking parameters
  const emailSchema = Joi.string().email().required();

  const validation = emailSchema.validate(email);

  if (validation.error) {
    throw validation.error;
  }

  // Building runtime
  const [runtime] = await initialize(
    'user create',
    options,
    configOverride,
  );

  const context = runtime.createContext();

  // Creating user
  const roles: UserRole[] = [];

  if (options.admin) {
    roles.push(UserRole.applicationAdmin);
  }

  if (options.user) {
    roles.push(UserRole.user);
  }

  await ope.user.create(
    context,
    {
      account: {
        email,
      },
      auth: {
        password,
      },
      profile: {
        firstName: options.firstName,
        lastName: options.lastName,
      },
      roles,
    },
  );

  await runtime.stop();

  return undefined;
}

// ============================================================
// Types
type CliArgs = [
  email: string,
  password: string,
  options: Options,
];

type Options = {
  admin: boolean,
  configDir?: string,
  env?: string,
  firstName: string,
  lastName: string,
  user: boolean,
};

// ============================================================
// Exports
export {
  action,
  commandName as name,
  makeCommand,
};

export type {
  Options,
};
