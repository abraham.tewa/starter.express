// ============================================================
// Import packages
import { Command } from 'commander';

// ============================================================
// Import modules
import * as start from './start';
import * as user from './user';

// ============================================================
// Functions
function build(): Command {
  const program = new Command();

  const startCmd = start.makeCommand();
  const userCreateCmd = user.create.makeCommand();

  program.addCommand(startCmd);

  const userProgram = program.command('user');
  userProgram.addCommand(userCreateCmd);

  return program;
}

// ============================================================
// Exports
const cmds = {
  start,
  user,
};

export { build, cmds };
