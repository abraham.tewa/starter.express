type KillableCommand = {
  endPromise: Promise<void>;
  kill: () => Promise<void>;
  stop: () => Promise<void>;
};

type ActionReturn = KillableCommand | undefined;

export type { ActionReturn };
