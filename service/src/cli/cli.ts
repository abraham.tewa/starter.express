import 'dotenv/config';

import { build } from './cmds';

function start() {
  const program = build();
  program.parse();
}

start();

export {};
