enum Collection {
  tokens = 'tokens',
  users = 'users',
}

enum TaskType {
  check = 'check',
}

export { Collection, TaskType };
