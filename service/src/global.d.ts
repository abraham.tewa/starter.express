import type { SessionData as AppSessionData } from './types';

declare global {

  declare module 'express-session' {
    interface SessionData {
      number: number;
      user?: AppSessionData['user']
    }
  }
}
