// ============================================================
// Imports
import type { TasksOperation } from '../config';

import type * as db from '../db';

// ============================================================
// Types
type TaskData = {
  [TasksOperation.check]: {
    userId: db.DbId;
  };
};

type Tasks = {
  data: {
    userId: db.DbId;
  };
  operation: TasksOperation.check;
};

// ============================================================
// Exports
export type { TaskData, Tasks };
