// ============================================================
// Imports
import { faker } from '@faker-js/faker';
import {
  ServerClient as TestServerClient,
} from '@internal/tests';
import type { http } from '@internal/helpers';

import * as user from './user';
import type { ServerClient, TestContext } from './types';

// ============================================================
// Functions
async function initializeServer(
  ctx: TestContext,
): Promise<{
    admin: http.ApiClient,
    noAuthClient: http.ApiClient,
    server: ServerClient,
  }> {
  const admin = {
    email: faker.internet.email(),
    password: user.generatePassword(),
  };

  await ctx.cli.user.create([
    admin.email,
    admin.password,
    {
      admin: true,
      firstName: faker.person.firstName(),
      lastName: faker.person.lastName(),
      user: true,
    },
  ]);

  const { port } = ctx.env.config;
  const procControl = await ctx.cli.start(
    [
      'localhost',
      String(port),
      {
        env: ctx.env.nodeConfigEnv,
      },
    ],
  );

  const server: ServerClient = new TestServerClient(
    port,
    ctx.env.config,
    procControl,
  );

  const noAuthClient = server.createClient();

  const adminClient = await user.authenticate(
    ctx,
    noAuthClient,
    admin.email,
    admin.password,
  );

  return {
    admin: adminClient,
    noAuthClient,
    server,
  };
}

// ============================================================
// Exports
export default initializeServer;
