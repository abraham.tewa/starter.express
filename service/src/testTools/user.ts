// ============================================================
// Imports
import { faker } from '@faker-js/faker';
import { type http } from '@internal/helpers';
import { assertApi, assertNoError } from '@internal/tests';
import cookieParser from 'cookie';
import Joi from 'joi';
import { MIN_PASSWORD_LENGTH } from '../constants';

import {
  type LockInfo as UserAccountLockInfo,
  UserAccountLockReason,
  UserAccountStatus,
  UserRole,
} from '../types/users';
import type { TestContext } from './types';
import type * as authenticateUser from '../api/authenticateUser';
import type { ApiDeclaration as CreateUserApi } from '../api/createUser';
import type * as deleteUser from '../api/deleteUser';
import type * as disconnectUser from '../api/disconnectUser';
import type * as getUser from '../api/getUser';
import type { ApiUser } from '../api/types';
import type { UserCreate } from '../operations/user/create';

// ============================================================
// Schema
const ApiUserSchema = Joi.object<ApiUser>({
  account: Joi.object<ApiUser['account']>({
    creationDate: Joi
      .date()
      .required(),
    email: Joi
      .string()
      .required(),
    lock: Joi.object<UserAccountLockInfo>({
      reason: Joi
        .string()
        .allow(...Object.values(UserAccountLockReason))
        .required(),
      since: Joi.date().required(),
      until: Joi.string().required(),
    }),
    status: Joi
      .string()
      .allow(...Object.values(UserAccountStatus))
      .required(),
  }).required(),
  id: Joi.string().required(),
  profile: Joi.object<ApiUser['profile']>({
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
  }).required(),
  roles: Joi
    .array()
    .items(
      Joi
        .string()
        .allow(...Object.values(UserRole))
        .required(),
    ).required(),
});

// ============================================================
// Functions
async function create(
  ctx: TestContext,
  apiClient: http.ApiClient,
  user?: UserCreate,
): Promise<TestUser> {
  const userToCreate = user ?? generateUser();

  const apiCall = await apiClient.safePost<CreateUserApi>('/user', {
    body: userToCreate,
  });

  assertApi(apiCall, 201);

  const createdUser = apiCall[0].body;

  const apiUser: ApiUser = {
    ...createdUser,
    account: {
      ...createdUser.account,
      status: UserAccountStatus.created,
    },
  };

  let sessionId: string | undefined;
  let userClient: http.ApiClient | undefined;

  return {
    get apiClient() {
      return userClient;
    },

    async authenticate(password?: string): Promise<http.ApiClient> {
      const authInfo = await authenticationRaw(
        ctx,
        apiClient.cloneClient(null),
        createdUser.account.email,
        password ?? userToCreate.auth.password,
      );

      [userClient, sessionId] = authInfo;

      return userClient;
    },

    async delete(): Promise<void> {
      await delUser(apiClient, createdUser.id);
    },

    async disconnect() {
      if (!userClient) {
        return;
      }

      await disconnectCurrentUser(userClient);
    },

    id: createdUser.id,

    info: apiUser,

    login: createdUser.account.email,

    password: userToCreate.auth.password,

    get sessionId() {
      return sessionId;
    },
  };
}

async function delUser(apiClient: http.ApiClient, userId: string) {
  const apiCall = await apiClient.safeDelete<deleteUser.ApiDeclaration>(`/user/${userId}`);
  assertApi(apiCall, 200);
}

/**
 * Authenticate the user and return an ApiClient object with the
 * cookie session id set.
 */
async function authenticate(
  ctx: TestContext,
  apiClient: http.ApiClient,
  login: string,
  password: string,
): Promise<http.ApiClient> {
  const [newClient] = await authenticationRaw(
    ctx,
    apiClient,
    login,
    password,
  );

  return newClient;
}

/**
 * Authenticate the user and create an ApiClient object with the
 * cookie session id set.
 * Will return the the ApiClient object and the sessionId
 */
async function authenticationRaw(
  ctx: TestContext,
  apiClient: http.ApiClient,
  login: string,
  password: string,
): Promise<[http.ApiClient, string]> {
  // Authenticating user
  const authenticateCall = await apiClient.safePost<authenticateUser.ApiDeclaration>(
    '/auth',
    {
      body: { login, password },
    },
  );

  assertApi(authenticateCall, 200);

  // Extracting session ID
  const req = authenticateCall[0];
  const setCookie = req.res.headers.get('Set-Cookie') ?? '';
  const cookies = cookieParser.parse(setCookie) as { Expires: string, Path: string } & Record<string, string>;

  const sessionId = cookies[ctx.config.session.cookieName];

  // Creating API client
  const userClient = apiClient.cloneClient({
    cookie: {
      [ctx.config.session.cookieName]: sessionId,
    },
  });

  return [
    userClient,
    sessionId,
  ];
}

async function disconnectCurrentUser(
  apiClient: http.ApiClient,
) {
  const apiCall = await apiClient.safeDelete<disconnectUser.ApiDeclaration>('/me');
  assertApi(apiCall, 204);
}

function generateUser(roles: UserRole[] = [UserRole.user]): UserCreate {
  const user: UserCreate = {
    account: {
      email: faker.internet.email().toLowerCase(),
    },
    auth: {
      password: generatePassword(),
    },
    profile: {
      firstName: faker.person.firstName(),
      lastName: faker.person.lastName(),
    },
    roles,
  };

  return user;
}

async function getUserById(
  apiClient: http.ApiClient,
  id: string,
): Promise<ApiUser | undefined> {
  const apiCall = await apiClient.safeGet<getUser.ApiDeclaration>(`/user/${id}`);

  assertNoError(apiCall);

  const [res] = apiCall;

  if (res.status === 200) {
    expect(isValidUser(res.body)).toBe(true);
    return res.body;
  }

  return undefined;
}

function isValidUser(user: ApiUser): boolean {
  if ('auth' in user) {
    return false;
  }

  const isValid = ApiUserSchema.validate(user);

  return !isValid.error;
}

function generatePassword(): string {
  let password = faker.internet.password();

  // Ensuring that password will not be rejected by adding all expected characters
  // and having the minimum expected length
  password += `aB1+${faker.string.alphanumeric(MIN_PASSWORD_LENGTH)}`;

  return password;
}

// ============================================================
// Types
type TestUser = {
  apiClient: http.ApiClient | undefined,

  /**
   * Authenticate the user a create a new API client containing
   * user authentication information within cookie.
   */
  authenticate: (
    /**
     * Password to use for authentication.
     * If not defined, the real user password will be used
     */
    password?: string
  ) => Promise<http.ApiClient>

  delete: () => Promise<void>,

  disconnect: () => Promise<void>,

  id: string,

  info: ApiUser,

  login: string,

  password: string,

  sessionId: string | undefined,
};

// ============================================================
// Exports
export {
  authenticate,
  create,
  delUser as delete,
  generatePassword,
  generateUser as generate,
  getUserById as getById,
  isValidUser,
};

export type {
  TestUser,
};
