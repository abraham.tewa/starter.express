// ============================================================
import type { DeepPartial } from '@internal/helpers';

// Imports
import type { MongoDbContext } from '@internal/runtime';
import type {
  ServerClient as TestServerClient,
  TestContext as UntypedTestContext,
} from '@internal/tests';
import type { cmds } from '../cli';
import type { Config } from '../config';
import type RunContext from '../RunContext';
import type ServiceRuntime from '../ServiceRuntime';
import type { LogCategory } from '../types';

// ============================================================
// Types
type BaseTestContext = UntypedTestContext<Config, typeof cmds>;

type TestContext = BaseTestContext & DbContext;

type DbContext = {
  clone: (override?: DeepPartial<Config>) => Promise<TestContext>
  db: MongoDbContext<LogCategory>,
  runContext: RunContext,
  runtime: ServiceRuntime,
  teardown: (options?: {
    cleanupDb?: boolean
  }) => Promise<void>,
};

type WithDbContext<T extends BaseTestContext> = T & DbContext;

type ServerClient = TestServerClient<Config>;

// ============================================================
// Exports
export type {
  BaseTestContext,
  ServerClient,
  TestContext,
  WithDbContext,
};
