import { getContext } from './context';

export { default as initializeServer } from './initializeServer';
export * as user from './user';
export * from './types';

const context = getContext();

export {
  context as ctx,
};
