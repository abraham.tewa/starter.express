/* eslint-disable import/first */
// ============================================================
// Imports
import path from 'node:path';
import { type DeepPartial, utils } from '@internal/helpers';
import {
  type ServiceAccessResult,
  setup,
} from '@internal/tests';

import dotenv from 'dotenv';

dotenv.config({
  path: utils.fs.findUpSync(__dirname, '.test.env'),
});

import { MongoDbContext, logBuilder } from '@internal/runtime';
import { cmds } from '../cli';
import {
  type Config,
  areServiceAccessible,
  getConfig,
  mergeConfig,
} from '../config';
import {
  CONFIG_DIR,
  META_INFO,
  PROJECT_ROOT,
} from '../constants';
import ServiceRuntime from '../ServiceRuntime';
import { LogCategory } from '../types';
import type {
  BaseTestContext,
  TestContext,
  WithDbContext,
} from './types';

// ============================================================
// Module's constants and variables
const workerId = process.env.JEST_WORKER_ID
  ? Number(process.env.JEST_WORKER_ID)
  : 0;

const portSeq = utils.sequence.number(workerId * 10);

const contextIdSeq = utils.sequence.build(
  () => new Date()
    .toISOString()
    .replaceAll(':', '-')
    .replaceAll('.', '_'),
);

let currentContext: TestContext | undefined;

// ============================================================
// Functions
async function initialize(contextId?: string) {
  if (currentContext) {
    return currentContext;
  }

  const baseContext: BaseTestContext = await setup({
    bin: {
      build: utils.project.path('./dist/main.js'),
      source: utils.project.path('./service/src/cli/cli.ts'),
    },
    buildNewConfig,
    checkServiceAccess,
    cmds,
    contextId: contextId ?? contextIdSeq.next(),
    mergeConfig,
    projectRoot: PROJECT_ROOT,
    spawnEnv: {
      files: [
        path.resolve(CONFIG_DIR, 'custom-environment-variables.json'),
      ],
    },
  });

  currentContext = await transform(
    baseContext,
    LogCategory.api,
  );

  return currentContext;
}

async function transform<T extends BaseTestContext>(
  ctx: T,
  apiLogCategory: LogCategory,
): Promise<WithDbContext<T>> {
  const client = utils.mongodb.createClient(ctx.config.db);

  const context = new MongoDbContext({
    client,
    logger: logBuilder<LogCategory>(
      ctx.config.log,
      apiLogCategory,
      META_INFO,
    ),
  });

  const runtime = new ServiceRuntime(ctx.config);
  (ctx as WithDbContext<T>).runtime = runtime;

  await runtime.initialize();

  (ctx as WithDbContext<T>).runContext = runtime.createContext();

  (ctx as WithDbContext<T>).db = context;
  (ctx as WithDbContext<T>).clone = async (override?: DeepPartial<Config>) => {
    const newCtx = await ctx.createNew(
      contextIdSeq.next(),
      override,
    );
    const transformed = await transform(newCtx, apiLogCategory) as WithDbContext<T>;
    return transformed;
  };
  (ctx as WithDbContext<T>).teardown = cleanup.bind(undefined, ctx as WithDbContext<T>);
  return ctx as WithDbContext<T>;
}

async function cleanup(
  ctx: TestContext,
  options: {
    cleanupDb?: boolean,
  } = {
    cleanupDb: true,
  },
) {
  if (options.cleanupDb) {
    await cleanupDb(ctx);
  }

  await cleanupAmqp(ctx);

  await ctx.runtime.stop();
  await ctx.db.client.close();
}

async function cleanupAmqp(ctx: TestContext) {
  const amqpConnection = await utils.amqp.createConnection(
    ctx.config.amqp,
    () => ctx,
  );
  const channel = await amqpConnection.createChannel();

  await channel.deleteQueue(ctx.config.amqp.queues.user);

  await amqpConnection.close();
}

async function cleanupDb(ctx: TestContext) {
  const mongoDb = utils.mongodb.createClient(ctx.config.db);
  await mongoDb.db().dropDatabase();
  await mongoDb.close();
}

function buildNewConfig(contextId: string, ...overrides: DeepPartial<Config>[]): Config {
  const config = getConfig(
    CONFIG_DIR,
    undefined,
    ...overrides,
  );

  config.db.dbPrefix = contextId;
  config.port += portSeq.next();

  config.amqp.queues.user += `+${contextId}`;

  return config;
}

async function checkServiceAccess(config: Config): Promise<ServiceAccessResult> {
  const access = await areServiceAccessible(config);

  const result: ServiceAccessResult = {
    amqp: {
      accessible: access.amqp,
      displayableAddress: utils.amqp.getConnectString(config.amqp, undefined),
      type: 'amqp',
    },

    db: {
      accessible: access.db,
      displayableAddress: utils.mongodb.getConnectString(config.db, undefined),
      type: 'mongodb',
    },

    tasksAccess: {
      accessible: access.tasks,
      displayableAddress: utils.redis.getConnectString(config.tasks.store),
      type: 'tasks',
    },
  };

  return result;
}

function getContext() {
  if (!currentContext) {
    throw new Error('Context not initialized');
  }

  return currentContext;
}

// ============================================================
// Exports
export default initialize;
export {
  cleanup,
  getContext,
};
