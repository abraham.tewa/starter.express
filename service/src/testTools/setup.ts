/* eslint-disable import/no-import-module-exports */
import 'dotenv/config';
import setup from './context';

module.exports = setup;
