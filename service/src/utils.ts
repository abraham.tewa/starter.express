import type { ObjectId } from 'mongodb';

function hexId(id: string | ObjectId) {
  if (typeof id === 'string') {
    return id;
  }

  return id.toHexString();
}

export { hexId };
