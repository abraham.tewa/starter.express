// ============================================================
// Imports
import ExpressRuntime from '@internal/expressRuntime';
import { utils } from '@internal/helpers';
import express from 'express';
import declareApi from './api';
import {
  type Config,
  TasksOperation,
  areServiceAccessible,
} from './config';
import { DevTools } from './config/types';
import * as operations from './operations';
import RunContext from './RunContext';
import {
  type DevToolsRoutes,
  type Job,
  LogCategory,
} from './types/service';

// ============================================================
// Functions

class ServiceRuntime extends ExpressRuntime<RunContext, LogCategory, Job> {
  #config: Config;

  #runContext: RunContext | undefined;

  constructor(config: Config) {
    super({
      handle: {
        setup: async () => {
          await this.#setup();
        },
        teardown: async () => {
          await this.#teardown();
        },
      },
      http: {
        defaultLogCategory: LogCategory.api,
        host: config.host,
        port: config.port,
        session: config.session,
      },
      log: config.log,
      meta: {
        service: {
          name: process.env.npm_package_name ?? 'unknown',
          version: process.env.npm_package_version ?? 'x.x.x',
        },
      },
      openapi: {
        fileEncoding: 'utf-8',
        filePath: utils.project.path('openapi.yml'),
        validateApiSpec: false,
        validateFormats: true,
        validateRequests: true,
        validateResponses: true,
        validateSecurity: false,
      },
      tasks: {
        config: config.tasks,
        getQueueName: getTaskQueueName,
        jobHandler: taskRunner,
      },
      tooling: {
        dashboard: config.devTools.includes(DevTools.dashboard),
        openapi: config.devTools.includes(DevTools.openapi),
        routePrefix: '/_',
        taskAdmin: config.devTools.includes(DevTools.dashboard),
      },
    });

    declareApi(this);

    // Documentation
    if (config.devTools.includes(DevTools.documentation)) {
      this.httpServer.app.use(
        this.toolingRoutes.documentation,
        express.static(utils.project.path('documentation')),
      );
    }

    this.#config = config;
  }

  /**
   * Check if all
   */
  async areServicesAccessibility(): Promise<boolean> {
    const { db, session, tasks } = await this.serviceAccessibility();
    return db && session && tasks;
  }

  override createContext(): RunContext {
    if (!this.#runContext) {
      throw new Error('Runtime not initialized');
    }

    const ctx = this.#runContext.child();

    return ctx;
  }

  async serviceAccessibility(): Promise<{
    db: boolean,
    session: boolean,
    tasks: boolean,
  }> {
    return areServiceAccessible(this.#config);
  }

  override get toolingRoutes(): DevToolsRoutes {
    return {
      ...super.toolingRoutes,
      documentation: '/_',
    };
  }

  async #setup() {
    this.#runContext = await RunContext.createContext(this, this.#config);
  }

  async #teardown() {
    await this.#runContext?.db.client.close();
  }
}

// ============================================================
// Helpers
async function taskRunner(
  data: Job['dataType'],
  context: RunContext,
): Promise<Job['returnType']> {
  switch (data.type) {
    case TasksOperation.check:
      await operations.checkUser(context, data.userId);
      break;

    default: {
      const { type } = data as { type: string };
      throw new Error(`Unknown operation: ${type}`);
    }
  }
}

function getTaskQueueName({ data }: { data: Job['dataType'] }): string {
  return data.type;
}

// ============================================================
// Exports
export default ServiceRuntime;
