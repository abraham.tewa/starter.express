// ============================================================
// Imports
import {
  utils,
} from '@internal/helpers';
import type {
  Request,
  Response,
} from 'express';
import type {
  ApiUser,
} from './types';
import type RunContext from '../RunContext';

// ============================================================
// Functions
async function disconnectUser(
  ctx: RunContext,
  req: Request<unknown, ResBody, ReqBody>,
  res: Response<ResBody>,
) {
  const promise = new utils.PromiseHolder();
  req.session.destroy(promise.nodeCallback);

  await promise;

  res
    .status(204)
    .send();
}

// ============================================================
// Password
type ApiDeclaration = {
  204: undefined,
  req: {
    body: undefined
  }
};

type ReqBody = {
  login: string,
  password: string,
};

type ResBody = ApiUser;

// ============================================================
// Exports
export default disconnectUser;
export {
  ApiDeclaration,
};
