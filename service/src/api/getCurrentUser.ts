// ============================================================
// Imports
import { http } from '@internal/helpers';
import type { Request, Response } from 'express';

import toApi from './utils/toApi';
import * as db from '../db';
import { LogCategory } from '../types/service';
import type { ApiUser } from './types';
import type RunContext from '../RunContext';

// ============================================================
// Functions
async function getCurrentUser(
  ctx: RunContext,
  req: Request<ReqParams>,
  res: Response<ApiUser>,
) {
  const userId = req.session.user?.id;

  if (!userId) {
    ctx.logger.trace({
      category: LogCategory.user,
      label: 'operation: getCurrentId',
      message: 'UserId not found in session',
      moreInfo: {
        session: req.session,
      },
    });
    throw new http.error.Unauthorized();
  }

  const user = await db.user.getById(ctx.db, userId);

  // Returning Unauthorized if user no longer exists
  if (!user) {
    throw new http.error.Unauthorized();
  }

  res.send(toApi(user));
}

// ============================================================
// Types
type ReqParams = {
  id: string;
};

type ApiDeclaration = {
  200: ApiUser;
  401: http.ApiError;
  404: http.ApiError;
  req: {
    body: never;
  };
};

// ============================================================
// Exports
export default getCurrentUser;
export type { ApiDeclaration };
