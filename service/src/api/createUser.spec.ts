// ============================================================
// Imports
import { faker } from '@faker-js/faker';
import { assertApi, assertApiError } from '@internal/tests';
import type { http } from '@internal/helpers';
import { MAX_PASSWORD_LENGTH, MIN_PASSWORD_LENGTH } from '../constants';

import * as testTools from '../testTools';
import { UserAccountStatus } from '../types';
import type * as createUser from './createUser';
import type * as getUser from './getUser';

const { ctx } = testTools;

// ============================================================
// Tests
describe('api: createUser', () => {
  let server: testTools.ServerClient;
  let adminClient: http.ApiClient;

  beforeAll(async () => {
    const testInfo = await testTools.initializeServer(ctx);
    server = testInfo.server;
    adminClient = testInfo.admin;
  });

  afterAll(async () => {
    await server.stop();
    await ctx.teardown();
  });

  it('return the created user', async () => {
    const generatedUser = testTools.user.generate();

    const createCall = await adminClient.safePost<createUser.ApiDeclaration>(
      '/user',
      {
        body: generatedUser,
      },
    );

    const { auth, ...expectedUser } = generatedUser;

    assertApi(createCall, 201);
    const createdUser = createCall[0].body;
    expect(testTools.user.isValidUser(createdUser)).toBe(true);

    expect(createdUser).toEqual({
      ...expectedUser,
      account: {
        ...expectedUser.account,
        creationDate: createdUser.account.creationDate,
        status: UserAccountStatus.created,
      },
      id: createdUser.id,
    });

    const getCall = await adminClient.safeGet<getUser.ApiDeclaration>(
      `/user/${createdUser.id}`,
    );

    assertApi(getCall, 200);

    const fetchedUser = getCall[0].body;

    expect(fetchedUser).toEqual(createdUser);
  });

  it('return 400 if email has uppercase characters', async () => {
    const generatedUser = testTools.user.generate();

    const createCall = await adminClient.safePost<createUser.ApiDeclaration>(
      '/user',
      {
        body: {
          ...generatedUser,
          account: {
            email: generatedUser.account.email.toUpperCase(),
          },
        },
      },
    );

    assertApi(createCall, 400);

    expect(createCall[0].body.errors).toEqual(['Invalid email (should be lowercase)']);
  });

  it('return 400 if email already used', async () => {
    const generatedUser = testTools.user.generate();

    let createCall = await adminClient.safePost<createUser.ApiDeclaration>(
      '/user',
      {
        body: generatedUser,
      },
    );

    assertApi(createCall, 201);

    createCall = await adminClient.safePost<createUser.ApiDeclaration>(
      '/user',
      {
        body: generatedUser,
      },
    );

    assertApiError(createCall, 400, 'Email already used');
  });

  describe('Password validation', () => {
    /**
     * Try to authenticate a non-existing user with a password that have an invalid format.
     * => Before even checking that the user exists, we check that the format is valid.
     */
    async function checkPasswordError(password: string, error: string) {
      const generatedUser = testTools.user.generate();
      generatedUser.auth.password = password;

      const createCall = await adminClient.safePost<createUser.ApiDeclaration>(
        '/user',
        {
          body: generatedUser,
        },
      );

      assertApiError(createCall, 400, 'Invalid user information');
      expect(createCall[0].body.errors).toEqual([error]);
    }

    it('return 400 if password is too short', async () => {
      await checkPasswordError(
        'aB1+',
        `Password too short (min: ${MIN_PASSWORD_LENGTH} characters)`,
      );
    });

    it('return 400 if password is too long', async () => {
      await checkPasswordError(
        `aB1+${faker.string.alphanumeric(MAX_PASSWORD_LENGTH)}`,
        `Password too long (max: ${MAX_PASSWORD_LENGTH} characters)`,
      );
    });

    it('return 400 if password starts with a space character', async () => {
      await checkPasswordError(
        '  HelloWorld123456+',
        'Password cannot starts with a space character',
      );
    });

    it('return 400 if password ends with a space character', async () => {
      await checkPasswordError(
        'HelloWorld123456+   ',
        'Password cannot ends with a space character',
      );
    });

    it('return 400 if do not contains a special character', async () => {
      await checkPasswordError(
        'helloWorld123',
        'Password must contains at least one special character',
      );
    });

    it('return 400 if do not contains a number', async () => {
      await checkPasswordError(
        'helloWord+',
        'Password must contains at least one number',
      );
    });

    it('return 400 if do not contains a lowercase character', async () => {
      await checkPasswordError(
        'HELLO WORLD123456+',
        'Password must contains at least one lowercase character',
      );
    });

    it('return 400 if do not contains an uppercase character', async () => {
      await checkPasswordError(
        'hello world 132456+',
        'Password must contains at least one uppercase character',
      );
    });
  });
});
