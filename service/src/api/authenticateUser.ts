// ============================================================
// Imports
import { http } from '@internal/helpers';
import type {
  Request,
  Response,
} from 'express';

import { toApi } from './utils';
import * as operation from '../operations';
import type { ApiUser } from './types';
import type RunContext from '../RunContext';

// ============================================================
// Functions
async function authenticateUser(
  ctx: RunContext,
  req: Request<unknown, ResBody, ReqBody>,
  res: Response<ResBody>,
) {
  const passwordFormatErrors = operation.auth.validatePasswordFormat(req.body.password);

  if (passwordFormatErrors.length) {
    throw new http.error.InvalidRequest('Invalid password format', { errors: passwordFormatErrors });
  }

  const [isValidPassword, user] = await operation.auth.isValidPassword(
    ctx,
    req.body.login,
    req.body.password,
  );

  if (!isValidPassword) {
    throw new http.error.Unauthorized('Invalid email/password');
  }

  req.session.user = {
    email: user.account.email,
    id: user._id.toHexString(),
    roles: [...user.roles],
  };

  res
    .status(200)
    .send(
      toApi(user),
    );
}

// ============================================================
// Password
type ApiDeclaration = {
  200: ApiUser,
  400: http.ApiError<{ errors: string[] }>,
  401: http.ApiError,
  req: {
    body: {
      login: string,
      password: string,
    }
  }
};

type ReqBody = {
  login: string,
  password: string,
};

type ResBody = ApiUser;

// ============================================================
// Exports
export default authenticateUser;
export {
  ApiDeclaration,
};
