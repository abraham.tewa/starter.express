// ============================================================
// Imports
import { assertApi, assertApiError } from '@internal/tests';

import type { http } from '@internal/helpers';
import * as testTools from '../testTools';
import type * as getUser from './getUser';
import type { ApiUser } from './types';
import type * as updateUser from './updateUser';

const { ctx } = testTools;

// ============================================================
// Tests
describe('api: getUser', () => {
  let server: testTools.ServerClient;
  let adminClient: http.ApiClient;

  beforeAll(async () => {
    const testInfo = await testTools.initializeServer(testTools.ctx);
    server = testInfo.server;
    adminClient = testInfo.admin;
  });

  afterAll(async () => {
    await server.stop();
    await ctx.teardown();
  });

  it('return 200 when update user', async () => {
    // Creating user
    const user = await testTools.user.create(testTools.ctx, adminClient);

    // Updating
    const newProfile = {
      firstName: `${user.info.profile.firstName}xx`,
      lastName: `${user.info.profile.lastName}xx`,
    };
    const expectedUser: ApiUser = {
      ...user.info,
      profile: newProfile,
    };

    const updateCall = await adminClient.safePatch<updateUser.ApiDeclaration>(
      `/user/${user.id}`,
      {
        body: {
          profile: newProfile,
        },
      },
    );

    assertApi(updateCall, 200);

    const updatedUser = updateCall[0].body;

    expect(updatedUser).toEqual(expectedUser);

    // Fetching new user
    const getCall = await adminClient.safeGet<getUser.ApiDeclaration>(`/user/${user.id}`);
    assertApi(getCall, 200);
    const getUser = getCall[0].body;

    expect(getUser).toEqual(expectedUser);

    await user.delete();
  });

  it('return 404 when user not found', async () => {
    // Creating user
    const user = await testTools.user.create(testTools.ctx, adminClient);

    await user.delete();

    const newProfile = {
      firstName: `${user.info.profile.firstName}xx`,
      lastName: `${user.info.profile.lastName}xx`,
    };

    const updateCall = await adminClient.safePatch<updateUser.ApiDeclaration>(
      `/user/${user.id}`,
      {
        body: {
          profile: newProfile,
        },
      },
    );

    assertApiError(updateCall, 404, `User "${user.id}" not found`);
  });
});
