// ============================================================
// Imports
import { assertApi, assertApiError } from '@internal/tests';

import type { http } from '@internal/helpers';
import * as testTools from '../testTools';
import type * as getCurrentUser from './getCurrentUser';

const { ctx } = testTools;

// ============================================================
// Tests
describe('api: getUser', () => {
  let server: testTools.ServerClient;
  let adminClient: http.ApiClient;
  let noAuthClient: http.ApiClient;

  beforeAll(async () => {
    const testInfo = await testTools.initializeServer(ctx);
    server = testInfo.server;
    adminClient = testInfo.admin;
    noAuthClient = testInfo.noAuthClient;
  });

  afterAll(async () => {
    await server.stop();
    await ctx.teardown();
  });

  it('return 200 with user information', async () => {
    const user = await testTools.user.create(ctx, adminClient);

    const apiCall = await adminClient.safeGet<getCurrentUser.ApiDeclaration>(`/user/${user.id}`);

    assertApi(apiCall, 200);

    const fetchedUser = apiCall[0].body;

    expect(fetchedUser).toEqual(user.info);

    await user.delete();
  });

  it('return 401 if the user no longer exists', async () => {
    // Creating and authenticating user
    const user = await testTools.user.create(ctx, adminClient);
    const userClient = await user.authenticate();

    let apiCall = await userClient.safeGet<getCurrentUser.ApiDeclaration>('/me');

    assertApi(apiCall, 200);

    const fetchedUser = apiCall[0].body;

    expect(fetchedUser).toEqual(user.info);

    // Delete the user
    await user.delete();

    // The client is no longer connected
    apiCall = await userClient.safeGet<getCurrentUser.ApiDeclaration>('/me');

    assertApiError(apiCall, 401);
  });

  describe('security', () => {
    it('return 404 when user is not authenticated', async () => {
      // Creating user
      const user = await testTools.user.create(ctx, adminClient);

      const apiCall = await noAuthClient.safeGet<getCurrentUser.ApiDeclaration>(`/user/${user.id}`);
      assertApi(apiCall, 404);

      await user.delete();
    });

    it('return 404 when user has no admin rights', async () => {
    // Creating and authenticating user
      const user = await testTools.user.create(ctx, adminClient);
      const userClient = await user.authenticate();

      const apiCall = await userClient.safeGet<getCurrentUser.ApiDeclaration>(`/user/${user.id}`);
      assertApi(apiCall, 404);

      await user.delete();
    });
  });
});
