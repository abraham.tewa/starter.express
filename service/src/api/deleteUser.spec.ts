// ============================================================
// Imports
import { assertApi } from '@internal/tests';
import type { http } from '@internal/helpers';

import * as testTools from '../testTools';
import type * as deleteUser from './deleteUser';
import type * as getUser from './getUser';

const { ctx } = testTools;

// ============================================================
// Tests
describe('api: deleteUser', () => {
  let server: testTools.ServerClient;
  let adminClient: http.ApiClient;

  beforeAll(async () => {
    const serverInfo = await testTools.initializeServer(ctx);
    server = serverInfo.server;
    adminClient = serverInfo.admin;
  });

  afterAll(async () => {
    await server.stop();
    await ctx.teardown();
  });

  it('return 200 when delete the user', async () => {
    const generatedUser = await testTools.user.create(ctx, adminClient);

    const deleteCall = await adminClient.safeDelete<deleteUser.ApiDeclaration>(
      `/user/${generatedUser.id}`,
    );

    assertApi(deleteCall, 200);

    const getCall = await adminClient.safeGet<getUser.ApiDeclaration>(
      `/user/${generatedUser.id}`,
    );

    assertApi(getCall, 404);
  });

  it('return 404 if user not found', async () => {
    const deleteCall = await adminClient.safeDelete<deleteUser.ApiDeclaration>(
      '/user/012345678901234567890123',
    );

    assertApi(deleteCall, 404);
  });
});
