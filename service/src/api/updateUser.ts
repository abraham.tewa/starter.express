// ============================================================
// Imports
import { http } from '@internal/helpers';
import type { Request, Response } from 'express';
import type { MatchKeysAndValues, UpdateFilter } from 'mongodb';
import { toApi } from './utils';

import * as db from '../db';
import type { ApiUser } from './types';
import type RunContext from '../RunContext';
import type { UserDocument } from '../types';

// ============================================================
// Functions
async function updateUser(
  ctx: RunContext,
  req: Request<ReqParams, unknown, ReqBody>,
  res: Response<ApiUser>,
) {
  const { id } = req.params;

  const updateFilter = createUpdateFilter(req.body);

  const user = await db.user.update(
    ctx.db,
    id,
    updateFilter,
    {
      returnDocument: 'after',
    },
  );

  if (!user) {
    throw new http.error.NotFound(`User "${id}" not found`);
  }

  res.send(toApi(user));
}

function createUpdateFilter(user: UpdateUser): UpdateFilter<UserDocument> {
  const setUser: MatchKeysAndValues<UserDocument> = {};

  if (typeof user.profile.firstName === 'string') {
    setUser['profile.firstName'] = user.profile.firstName;
  }

  if (typeof user.profile.lastName === 'string') {
    setUser['profile.lastName'] = user.profile.lastName;
  }

  return {
    $set: setUser,
  };
}

// ============================================================
// Types
type ReqParams = {
  id: string;
};

type ReqBody = UpdateUser;

type UpdateUser = Omit<ApiUser, 'id' | 'account' | 'roles'>;

type ApiDeclaration = {
  200: ApiUser;
  404: undefined;
  req: {
    body: ReqBody;
  };
};

// ============================================================
// Exports
export default updateUser;
export {
  ApiDeclaration,
};
