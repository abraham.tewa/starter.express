// ============================================================
// Imports
import { http } from '@internal/helpers';
import type { Request, Response } from 'express';

import toApi from './utils/toApi';
import * as db from '../db';
import type { ApiUser } from './types';
import type RunContext from '../RunContext';

// ============================================================
// Functions
async function getUser(
  ctx: RunContext,
  req: Request<ReqParams>,
  res: Response<ApiUser>,
) {
  const { id } = req.params;

  const user = await db.user.getById(ctx.db, id);

  if (!user) {
    throw new http.error.NotFound('User not found');
  }

  res.send(toApi(user));
}

// ============================================================
// Types
type ReqParams = {
  id: string;
};

type ApiDeclaration = {
  200: ApiUser;
  404: undefined;
  req: {
    body: never;
  };
};

// ============================================================
// Exports
export default getUser;
export type { ApiDeclaration };
