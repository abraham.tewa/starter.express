import type { ToApiObject } from 'service/src/types/documents';
import type { UserJson } from '../types';

type ApiUser = ToApiObject<Omit<UserJson, 'auth' | 'account'>> & {
  account: Omit<UserJson['account'], 'emailValidationToken'>
};

export type { ApiUser };
