// ============================================================
// Imports
import { http } from '@internal/helpers';
import type { Request, Response } from 'express';

import * as db from '../db';
import type { ApiUser } from './types';
import type RunContext from '../RunContext';

// ============================================================
// Functions
async function deleteUser(
  ctx: RunContext,
  req: Request<ReqParams>,
  res: Response<ApiUser>,
) {
  const { id } = req.params;

  const deleted = await db.user.delete(ctx.db, id);

  if (deleted === 0) {
    throw new http.error.NotFound(`User "${id}" not found`);
  }

  res.status(200).send();
}

// ============================================================
// Types
type ReqParams = {
  id: string;
};

type ApiDeclaration = {
  200: undefined;
  req: {
    body: never;
  };
};

// ============================================================
// Exports
export default deleteUser;
export { ApiDeclaration };
