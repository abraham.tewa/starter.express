// ============================================================
// Imports
import { assertApi, assertApiError } from '@internal/tests';
import type { http } from '@internal/helpers';

import * as testTools from '../testTools';
import type * as disconnectUser from './disconnectUser';
import type * as getCurrentUser from './getCurrentUser';

const { ctx } = testTools;

// ============================================================
// Tests
describe('api: disconnectUser', () => {
  let server: testTools.ServerClient;
  let user: testTools.user.TestUser;
  let adminClient: http.ApiClient;

  beforeAll(async () => {
    const serverInfo = await testTools.initializeServer(ctx);
    adminClient = serverInfo.admin;
    server = serverInfo.server;
    user = await testTools.user.create(ctx, adminClient);
  });

  afterAll(async () => {
    await server.stop();
    await ctx.teardown();
  });

  it('return 204 when disconnection successful', async () => {
    const userClient = await user.authenticate();

    // Ensuring user is connected
    let getUserCall = await userClient.safeGet<getCurrentUser.ApiDeclaration>('/me');
    assertApi(getUserCall, 200);
    const fetchedUser = getUserCall[0].body;
    expect(fetchedUser).toEqual(user.info);

    // Disconnecting user
    const disconnectUserCall = await userClient.safeDelete<disconnectUser.ApiDeclaration>('/me');
    assertApi(disconnectUserCall, 204);

    // Ensuring the user is no longer connected
    getUserCall = await userClient.safeGet<getCurrentUser.ApiDeclaration>('/me');
    assertApiError(getUserCall, 401);
  });

  it('return 204 even if no user connected', async () => {
    const userClient = await user.authenticate();

    // Ensuring user is connected
    let getUserCall = await userClient.safeGet<getCurrentUser.ApiDeclaration>('/me');
    assertApi(getUserCall, 200);

    // Disconnecting user
    const disconnectUserCall = await userClient.safeDelete<disconnectUser.ApiDeclaration>('/me');
    assertApi(disconnectUserCall, 204);

    // Ensuring no user is connected
    getUserCall = await userClient.safeGet<getCurrentUser.ApiDeclaration>('/me');
    assertApiError(getUserCall, 401);
  });
});
