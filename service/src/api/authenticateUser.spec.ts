// ============================================================
import { faker } from '@faker-js/faker';
// Imports
import {
  assertApi,
  assertApiError,
  assertHttpHeaderExists,
  assertHttpHeaderNotExists,
  assertIsDefined,
  assertNoError,
} from '@internal/tests';
import cookieParser from 'cookie';

import type { http } from '@internal/helpers';
import {
  MAX_AUTHENTICATION_ATTEMPT_ALLOWED,
  MAX_PASSWORD_LENGTH,
  MIN_PASSWORD_LENGTH,
  USER_ACCOUNT_LOCK_DURATION,
} from '../constants';
import * as testTools from '../testTools';
import type * as authenticateUser from './authenticateUser';

const { ctx } = testTools;

// ============================================================
// Tests
describe('api: authenticateUser', () => {
  let server: testTools.ServerClient;
  let adminClient: http.ApiClient;
  let noAuthClient: http.ApiClient;

  beforeAll(async () => {
    const serverInfo = await testTools.initializeServer(ctx);
    server = serverInfo.server;
    adminClient = serverInfo.admin;
    noAuthClient = serverInfo.noAuthClient;
  });

  afterAll(async () => {
    await server.stop();
    await ctx.teardown();
  });

  it('return 200 with a valid session cookie', async () => {
    const user = await testTools.user.create(ctx, adminClient);

    const loginInfo = {
      login: user.info.account.email,
      password: user.password,
    };

    const authenticateCall = await noAuthClient.safePost<authenticateUser.ApiDeclaration>(
      '/auth',
      { body: loginInfo },
    );

    assertApi(authenticateCall, 200);

    assertHttpHeaderExists(authenticateCall, 'Set-Cookie');

    const req = authenticateCall[0];

    expect(testTools.user.isValidUser(req.body)).toBe(true);

    const cookie = getCookie(authenticateCall);
    assertIsDefined(cookie);

    const cookieMaxAge = new Date(cookie.Expires);
    const expectedDuration = ctx.env.config.session.maxAge * 1000;

    expect(cookieMaxAge.getTime()).toBeLessThan(Date.now() + expectedDuration);
    expect(cookieMaxAge.getTime()).toBeGreaterThan(Date.now() + expectedDuration - 10 * 1000);

    await user.disconnect();
    await user.delete();
  });

  it('return 401 when invalid password', async () => {
    const user = await testTools.user.create(ctx, adminClient);

    const fakePassword = `${user.password}x`;
    const loginInfo = {
      login: user.info.account.email,
      password: fakePassword,
    };

    const authenticateCall = await noAuthClient.safePost<authenticateUser.ApiDeclaration>(
      '/auth',
      { body: loginInfo },
    );

    assertApiError(authenticateCall, 401);
    assertHttpHeaderNotExists(authenticateCall, 'Set-Cookie');

    await user.delete();
  });

  it('not return 400 if user no longer exists', async () => {
    let user = await testTools.user.create(ctx, adminClient);

    const apiClient = await user.authenticate();
    const { sessionId: initialSessionId } = user;
    assertIsDefined(initialSessionId);
    await user.delete();

    // Creating a new user
    user = await testTools.user.create(ctx, adminClient);

    const apiCall = await apiClient.safePost<authenticateUser.ApiDeclaration>('/auth', {
      body: {
        login: user.login,
        password: user.password,
      },
    });

    assertApi(apiCall, 200);

    // Ensuring that the call have created a new session
    const newSessionId = getSessionId(apiCall);
    assertIsDefined(newSessionId);

    expect(newSessionId.length).toBeGreaterThan(0);
    expect(newSessionId).toEqual(initialSessionId);
  });

  it('update failed attempts', async () => {
    const user = await testTools.user.create(ctx, adminClient);

    // Resetting failed attempt counter by forcing authentication
    await user.authenticate();
    await user.disconnect();

    // Logging with fake password
    const fakePassword = `${user.password}x`;
    const loginInfo = {
      login: user.info.account.email,
      password: fakePassword,
    };

    // Ensuring no lock
    let apiUser = await testTools.user.getById(adminClient, user.id);

    assertIsDefined(apiUser);
    expect(apiUser.account.lock).toBeUndefined();

    // Locking the account
    await lockUser(noAuthClient, loginInfo);

    apiUser = await testTools.user.getById(adminClient, user.id);
    assertIsDefined(apiUser);
    expect(testTools.user.isValidUser(apiUser)).toBe(true);
    assertIsDefined(apiUser.account.lock);

    const lockUntil = new Date(apiUser.account.lock.until);
    const lockUntilMin = Date.now() + USER_ACCOUNT_LOCK_DURATION - 60 * 1000;
    const lockUntilMax = Date.now() + USER_ACCOUNT_LOCK_DURATION;

    expect(lockUntil.getTime()).toBeGreaterThan(lockUntilMin);
    expect(lockUntil.getTime()).toBeLessThan(lockUntilMax);

    // Ensuring that the user is not able to connect anymore
    const authenticateCall = await noAuthClient.safePost<authenticateUser.ApiDeclaration>(
      '/auth',
      { body: loginInfo },
    );

    assertApiError(authenticateCall, 401);
    assertHttpHeaderNotExists(authenticateCall, 'Set-Cookie');

    await user.delete();
  });

  describe('check first password format', () => {
    /**
     * Try to authenticate a non-existing user with a password that have an invalid format.
     * => Before even checking that the user exists, we check that the format is valid.
     */
    async function authWithInvalidPasswordFormat(password: string, error: string) {
      const generatedUser = testTools.user.generate();
      generatedUser.auth.password = password;

      const apiCall = await noAuthClient.safePost<authenticateUser.ApiDeclaration>('/auth', {
        body: {
          login: generatedUser.account.email,
          password: generatedUser.auth.password,
        },
      });

      assertApi(apiCall, 400);
      expect(apiCall[0].body.errors).toEqual([error]);
    }

    it('return 400 if password is too short', async () => {
      await authWithInvalidPasswordFormat(
        'aB1+',
        `Password too short (min: ${MIN_PASSWORD_LENGTH} characters)`,
      );
    });

    it('return 400 if password is too long', async () => {
      await authWithInvalidPasswordFormat(
        `aB1+${faker.string.alphanumeric(MAX_PASSWORD_LENGTH)}`,
        `Password too long (max: ${MAX_PASSWORD_LENGTH} characters)`,
      );
    });

    it('return 400 if password starts with a space character', async () => {
      await authWithInvalidPasswordFormat(
        '  HelloWorld123456+',
        'Password cannot starts with a space character',
      );
    });

    it('return 400 if password ends with a space character', async () => {
      await authWithInvalidPasswordFormat(
        'HelloWorld123456+   ',
        'Password cannot ends with a space character',
      );
    });

    it('return 400 if do not contains a special character', async () => {
      await authWithInvalidPasswordFormat(
        'helloWorld123',
        'Password must contains at least one special character',
      );
    });

    it('return 400 if do not contains a number', async () => {
      await authWithInvalidPasswordFormat(
        'helloWord+',
        'Password must contains at least one number',
      );
    });

    it('return 400 if do not contains a lowercase character', async () => {
      await authWithInvalidPasswordFormat(
        'HELLO WORLD123456+',
        'Password must contains at least one lowercase character',
      );
    });

    it('return 400 if do not contains an uppercase character', async () => {
      await authWithInvalidPasswordFormat(
        'hello world 132456+',
        'Password must contains at least one uppercase character',
      );
    });
  });
});

async function lockUser(
  apiClient: http.ApiClient,
  loginInfo: {
    login: string,
    password: string,
  },
) {
  const fakeLogin = {
    ...loginInfo,
    password: `${loginInfo.password}xx`,
  };

  for (let i = 0; i < MAX_AUTHENTICATION_ATTEMPT_ALLOWED + 1; i += 1) {
    // eslint-disable-next-line no-await-in-loop
    const authenticateCall = await apiClient.safePost<authenticateUser.ApiDeclaration>(
      '/auth',
      {
        body: fakeLogin,
      },
    );

    assertApiError(authenticateCall, 401);
  }
}

function getCookie(
  apiCall: http.SafeApi<http.ApiDeclaration>,
): {
    Expires: string,
    Path: string,
  } & Record<string, string> | undefined {
  assertNoError(apiCall);

  const setCookie = apiCall[0].res.headers.get('Set-Cookie') ?? '';
  const cookie = cookieParser.parse(setCookie) as {
    Expires: string,
    Path: string,
  } & Record<string, string>;
  expect(cookie).toHaveProperty(ctx.env.config.session.cookieName);

  return cookie;
}

function getSessionId(
  apiCall: http.SafeApi<http.ApiDeclaration>,
): string | undefined {
  const cookie = getCookie(apiCall);

  assertIsDefined(cookie);

  const sessionId = cookie[ctx.env.config.session.cookieName];

  return sessionId;
}
