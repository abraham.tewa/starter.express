// ============================================================
// Imports
import { assertApi, assertApiError } from '@internal/tests';
import type { http } from '@internal/helpers';

import * as testTools from '../testTools';
import type * as getCurrentUser from './getCurrentUser';

const { ctx } = testTools;

// ============================================================
// Tests
describe('api: getCurrentUser', () => {
  let server: testTools.ServerClient;
  let user: testTools.user.TestUser;
  let noAuthClient: http.ApiClient;

  beforeAll(async () => {
    const testInfo = await testTools.initializeServer(ctx);
    server = testInfo.server;
    noAuthClient = testInfo.noAuthClient;
    user = await testTools.user.create(ctx, testInfo.admin);
  });

  afterAll(async () => {
    await server.stop();
    await ctx.teardown();
  });

  it('return 200 with user information', async () => {
    const userClient = await user.authenticate();

    const apiCall = await userClient.safeGet<getCurrentUser.ApiDeclaration>('/me');

    assertApi(apiCall, 200);

    const fetchedUser = apiCall[0].body;

    expect(fetchedUser).toEqual(user.info);
  });

  it('return 401 when user is not authenticated', async () => {
    const apiCall = await noAuthClient.safeGet<getCurrentUser.ApiDeclaration>('/me');

    assertApiError(apiCall, 401, 'The access to the given resource is not allowed');
  });

  it('return 401 if the user no longer exists', async () => {
    // Creating and authenticating user
    const userClient = await user.authenticate();

    let apiCall = await userClient.safeGet<getCurrentUser.ApiDeclaration>('/me');

    assertApi(apiCall, 200);

    const fetchedUser = apiCall[0].body;

    expect(fetchedUser).toEqual(user.info);

    // Delete the user
    await user.delete();

    // The client is no longer connected
    apiCall = await userClient.safeGet<getCurrentUser.ApiDeclaration>('/me');

    assertApiError(apiCall, 401);
  });
});
