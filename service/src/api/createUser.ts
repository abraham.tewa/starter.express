// ============================================================
// Imports
import { http } from '@internal/helpers';
import type { Request, Response } from 'express';
import { toApi } from './utils';
import * as ope from '../operations';
import type { ApiUser } from './types';
import type { UserCreate } from '../operations/user';
import type RunContext from '../RunContext';

// ============================================================
// Functions
async function createUser(
  ctx: RunContext,
  req: Request<unknown, ApiUser, UserCreate>,
  res: Response<ApiUser>,
) {
  const userToCreate = req.body;

  // Data validation
  let errors: string[] = [];

  if (userToCreate.account.email.toLowerCase() !== userToCreate.account.email) {
    errors.push('Invalid email (should be lowercase)');
  }

  errors = [
    ...errors,
    ...ope.auth.validatePasswordFormat(userToCreate.auth.password),
  ];

  if (errors.length) {
    throw new http.error.InvalidRequest('Invalid user information', { errors });
  }

  // User creation
  try {
    const createdUser = await ope.user.create(ctx, req.body);

    res.status(201).send(
      toApi(createdUser),
    );
  } catch (err) {
    if (err instanceof ope.user.EmailAlreadyUsedError) {
      throw new http.error.InvalidRequest('Email already used');
    }

    throw err;
  }
}

// ============================================================
// Types
type ApiDeclaration = {
  201: ApiUser;
  400: http.ApiError<{ errors: string[] }>;
  req: {
    body: UserCreate;
  };
};

// ============================================================
// Exports
export default createUser;
export type { ApiDeclaration };
