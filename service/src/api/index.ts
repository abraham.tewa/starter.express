// ============================================================
// Imports
import { utils } from '@internal/helpers';
import type { NextFunction, Request, Response } from 'express';
import authenticateUser from './authenticateUser';

import createUser from './createUser';
import deleteUser from './deleteUser';
import disconnectUser from './disconnectUser';
import getCurrentUser from './getCurrentUser';
import getUser from './getUser';
import updateUser from './updateUser';
import { UserRole } from '../types';
import type ServiceRuntime from '../ServiceRuntime';

// ============================================================
// Module's constants and variables
const ALL_USERS: UserRole[] = [];
const ADMIN_ONLY = [UserRole.applicationAdmin];

// ============================================================
// Functions
function declare(runtime: ServiceRuntime) {
  const parentRouter = runtime.httpServer.app;

  const adminOnly = auth(ADMIN_ONLY);
  const allUsers = auth(ALL_USERS);

  parentRouter.get('/user/:id', adminOnly, runtime.w(getUser));
  parentRouter.post('/user', adminOnly, runtime.w(createUser));
  parentRouter.patch('/user/:id', adminOnly, runtime.w(updateUser));
  parentRouter.delete('/user/:id', adminOnly, runtime.w(deleteUser));

  parentRouter.post('/auth', allUsers, runtime.w(authenticateUser));
  parentRouter.get('/me', allUsers, runtime.w(getCurrentUser));
  parentRouter.delete('/me', allUsers, runtime.w(disconnectUser));
}

// ============================================================
// Helpers

function auth(roles: UserRole[]) {
  return (req: Request, res: Response, next: NextFunction) => {
    if (!roles.length) {
      next();
      return;
    }

    const userRoles = req.session.user?.roles;

    if (!userRoles) {
      res.status(404).send();
      return;
    }

    const validRoles = utils.array.intersects(
      roles,
      userRoles,
    );

    if (!validRoles.length) {
      res.status(404).send();
      return;
    }

    next();
  };
}

// ============================================================
// Exports
export default declare;
