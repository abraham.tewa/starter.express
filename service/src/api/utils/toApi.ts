// ============================================================
// Imports
import type {
  UserAuthInfo,
  UserDocument,
  UserWithPasswordDocument,
} from '../../types';
import type { ApiUser } from '../types';

// ============================================================
// Functions
function toApi({
  _id: id,
  account: {
    emailValidationToken,
    ...account
  },
  ...user
}: UserDocument | UserWithPasswordDocument): ApiUser {
  const apiUser: Omit<ApiUser, 'auth'> & { auth?: UserAuthInfo } = {
    ...user,
    account: {
      ...account,
      creationDate: account.creationDate.toISOString(),
      lock: account.lock
        ? {
          ...account.lock,
          since: account.lock.since.toISOString(),
          until: account.lock.until.toISOString(),
        }
        : undefined,
    },
    id: id.toHexString(),
  };

  delete apiUser.auth;

  return apiUser as ApiUser;
}

// ============================================================
// Exports
export default toApi;
