// ============================================================
// Imports
import { ObjectId } from 'mongodb';
import type { UserDocument } from '../../types';

import type { ApiUser } from '../types';

// ============================================================
// Functions
function fromApi({ account, id, ...user }: ApiUser): UserDocument {
  const document: UserDocument = {
    ...user,
    _id: new ObjectId(id),
    account: {
      ...account,
      creationDate: new Date(account.creationDate),
      lock: account.lock
        ? {
          ...account.lock,
          since: new Date(account.lock.since),
          until: new Date(account.lock.until),
        }
        : undefined,
    },
  };

  return document;
}

// ============================================================
// Exports
export default fromApi;
