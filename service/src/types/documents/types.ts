// ============================================================
// Imports
import Joi from 'joi';
import type { JSONValue } from '@internal/helpers';
import type { SchemaMap } from 'joi';
import type { ObjectId } from 'mongodb';

// ============================================================
// Schema
function joinQuerySchema<TSchema = unknown, IsStrict = false, T = TSchema>(
  schemaToJoin?: SchemaMap<T, IsStrict>,
): Joi.ObjectSchema<TSchema & QueryPagination> {
  const schema = Joi.object<TSchema & QueryPagination>({
    ...schemaToJoin,
    'pagination.after': Joi
      .string()
      .base64({
        paddingRequired: false,
        urlSafe: true,
      }),
    'pagination.before': Joi
      .string()
      .base64({
        paddingRequired: false,
        urlSafe: true,
      }),
    'pagination.first': Joi
      .number()
      .integer()
      .positive(),
    'pagination.last': Joi
      .number()
      .integer()
      .positive(),
    'pagination.offset': Joi
      .number()
      .integer()
      .min(0),
  }).oxor('pagination.before', 'pagination.after', { separator: false })
    .oxor('pagination.before', 'pagination.first', { separator: false })
    .oxor('pagination.after', 'pagination.last', { separator: false });

  return schema;
}

// ============================================================
// Types
type QueryPagination<T extends JSONValue = string> = {
  'pagination.offset'?: number
  'pagination.returnTotal'?: boolean,
  'pagination.sorts'?: [[string, 'asc' | 'desc']],
} & (
  | { 'pagination.after'?: T, 'pagination.first'?: number }
  | { 'pagination.before'?: T, 'pagination.last'?: number }
);

type Pagination<T = string> = {
  offset?: number
  sorts?: [[string, 'asc' | 'desc']],
} & (
  | { after?: T, first?: number }
  | { before?: T, last?: number }
);

type DocumentObject = {
  [key: string]: DocumentValue
};

/**
 * A document is an object stored in a Mongo database
 */

type Document<T extends DocumentObject = DocumentObject> = T & {
  _id: ObjectId,
};

type DocumentValue =
  | string
  | number
  | boolean
  | null
  | undefined
  | Date
  | ObjectId
  | DocumentValue[]
  | DocumentObject;

type ToApiObject<T extends Document | DocumentObject> = Omit<T, '_id'> & {
  id: string,
};

// ============================================================
// Exports
export {
  joinQuerySchema,
};

export type {
  Document,
  DocumentObject,
  DocumentValue,
  Pagination,
  QueryPagination,
  ToApiObject,
};
