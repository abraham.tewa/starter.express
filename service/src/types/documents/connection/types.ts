// ============================================================
// Imports
import type { JSONValue } from '@internal/helpers';

// ============================================================
// Types
type Cursor = string;

type Connection<T extends JSONValue, WithTotal extends boolean> = {
  edges: Array<{
    cursor: Cursor,
    node: T,
  }>,
  pageInfo: {
    endCursor: Cursor,
    hasNextPage: boolean,
    startCursor: Cursor,
  },
  totalCount: WithTotal extends true ? number : undefined,
};

// ============================================================
// Exports
export type {
  Connection,
  Cursor,
};
