// ============================================================
// Imports
import stringify from 'fast-json-stable-stringify';

// ============================================================
// Functions
function toCursor<T>(
  info: T,
): string {
  const cursor = stringify(info);

  return Buffer
    .from(cursor)
    .toString('base64');
}

function fromCursor<T>(
  cursor: string,
): T {
  const str = Buffer.from(cursor, 'base64').toString();
  const info = JSON.parse(str) as T;
  return info;
}

// ============================================================
// Exports
export {
  fromCursor,
  toCursor,
};
