import type { JSONObject } from '@internal/helpers';
import * as curs from './cursor';

import {
  type Connection,
  type Cursor,
} from './types';
import type { Pagination, QueryPagination } from '../types';

// ============================================================
// Class
class ConnectionHandler<
T,
CursorData = T,
ApiObject extends JSONObject = JSONObject,
CheckArguments extends unknown[] = [],
> {
  #checkCursor?: CheckCursor<CursorData, CheckArguments>;

  #toApi: (object: T) => ApiObject;

  #toCursorData?: (object: T) => CursorData;

  constructor({
    checkCursor,
    toApi,
    toCursorData,
  }: {
    checkCursor?: CheckCursor<CursorData, CheckArguments>,
    toApi: (object: T) => ApiObject,
    toCursorData?: (object: T) => CursorData,
  }) {
    this.#checkCursor = checkCursor;
    this.#toCursorData = toCursorData;
    this.#toApi = toApi;
  }

  createCursor(info: T): Cursor {
    const cursorData = this.#toCursorData
      ? this.#toCursorData(info)
      : info as unknown as CursorData;

    return curs.toCursor(cursorData);
  }

  fromRouteQuery(
    apiPagination: QueryPagination,
    ...args: CheckArguments
  ): Pagination<CursorData> {
    const pagination: Pagination<T> = {
      offset: apiPagination['pagination.offset'],
      sorts: apiPagination['pagination.sorts'],
    };

    if ('pagination.after' in apiPagination || 'pagination.first' in apiPagination) {
      const after = apiPagination['pagination.after']
        ? this.getCursorData(apiPagination['pagination.after'], ...args)
        : undefined;

      const first = 'pagination.first' in apiPagination
        ? apiPagination['pagination.first']
        : undefined;

      return {
        ...pagination,
        after,
        first,
      };
    }

    if ('pagination.before' in apiPagination || 'pagination.last' in apiPagination) {
      const before = apiPagination['pagination.before']
        ? this.getCursorData(apiPagination['pagination.before'], ...args)
        : undefined;

      const last = 'pagination.before' in apiPagination
        ? apiPagination['pagination.last']
        : undefined;

      return {
        ...pagination,
        before,
        last,
      };
    }

    return pagination;
  }

  getCursorData(
    cursor: Cursor,
    ...args: CheckArguments
  ): CursorData {
    const info = curs.fromCursor<CursorData>(cursor);

    if (this.#checkCursor) {
      this.#checkCursor(info, ...args);
    }

    return info;
  }

  async toConnection(
    objects: T[],
    hasNextPage: boolean,
    totalCount?: number,
  ): Promise<Connection<ApiObject, boolean>> {
    const edges = objects.map((item) => ({
      cursor: this.createCursor(item),
      node: this.#toApi(item),
    }));

    const connect = {
      edges,
      pageInfo: {
        endCursor: edges.at(-1)?.cursor ?? '',
        hasNextPage,
        startCursor: edges[0]?.cursor ?? '',
      },
      totalCount,
    } as Connection<ApiObject, boolean>;

    return connect;
  }
}

// ============================================================
// Types
type CheckCursor<T, Args extends unknown[]> = (
  cursorInfo: T,
  ...args: Args
) => void;

// ============================================================
// Exports
export default ConnectionHandler;
