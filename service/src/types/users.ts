// ============================================================
// Imports
import type { utils } from '@internal/helpers';
import type { Document } from './documents';

// ============================================================
// Module's constants and variables
enum AccountLockReason {
  toManyAuthFailed = 'toManyAuthFailed',
}

enum UserRole {
  applicationAdmin = 'application-admin',
  user = 'user',
}

enum UserAccountStatus {
  created = 'created',
  validated = 'validated',
}

// ============================================================
// Types
type SessionData = {
  user?: {
    email: string,
    id: string,
    roles: UserRole[],
  }
};

type LockInfo = {
  reason: AccountLockReason,
  since: Date,
  until: Date,
};

type User = {
  account: {
    creationDate: Date,
    email: string,
    emailValidationToken?: string,
    lock?: LockInfo,
    status: UserAccountStatus,
  },
  profile: {
    firstName: string,
    lastName: string,
  },
  roles: UserRole[],
};

type UserAuthInfo = {
  /**
   * Number of attempts failed since the latest
   */
  nbFailedAttempts: number,
} & utils.auth.PasswordHashInfo;

type UserWithPassword = User & {
  auth: UserAuthInfo,
};

type UserJson = Omit<User, 'account'> & {
  account: Omit<User['account'], 'creationDate' | 'lock'> & {
    creationDate: string,
    lock?: Omit<LockInfo, 'since' | 'until'> & {
      since: string,
      until: string,
    }
  }
};

type UserDocument = Document<User>;
type UserWithPasswordDocument = Document<UserWithPassword>;

// ============================================================
// Exports
export {
  AccountLockReason as UserAccountLockReason,
  UserAccountStatus,
  UserRole,
};

export type {
  LockInfo,
  SessionData,
  User,
  UserAuthInfo,
  UserDocument,
  UserJson,
  UserWithPassword,
  UserWithPasswordDocument,
};
