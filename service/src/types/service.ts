// ============================================================
// Imports
import type {
  MongoDbContext,
  ToolingRoutes,
} from '@internal/runtime';
import type { TasksOperation } from '../config';

// ============================================================
// Types
type Job = CheckJob;

type CheckJob = {
  dataType: {
    type: TasksOperation.check;
    userId: string;
  };

  returnType: Promise<undefined>;
};

enum LogCategory {
  api = 'api',
  cli = 'cli',
  user = 'user',
}

type DbContext = MongoDbContext<LogCategory>;

type DevToolsRoutes = ToolingRoutes & {
  documentation: string,
};

// ============================================================
// Exports
export type {
  DbContext,
  DevToolsRoutes,
  Job,
};

export { LogCategory };
