import type { SessionData as AppSessionData } from './users';

declare module 'express-session' {
  interface SessionData {
    user?: AppSessionData['user']
  }
}

export * from './users';
export * from './service';
