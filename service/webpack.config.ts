/* eslint-disable import/no-extraneous-dependencies */
import { composePlugins, withNx } from '@nx/webpack';

// Nx plugins for webpack.
export default composePlugins(withNx(), (config) => config);
