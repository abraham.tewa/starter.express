# Hacking

## Developer environment requirements

* [Docker](https://www.docker.com/) 24.0+

### For MediaWiki dump imports
* [jq](https://jqlang.github.io/jq/) 1.6+
* [yq](https://github.com/kislyuk/yq) 3.2+ (note that xq would be installed in the process)
* [gunzip](https://www.gnu.org/software/gzip/) 1.10+
* [bzip2](https://sourceware.org/bzip2/) 1.08+
