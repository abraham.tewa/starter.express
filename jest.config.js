/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable import/no-extraneous-dependencies */
const { getJestProjects } = require('@nx/jest');
const config = require('./jest.config.base');

module.exports = {
  ...config,
  projects: getJestProjects(),
};
