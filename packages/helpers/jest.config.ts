import type { Config } from 'jest';

const config : Config = {
  displayName: '@internal/helpers',
  preset: '../../jest.config.base.js',
};

export default config;
