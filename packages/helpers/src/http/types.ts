// ============================================================
// Import packages
import type { Response } from 'cross-fetch';

// ============================================================
// Import modules
import type ClientFetchError from './ClientFetchError';
import type {
  FetchResponse,
} from './fetch';
import type {
  JSONObject, JSONValue, Safe, SafePromise,
} from '../types';

// ============================================================
// Types
type Statuses = 200 | 201 | 202 | 204 | 400 | 401 | 403 | 404 | 409 | 412;

type JSONAble =
  | null
  | string
  | number
  | boolean
  | undefined
  | JSONAble[]
  | {
    [key: string]: JSONAble
  };

type ApiDeclaration = {
  [S in Statuses]?: JSONAble
} & {
  req: {
    body?: JSONAble
  }
  route?: string | undefined,
};

type ApiBody<T extends ApiDeclaration, S extends AllStatus<T>[] | AllStatus<T>> =
  S extends AllStatus<T>[]
    ? { [status in AllStatus<T>]: T[status] }[S[number]]
    : (S extends AllStatus<T> ? T[S] : never)
;

type ApiCall<T extends ApiDeclaration, S extends AllStatus<T>[] = AllStatus<T>[]> = {
  [status in AllStatus<T>]: {
    body: T[status]
    res: Response & {
      json(): Promise<T[status]>,
      status: status,
    },
    status: status,
  }
}[S[number]];

type AllStatus<T extends ApiDeclaration> = Exclude<keyof T, 'req' | 'route'>;

type ApiCallStatus<T extends ApiDeclaration, S extends AllStatus<T> | AllStatus<T>[]> =
  S extends AllStatus<T>
    ? ApiCall<T, [S]>
    : (S extends AllStatus<T>[] ? ApiCall<T, S> : never);

type ApiError<T extends JSONObject = JSONObject> = T & {
  code: string,
  message: string,
  status: number,
};

type FetchResult<T extends JSONValue> = {
  res: FetchResponse<T>,
} & (FetchResultSuccess<T> | FetchResultError<T>);

type FetchResultSuccess<T extends JSONValue> = { body: T, error: null, res: FetchResponse<T> };
type FetchResultError<T extends JSONValue> = { body: null, error: ClientFetchError<T>, res: FetchResponse<T> };

type SafeFetch<T extends JSONValue> = Safe<FetchResult<T>>;
type SafeFetchPromise<T extends JSONValue> = SafePromise<FetchResult<T>>;

type SafeApi<T extends ApiDeclaration> = Safe<ApiCall<T>>;
type SafeApiPromise<T extends ApiDeclaration> = Promise<Safe<ApiCall<T>>>;

// ============================================================
// Exports
export type {
  AllStatus,
  ApiBody,
  ApiCall,
  ApiCallStatus,
  ApiDeclaration,
  ApiError,
  FetchResultError,
  FetchResultSuccess,
  JSONAble,
  JSONValue,
  SafeApi,
  SafeApiPromise,
  SafeFetch,
  SafeFetchPromise,
  Statuses,
};
