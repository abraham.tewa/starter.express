// ============================================================
// Imports
import cookie from 'cookie';
import crossFetch, {
  Request,
} from 'cross-fetch';

import type {
  ApiCall,
  ApiDeclaration,
  SafeApiPromise,
} from './types';

// ============================================================
// Functions
async function fetcher<T>(
  url: URL,
  options?: FetchOptions,
  {
    as,
  }: {
    as?: Auth,
  } = {},
): Promise<FetchResponse<T>> {
  const fetchUrl = new URL(url.href);

  if (as && 'username' in as) {
    fetchUrl.username = as.username;
    fetchUrl.password = as.password;
  }

  const body: BodyInit | undefined = typeof options?.body === 'boolean'
  || typeof options?.body === 'object'
  || typeof options?.body === 'number'
    ? JSON.stringify(options.body)
    : options?.body;

  const headers: HeadersInit = typeof options?.body === 'object'
    ? {
      'Content-Type': 'application/json',
    }
    : {};

  if (as && 'token' in as) {
    headers.Authorization = `Bearer ${as.token}`;
  } else if (as && 'cookie' in as) {
    headers.Cookie = Object
      .entries(as.cookie)
      .map(([name, value]) => cookie.serialize(name, value))
      .join('; ');
  }

  const init: RequestInit = {
    ...options,
    body,
    headers: {
      Accept: 'application/json',
      ...(options?.headers || {}),
      ...headers,
    },
  };

  const dbReq = new Request(fetchUrl, init);

  const res = await crossFetch(dbReq);

  return res;
}

fetcher.api = async <T extends ApiDeclaration>(
  url: URL,
  options?: FetchOptions,
  {
    as,
  }: {
    as?: Auth,
  } = {},
): SafeApiPromise<T> => {
  let res: FetchResponse<T>;
  try {
    res = await fetcher(url, options, { as });
  } catch (err) {
    return [null, err as Error];
  }

  let body;
  const contentType = res.headers.get('content-type');

  if (options?.method?.toUpperCase() === 'HEAD') {
    body = undefined;
  } else if (contentType?.startsWith('application/json')) {
    try {
      body = await res.json();
    } catch (err) {
      return [null, err as Error];
    }
  } else {
    body = await res.text();
  }

  return [
    {
      body,
      res,
      status: res.status,
    } as ApiCall<T>,
    null,
  ];
};

// ============================================================
// Types
type Auth = {
  password: string,
  username: string
}
| { token: string }
| { cookie: Record<string, string> };

type FetchOptions<T extends ApiDeclaration = ApiDeclaration> = Omit<RequestInit, 'body'> & {
  body?: T['req']['body'],
};

type FetchResponse<T> = Omit<Response, 'json'> & {
  json(): Promise<T>,
};

// ============================================================
// Exports
export default fetcher;
export type {
  Auth,
  FetchOptions,
  FetchResponse,
};
