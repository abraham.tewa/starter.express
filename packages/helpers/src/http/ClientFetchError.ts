import type { FetchResponse } from './fetch';
import type { JSONValue } from '..';

class ClientFetchError<T extends JSONValue> extends Error {
  res: FetchResponse<T>;

  text: string;

  constructor(res: FetchResponse<T>, text: string) {
    super('Fetch error');
    this.res = res;

    this.text = text;
  }
}

export default ClientFetchError;
