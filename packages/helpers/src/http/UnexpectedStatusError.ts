// ============================================================
// Import modules
import type {
  AllStatus, ApiCall, ApiDeclaration,
} from './types';

// ============================================================
// Class
class UnexpectedStatusError<T extends ApiDeclaration> extends Error {
  expected: AllStatus<T>[];

  constructor(safe: ApiCall<T>, expected: AllStatus<T> | AllStatus<T>[]) {
    const status = safe.status.toString();
    const list: AllStatus<T>[] = typeof expected === 'number'
      ? [expected]
      : expected as AllStatus<T>[];

    super(`Unexpected statuses.\nExpected: ${list.join(', ')}\nReceived: ${status}`);

    this.expected = list;
  }
}

// ============================================================
// Exports
export default UnexpectedStatusError;
