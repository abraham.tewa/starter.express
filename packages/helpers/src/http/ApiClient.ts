// ============================================================
// Imports
import fetcher from './fetch';
import type {
  Auth,
  FetchOptions,
} from './fetch';
import type {
  ApiCall,
  ApiDeclaration,
  SafeApiPromise,
  Statuses,
} from './types';

// ============================================================
// Class
class ApiClient {
  readonly parent?: ApiClient;

  #as?: Auth;

  #url: URL;

  constructor(
    url: URL | string,
    as?: Auth,
    parent?: ApiClient,
  ) {
    this.#as = as;
    this.#url = new URL(typeof url === 'string' ? url : url.href);
    this.parent = parent;
  }

  cloneClient(as?: Auth | null): ApiClient {
    return new ApiClient(
      this.#url,
      as === null ? undefined : as ?? this.#as,
    );
  }

  async get<T extends ApiDeclaration>(
    pathname: string,
    init?: Omit<FetchOptions<T>, 'method'>,
  ): Promise<ApiCall<T>> {
    const [res, err] = await this.safeGet(pathname, init);

    if (err) {
      throw err;
    }

    return res;
  }

  async post<T extends ApiDeclaration>(
    pathname: T['route'] extends string ? T['route'] : string,
    init?: Omit<FetchOptions<T>, 'method'>,
  ): Promise<ApiCall<T>> {
    const [res, err] = await this.safePost(pathname, init);

    if (err) {
      throw err;
    }

    return res;
  }

  async safeDelete<T extends ApiDeclaration>(
    pathname: string,
    init?: Omit<FetchOptions, 'method'>,
  ): SafeApiPromise<T> {
    return this.safeFetch<T>(
      pathname,
      {
        ...(init ?? {}),
        method: 'DELETE',
      },
    );
  }

  async safeFetch<T extends ApiDeclaration>(
    pathname: string,
    init?: FetchOptions,
  ): SafeApiPromise<T> {
    const reqUrl = new URL(pathname, this.#url);

    const res = await fetcher.api<T>(
      reqUrl,
      {
        ...init,
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          ...init?.headers,
        },
      },
      {
        as: this.#as,
      },
    );

    return res;
  }

  async safeGet<T extends ApiDeclaration>(
    pathname: string,
    init?: Omit<FetchOptions<T>, 'method' | 'body'>,
  ): SafeApiPromise<T> {
    const safe = await this.safeFetch<T>(
      pathname,
      {
        ...(init ?? {}),
        method: 'GET',
      },
    );

    return safe;
  }

  async safeHead<T extends Statuses>(
    pathname: string,
    init?: Omit<FetchOptions, 'method'>,
  ): SafeApiPromise<HeadApiDeclaration<T>> {
    const safe = await this.safeFetch<HeadApiDeclaration<T>>(
      pathname,
      {
        ...(init ?? {}),
        method: 'HEAD',
      },
    );

    return safe;
  }

  async safePatch<T extends ApiDeclaration>(
    pathname: string,
    init?: Omit<FetchOptions, 'method'>,
  ): SafeApiPromise<T> {
    const safe = await this.safeFetch<T>(
      pathname,
      {
        ...(init ?? {}),
        method: 'PATCH',
      },
    );

    return safe;
  }

  async safePost<T extends ApiDeclaration>(
    pathname: string,
    init?: Omit<FetchOptions<T>, 'method'>,
  ): SafeApiPromise<T> {
    const safe = await this.safeFetch<T>(
      pathname,
      {
        ...(init ?? {}),
        method: 'POST',
      },
    );

    return safe;
  }

  async safePut<T extends ApiDeclaration>(
    pathname: string,
    init?: InitOptions<T>,
  ): SafeApiPromise<T> {
    const safe = this.safeFetch<T>(
      pathname,
      {
        ...(init ?? {}),
        method: 'PUT',
      },
    );

    return safe;
  }

  get url(): URL {
    return new URL(this.#url.href);
  }
}

// ============================================================
// Types

type HeadApiDeclaration<T extends Statuses> = Record<T, never> & {
  req: {
    body: never,
  }
  route: string | undefined,
};

type InitOptions<T extends ApiDeclaration> = T['req']['body'] extends undefined
  ? Omit<FetchOptions, 'method'>
  : Omit<FetchOptions, 'method' | 'body'> & {
    body: T['req']['body']
  };

// ============================================================
// Exports
export default ApiClient;
export type {
  HeadApiDeclaration,
};
