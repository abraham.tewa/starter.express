// ============================================================
// Types
type HttpType = undefined | object | string;

// ============================================================
// Exports
export type {
  HttpType,
};
