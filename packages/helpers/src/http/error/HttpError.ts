import type { Response } from 'express';
import type { JSONAbleObject, JSONObject } from '../../types';

class HttpError extends Error {
  code: string;

  data?: JSONAbleObject;

  status: number;

  constructor(
    status: number,
    code: string,
    message: string,
    data?: JSONAbleObject,
  ) {
    super(message);

    this.status = status;
    this.code = code;
    this.data = data;
  }

  json(): JSONObject {
    return {
      ...(this.data || {}),
      code: this.code,
      message: this.message,
      status: this.status,
    };
  }

  send(res: Response) {
    res
      .status(this.status)
      .json(this.json());
  }
}

export default HttpError;
