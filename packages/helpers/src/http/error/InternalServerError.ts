// ============================================================
// Imports
import HttpError from './HttpError';
import type { JSONAbleObject } from '../../types';

// ============================================================
// Class
class InternalServerError extends HttpError {
  static readonly code = 'InternalServerError';

  static readonly status = 500;

  readonly originalError?: unknown;

  constructor(message = 'Internal Server Error', originalError?: unknown, data?: JSONAbleObject) {
    super(InternalServerError.status, InternalServerError.code, message, data);

    console.log(originalError);
    this.originalError = originalError;
  }
}

// ============================================================
// Exports
export default InternalServerError;
