// ============================================================
// Import modules
import Forbidden from './Forbidden';
import InternalServerError from './InternalServerError';
import InvalidRequest from './InvalidRequest';
import NotFound from './NotFound';
import Unauthorized from './Unauthorized';

import type HttpError from './HttpError';

// ============================================================
// Module's constants and variables
const errors: Record<string, new(msg?: string) => HttpError > = {
  400: InvalidRequest,
  401: Unauthorized,
  403: Forbidden,
  404: NotFound,
  500: InternalServerError,
};

// ============================================================
// Functions
function createError(status: number, message?: string): HttpError | undefined {
  const Constructor = errors[status];

  // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
  if (!Constructor) {
    return undefined;
  }

  return new Constructor(message);
}

// ============================================================
// Exports
export { default as Forbidden } from './Forbidden';
export { default } from './HttpError';
export { default as HttpError } from './HttpError';
export { default as InternalServerError } from './InternalServerError';
export { default as InvalidRequest } from './InvalidRequest';
export {
  createError as create,
};
export { default as NotFound } from './NotFound';
export { default as Unauthorized } from './Unauthorized';
