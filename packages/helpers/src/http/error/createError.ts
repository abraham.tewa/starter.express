// ============================================================
// Imports
import {
  Forbidden,
  InternalServerError,
  InvalidRequest,
  NotFound,
  Unauthorized,
} from '.';
import type { HttpError } from '.';

// ============================================================
// Module's constants and variables
const statusMap: Record<number, ErrorBuilder | undefined> = {
  [Forbidden.status]: (message?: string) => new Forbidden(message),
  [InternalServerError.status]: (message?: string) => new InternalServerError(message),
  [InvalidRequest.status]: (message?: string) => new InvalidRequest(message),
  [NotFound.status]: (message?: string) => new NotFound(message),
  [Unauthorized.status]: (message?: string) => new Unauthorized(message),
};

const codeMap: Record<string, ErrorBuilder | undefined> = {
  [Forbidden.code]: statusMap[Forbidden.status],
  [InternalServerError.code]: statusMap[InternalServerError.status],
  [InvalidRequest.code]: statusMap[InvalidRequest.status],
  [NotFound.code]: statusMap[NotFound.status],
  [Unauthorized.code]: statusMap[Unauthorized.status],
};

// ============================================================
// Functions
/**
 * Create an HttpError from a http status.
 * Use this function to dynamically generate errors without importing the exact HttpError class.
 */
function createHttpError(statusOrCode: number | string, message: string): HttpError {
  const errorBuilder = typeof statusOrCode === 'string'
    ? codeMap[statusOrCode]
    : statusMap[statusOrCode];

  if (!errorBuilder) {
    const errorMessage = typeof statusOrCode === 'string'
      ? `Invalid code "${statusOrCode}"`
      : `Invalid status "${statusOrCode}"`;
    throw new Error(errorMessage);
  }

  return errorBuilder(message);
}

// ============================================================
// Types
type ErrorBuilder = (message?: string) => HttpError;

// ============================================================
// Exports
export default createHttpError;
