import HttpError from './HttpError';
import type { JSONAbleObject } from '../../types';

class NotFound extends HttpError {
  static readonly code = 'NotFound';

  static readonly status = 404;

  constructor(message = 'Resource not found', data?: JSONAbleObject) {
    super(NotFound.status, NotFound.code, message, data);
  }
}

export default NotFound;
