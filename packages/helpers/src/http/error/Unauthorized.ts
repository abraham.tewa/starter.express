// ============================================================
// Imports
import HttpError from './HttpError';
import type { JSONAbleObject } from '../../types';

// ============================================================
// Class
class Unauthorized extends HttpError {
  static readonly code = 'Unauthorized';

  static defaultMessage = 'The access to the given resource is not allowed';

  static readonly status = 401;

  constructor(message = Unauthorized.defaultMessage, data?: JSONAbleObject) {
    super(Unauthorized.status, Unauthorized.code, message, data);
  }
}

// ============================================================
// Exports
export default Unauthorized;
