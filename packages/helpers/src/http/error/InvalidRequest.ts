import HttpError from './HttpError';
import type { JSONAbleObject } from '../../types';

class InvalidRequest extends HttpError {
  static readonly code = 'InvalidRequest';

  static readonly status = 400;

  constructor(message = 'The request is not valid', data?: JSONAbleObject) {
    super(InvalidRequest.status, InvalidRequest.code, message, data);
  }
}

export default InvalidRequest;
