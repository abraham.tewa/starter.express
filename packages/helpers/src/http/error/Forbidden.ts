// ============================================================
// Imports
import HttpError from './HttpError';
import type { JSONAbleObject } from '../../types';

// ============================================================
// Class
class Forbidden extends HttpError {
  static readonly code = 'Forbidden';

  static readonly status = 403;

  constructor(message = 'Not enough rights to perform the action', data?: JSONAbleObject) {
    super(Forbidden.status, Forbidden.code, message, data);
  }
}

// ============================================================
// Exports
export default Forbidden;
