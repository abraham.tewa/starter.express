// eslint-disable-next-line @typescript-eslint/no-explicit-any
type AnyFunction = (...args: any[]) => any;

type EmptyObject = Record<never, never>;

type JSONValue =
| string
| number
| boolean
| null
| JSONObject
| JSONArray;

type JSONObject = {
  [x: string]: JSONValue;
};
type JSONArray = JSONValue[];

type JSONAble =
  | null
  | string
  | number
  | boolean
  | undefined
  | JSONAble[]
  | JSONAbleObject;

type JSONAbleObject = {
  [key: string]: JSONAble
};

type DeepPartial<T> = T extends object ? {
  [P in keyof T]?: DeepPartial<T[P]>;
} : T;

type OmitFirstParameter<
  T extends AnyFunction,
// eslint-disable-next-line @typescript-eslint/no-explicit-any
> = T extends (a: any, ...args: infer R) => unknown
  ? (...args: R) => ReturnType<T>
  : never;

type OmitFirstTwoParameters<
  T extends AnyFunction,
// eslint-disable-next-line @typescript-eslint/no-explicit-any
> = T extends (a: any, b: any, ...args: infer R) => unknown
  ? (...args: R) => ReturnType<T>
  : never;

type Safe<T, E extends Error = Error> = [T, null] | [null, E];

type SafePromise<T, E extends Error = Error> = Promise<Safe<T, E>>;

type EnvironmentVariables = Record<string, string | undefined>;

export type {
  AnyFunction,
  DeepPartial,
  EmptyObject,
  EnvironmentVariables,
  JSONAble,
  JSONAbleObject,
  JSONObject,
  JSONValue,
  OmitFirstParameter,
  OmitFirstTwoParameters,
  Safe,
  SafePromise,
};
