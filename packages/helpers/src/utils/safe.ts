// ============================================================
// Imports
import type { Safe, SafePromise } from '../types';

// ============================================================
// Functions
/**
 * Call a function and return a safe object
 */
function safe<T, E extends Error = Error>(
  cb: () => T,
): Safe<T, E> {
  try {
    const val = cb();

    return [val, null];
  } catch (err) {
    return [null, err as E];
  }
}

function safer<A extends unknown[], T, E extends Error = Error>(
  cb: (...args: A) => T,
): (...args: A) => Safe<T, E> {
  return (...args: A) => safe(() => cb(...args));
}

async function safePromise<T, E extends Error = Error>(
  cb: () => Promise<T>,
): SafePromise<T, E> {
  try {
    const val = await cb();

    return [val, null];
  } catch (err) {
    return [null, err as E];
  }
}

function saferPromise<A extends unknown[], T, E extends Error = Error>(
  cb: (...args: A) => Promise<T>,
): (...args: A) => SafePromise<T, E> {
  return (...args: A) => safePromise(() => cb(...args));
}

// ============================================================
// Exports
export {
  safe,
  safePromise,
  safer,
  saferPromise,
};
