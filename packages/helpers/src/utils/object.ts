// ============================================================
// Imports
import cloneDeep from 'clone-deep';

// ============================================================
// Functions
function isEmpty(object: object): object is Record<never, never> {
  return Object.keys(object).length === 0;
}

function mapKeys<K extends string | number | symbol, V, T>(
  object: Record<K, V>,
  cb: (value: V, key: K, object: Record<K, V>) => T,
): Record<K, T> {
  const newEntries = Object
    .entries(object)
    .map(
      ([name, value]) => [name, cb(value as V, name as K, object)] as [K, T],
    );

  const newObject = Object.fromEntries(newEntries);

  return newObject as Record<K, T>;
}

// ============================================================
// Exports
export {
  cloneDeep,
  isEmpty,
  mapKeys,
};
