import fs from 'fs/promises';
import dotenv from 'dotenv';
import { reduceAsync } from './array';
import type { EnvironmentVariables } from '../types';

async function load({
  files,
  load: loadEnv,
  vars,
}:{
  /**
   * List of files containing environment variables.
   * Files are expected to be of two kind:
   *  - ".env" files
   *    They will be loaded through dotenv package (https://www.npmjs.com/package/dotenv)
   *  - ".json" files
   *    They will be considered as a "custom environment variable" files from
   *    node "config" package
   *    See: https://github.com/node-config/node-config/wiki/Environment-Variables#custom-environment-variables
   */
  files?: string[],

  /**
   * Function that will return a set of environment variables.
   * Values returned here will override values from "files" parameters.
   */
  load?: () => (EnvironmentVariables | Promise<EnvironmentVariables>),

  /**
   * List of environment variables to load.
   * Variables defined here will override values loaded from "files" and "load" parameters.
   */
  vars?: Array<string | CustomEnvironmentVariablesContent>,
}): Promise<EnvironmentVariables> {
  let envVars: EnvironmentVariables = {};

  if (files?.length) {
    envVars = await reduceAsync(
      files,
      async (acc, file) => {
        const newVars = await loadFile(file);

        return {
          ...acc,
          ...newVars,
        };
      },
      envVars,
    );
  }

  if (loadEnv) {
    envVars = {
      ...await loadEnv(),
    };
  }

  if (vars?.length) {
    envVars = vars.reduce<EnvironmentVariables>(
      (acc, env) => {
        if (typeof env === 'string') {
          acc[env] = process.env[env];
          return acc;
        }

        return {
          ...acc,
          ...loadCustomEnvironmentVariable(env),
        };
      },
      envVars,
    );
  }

  return envVars;
}

async function loadFile(file: string, encoding: BufferEncoding = 'utf-8'): Promise<EnvironmentVariables> {
  const content = await fs.readFile(file, { encoding });

  try {
    const customEnvVar = JSON.parse(content) as CustomEnvironmentVariablesContent;
    const envVars = loadCustomEnvironmentVariable(customEnvVar);
    return envVars;
  } catch (err) {
    //
  }

  const envVars = dotenv.parse(content);

  return envVars;
}

function loadCustomEnvironmentVariable(content: CustomEnvironmentVariablesContent): EnvironmentVariables {
  const envVars = Object
    .values(content)
    .reduce<EnvironmentVariables>(
    (acc, envName) => {
      if (typeof envName === 'string') {
        const env = process.env[envName];
        acc[envName] = env;
        return acc;
      }

      return {
        ...acc,
        ...loadCustomEnvironmentVariable(envName),
      };
    },
    {},
  );

  return envVars;
}

// ============================================================
// Types
type CustomEnvironmentVariablesContent = {
  [key: string]: string | CustomEnvironmentVariablesContent
};

// ============================================================
// Exports
export {
  load,
};

export type {
  CustomEnvironmentVariablesContent,
};
