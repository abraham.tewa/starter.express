function fromEntries<K, V>(entries: [K, V][]): Map<K, V> {
  const map = new Map<K, V>();

  entries.forEach(([k, v]) => {
    map.set(k, v);
  });

  return map;
}

export {
  fromEntries,
};
