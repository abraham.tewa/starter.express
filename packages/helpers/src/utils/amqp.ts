// ============================================================
// Imports
import amqplib, {
  type Channel,
  type Connection,
  type ConsumeMessage,
  type Options,
  type Replies,
} from 'amqplib';
import Joi from 'joi';
import { createTimeoutPromise } from './promise';
import type { AnyFunction, JSONAble } from '../types';

// ============================================================
// Class
async function createConnection<Ctx, AsyncApi extends AsyncApiDeclaration>(
  connect: ConnectInfo | string,
  createContext: ContextBuilder<Ctx, AsyncApi>,
  parser: (content: Buffer) => (AsyncMessage | Promise<AsyncMessage>) = jsonParser,
): Promise<ConnectionWithContext<Ctx, AsyncApi>> {
  const connectString = getConnectString(connect, [], true);

  const connection = await amqplib.connect(connectString);

  return withContext(connection, createContext, parser);
}

async function isAccessible(
  connect: ConnectInfo | string,
  /**
   * Time (in milliseconds) to way before considering the instance
   * not accessible
   */
  timeoutMS: number = 500,
) {
  const connectString = getConnectString(connect, [], true);
  let connection: Connection | false = false;

  try {
    connection = await Promise.race([
      amqplib.connect(connectString),
      createTimeoutPromise<false>(false, timeoutMS),
    ]);

    return Boolean(connection);
  } catch (err) {
    return false;
  } finally {
    if (connection) {
      await connection.close();
    }
  }
}

function getConnectString(
  connect: ConnectInfo | string,
  override: ConnectInfo[] = [],
  includeAuthInfo: boolean = false,
): string {
  if (typeof connect === 'object') {
    const { url, ...connectInfo } = connect;
    const connectURL = new URL('amqp://localhost:5672');

    return getConnectString(
      connectURL.href,
      [
        connectInfo,
        ...override,
      ],
      includeAuthInfo,
    );
  }

  const config = mergeConnectInfo(...override);

  const connectURL = new URL(connect);

  connectURL.password = config.password ?? connectURL.password;
  connectURL.host = config.hostname ?? connectURL.host;
  connectURL.port = config.port?.toString() ?? connectURL.port;
  connectURL.protocol = config.protocol ?? connectURL.protocol;
  connectURL.username = config.username ?? connectURL.username;

  let vhost: string = config.vhost ?? '';

  if (config.vhostPrefix && vhost) {
    vhost = `${config.vhostPrefix}${vhost}`;
  }

  connectURL.pathname = vhost;

  if (!includeAuthInfo) {
    connectURL.password = '';
    connectURL.username = '';
  }

  return connectURL.href;
}

function mergeConnectInfo(...args: ConnectInfo[]): ConnectInfo {
  const info = Object.assign(
    {},
    ...args,
  ) as ConnectInfo;

  return info;
}

function withContext<Ctx, AsyncApi extends AsyncApiDeclaration>(
  connection: Connection,
  createContext: ContextBuilder<Ctx, AsyncApi>,
  parser: (content: Buffer) => (AsyncMessage | Promise<AsyncMessage>) = jsonParser,
): ConnectionWithContext<Ctx, AsyncApi> {
  const cacheFunction: {
    [key in keyof Connection]?: AnyFunction
  } = {};

  const proxy = new Proxy(
    connection as unknown as ConnectionWithContext<Ctx, AsyncApi>,
    {
      get(target, prop: keyof Connection) {
        switch (prop) {
          case 'createChannel': {
            return async (...args: Parameters<Connection['createChannel']>) => {
              const channel = await connection.createChannel(...args);
              return buildChannelProxy<Ctx, AsyncApi>(
                target,
                channel,
                createContext,
                parser,
              );
            };
          }

          case 'createConfirmChannel': {
            return async () => {
              const channel = await connection.createConfirmChannel();
              return buildChannelProxy<Ctx, AsyncApi>(target, channel, createContext, parser);
            };
          }

          default: {
            const value = target[prop];

            if (typeof value !== 'function') {
              return value;
            }

            if (!(prop in cacheFunction)) {
              cacheFunction[prop] = value.bind(target);
            }

            return cacheFunction[prop];
          }
        }
      },
    },
  );

  return proxy;
}

function buildChannelProxy<Ctx, AsyncApi extends AsyncApiDeclaration>(
  connection: ConnectionWithContext<Ctx, AsyncApi>,
  channel: Channel,
  createContext: ContextBuilder<Ctx, AsyncApi>,
  parser: (content: Buffer) => (AsyncMessage | Promise<AsyncMessage>),
): ChannelWithContext<Ctx, AsyncApi> {
  const cacheFunction: {
    [key in keyof Channel]?: AnyFunction
  } = {};

  const proxy = new Proxy(
    channel as unknown as ChannelWithContext<Ctx, AsyncApi>,
    {
      get(target, prop: keyof ChannelWithContext<Ctx, AsyncApi>) {
        switch (prop) {
          case 'connection': {
            return connection;
            break;
          }

          case 'consume': {
            return (queue: QueueNames<AsyncApi>, ...args: ConsumeArgs<Ctx, AsyncApi[typeof queue]>) => {
              const last = args.at(-1);

              const middlewares = typeof last === 'function'
                ? args as Middleware<Ctx, AsyncApi[typeof queue]>[]
                : args.slice(0, -1) as Middleware<Ctx, AsyncApi[typeof queue]>[];

              const options = typeof last === 'function'
                ? undefined
                : last as Options.Consume;

              return channel.consume(
                queue,
                (msg: ConsumeMessage | null) => {
                  void consume(middlewares, createContext, msg, parser);
                },
                options,
              );
            };
          }

          case 'sendToQueue': {
            return (queue: QueueNames<AsyncApi>, content: AsyncMessage, options?: Options.Publish) => {
              const buffer = Buffer.from(JSON.stringify(content));
              return channel.sendToQueue(queue, buffer, options);
            };
          }

          case 'withQueue': {
            return (queue: QueueNames<AsyncApi>) => withQueue<Ctx, AsyncApi>(proxy, queue);
          }

          default: {
            const value = target[prop];

            if (typeof value !== 'function') {
              return value;
            }

            if (!(prop in cacheFunction)) {
              cacheFunction[prop] = value.bind(target);
            }

            return cacheFunction[prop];
          }
        }
      },
    },
  );

  return proxy;
}

async function consume<Ctx, AsyncApi extends AsyncApiDeclaration>(
  middlewares: Middleware<Ctx, AllQueues<AsyncApi>>[],
  createContext: ContextBuilder<Ctx, AsyncApi>,
  msg: ConsumeMessage | null,
  parser: (content: Buffer) => (AsyncMessage | Promise<AsyncMessage>),
) {
  let previousError: unknown;
  let nextCalled = false;

  const next = (err?: unknown) => {
    nextCalled = true;

    if (err) {
      previousError = err;
    }
  };

  let parsedMessage: ParsedConsumeMessageSimple<AllQueues<AsyncApi>> | null = null;

  try {
    parsedMessage = !msg
      ? null
      : {
        ...msg,
        content: await parser(msg.content) as AsyncApi['consume'],
      };
  } catch (err) {
    previousError = err;
  }

  for (let m = 0; m < middlewares.length; m += 1) {
    const cb = middlewares[m];

    try {
      // If an error has been thrown, we skill all non-errors middlewares
      if (previousError && cb.length >= 4) {
        // eslint-disable-next-line no-await-in-loop
        const ctx = await createContext(parsedMessage);
        // eslint-disable-next-line no-await-in-loop
        await cb(parsedMessage, ctx, next, previousError);
      } else if (!previousError) {
        // eslint-disable-next-line no-await-in-loop
        const ctx = await createContext(parsedMessage);
        // eslint-disable-next-line no-await-in-loop
        await cb(parsedMessage, ctx, next);
      } else {
        nextCalled = true;
      }
    } catch (err) {
      previousError = err;
    }

    // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
    if (!nextCalled) {
      return;
    }
  }
}

function withQueue<
  Ctx,
  AsyncApi extends AsyncApiDeclaration,
>(
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  channel: ChannelWithContext<Ctx, AsyncApi>,
  queueName: QueueNames<AsyncApi>,
): WithQueue<Ctx, AsyncApi[typeof queueName]> {
  return {
    assert: (options?: Options.AssertQueue) => channel.assertQueue(queueName as string, options),
    bind: (
      source: string,
      pattern: string,
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      args?: any,
    ) => channel.bindQueue(queueName as string, source, pattern, args),
    check() {
      return channel.checkQueue(queueName as string);
    },
    consume(...middlewares: ConsumeArgs<Ctx, AsyncApi[typeof queueName]>) {
      const res = channel.consume(queueName, ...middlewares);

      return res;
    },
    delete: (
      options?: Options.DeleteQueue,
    ) => channel.deleteQueue(queueName as string, options),
    name: queueName,
    purge: () => channel.purgeQueue(queueName as string),
    send: (
      content: AsyncApi[typeof queueName]['produce'],
    ) => channel.sendToQueue(queueName, content),
    unbind: (
      source: string,
      pattern: string,
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      args?: any,
    ) => channel.unbindQueue(queueName as string, source, pattern, args),
  };
}

function jsonParser<Content>(content: Buffer): Content {
  const data = content.toString();
  return JSON.parse(data) as Content;
}

// ============================================================
// Types
type AsyncMessage = JSONAble;

type QueueNames<T extends AsyncApiDeclaration> = Extract<keyof T, string>;

type AllQueues<T extends AsyncApiDeclaration> = {
  [key in QueueNames<T>]: T[key]
}[QueueNames<T>];

type AsyncApiDeclaration = {
  [key: string]: AsyncQueue,
};

type AsyncQueue = {
  consume: AsyncMessage,
  produce: AsyncMessage,
};

/**
 * channel.sendToQueue callback types
 */
type SendToQueue<AsyncApi extends AsyncApiDeclaration> = {
  [key in QueueNames<AsyncApi>]: (
    queue: key,
    content: AsyncApi[key]['produce'],
  ) => boolean
}[QueueNames<AsyncApi>];

type ConsumeCb<Ctx, AsyncApi extends AsyncApiDeclaration> = (
  queue: string,
  ...middlewares: ConsumeArgs<Ctx, AllQueues<AsyncApi>>
) => Promise<Replies.Consume>;

type ContextBuilder<Ctx, AsyncApi extends AsyncApiDeclaration> = (
  message: ParsedConsumeMessageSimple<AllQueues<AsyncApi>> | null
) => (Ctx | Promise<Ctx>);

type ConnectionWithContext<Ctx, AsyncApi extends AsyncApiDeclaration> =
  Omit<Connection, 'createChannel' | 'createConfirmChannel'> & {
    createChannel: () => Promise<ChannelWithContext<Ctx, AsyncApi>>,
    createConfirmChannel: () => Promise<ConfirmChannelWithContext<Ctx, AsyncApi>>,
  };

type ChannelWithContext<Ctx, AsyncApi extends AsyncApiDeclaration> =
  Omit<Channel, 'consume' | 'sendToQueue'> & {
    connection: ConnectionWithContext<Ctx, AsyncApi>,
    consume: ConsumeCb<Ctx, AsyncApi>,
    sendToQueue: SendToQueue<AsyncApi>,
    withQueue: <T extends AsyncQueue>(queue: string) => WithQueue<Ctx, T>,
  };

type ConfirmChannelWithContext<Ctx, AsyncApi extends AsyncApiDeclaration> =
  Omit<Channel, 'consume' | 'sendToQueue'> & {
    consume: ConsumeCb<Ctx, AsyncApi>;
    sendToQueue: SendToQueue<AsyncApi>,
  };

type Middleware<Ctx, Queue extends AsyncQueue> = (
  msg: ParsedConsumeMessageSimple<Queue> | null,
  ctx: Ctx,
  next: () => void,
  err?: unknown,
) => (void | Promise<void>);

// type MiddlewareSimple<Ctx, Message extends AsyncMessage> = (
//   msg: ParsedConsumeMessageSimple<Message> | null,
//   ctx: Ctx,
//   next: () => void,
//   err?: unknown,
// ) => (void | Promise<void>);

/**
 * Args used for Consume method
 * The args must have at least one middleware
 * The options are optionnal
 */

type ConsumeArgs<Ctx, Queue extends AsyncQueue> =
  [Middleware<Ctx, Queue>, ...Middleware<Ctx, Queue>[]] |
  [Middleware<Ctx, Queue>, ...Middleware<Ctx, Queue>[], Options.Consume];

type ParsedConsumeMessageSimple<Queue extends AsyncQueue> = Omit<ConsumeMessage, 'content'> & {
  content: Queue['consume'],
};

type ConsumedMessages<Queue extends AsyncQueue> = Omit<ConsumeMessage, 'content'> & {
  content: Queue['consume'],
};

type ConnectInfo = {
  hostname?: string,
  password?: string,
  port?: number,
  protocol?: string,
  url?: string,
  username?: string,
  vhost?: string,
  vhostPrefix?: string,
};

const Schema = Joi.object<ConnectInfo>({
  hostname: Joi.string().hostname().optional(),
  password: Joi.string().optional(),
  port: Joi.number().port().optional(),
  protocol: Joi.string().allow('amqp').optional(),
  url: Joi.string().optional(),
  username: Joi.string().optional(),
  vhost: Joi.string().optional(),
  vhostPrefix: Joi.string().optional(),
});

type WithQueue<Ctx, Queue extends AsyncQueue> = {
  assert: (options?: Options.AssertQueue) => ReturnType<Channel['assertQueue']>,
  bind: (
    source: string,
    pattern: string,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    args?: any,
  ) => ReturnType<Channel['bindQueue']>,
  check: () => ReturnType<Channel['checkQueue']>,
  consume: (
    ...middlewares: ConsumeArgs<Ctx, Queue>
  ) => ReturnType<Channel['consume']>,
  delete: (
    options?: Options.DeleteQueue,
  ) => ReturnType<Channel['deleteQueue']>,
  name: string,
  purge: () => ReturnType<Channel['purgeQueue']>,
  send: (
    content: Queue['produce'],
  ) => ReturnType<Channel['sendToQueue']>,
  unbind: (
    source: string,
    pattern: string,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    args?: any,
  ) => ReturnType<Channel['unbindQueue']>,
};

// ============================================================
// Exports
export {
  Schema,
  createConnection,
  getConnectString,
  isAccessible,
  withContext,
};

export type {
  AllQueues,
  AsyncApiDeclaration,
  AsyncMessage,
  ChannelWithContext,
  ConnectInfo,
  ConnectionWithContext,
  ConsumeCb,
  ConsumedMessages,
  QueueNames,
  WithQueue,
};
