// ============================================================
// Imports
import crypto from 'node:crypto';
import PromiseHolder from './PromiseHolder';
import { randomString, toBase64 } from './string';

// ============================================================
// Module's constants and variables
const encryptionParameters: AlgorithmInfo['parameters'] = {
  N: 2 ** 15,
  maxmem: 50 * 1024 * 1024,
  p: 3,
  r: 8,
};

const HASH_ALGORITHM = 'scrypt';

// ============================================================
// Functions
async function generateAuthInfo(password: string): Promise<PasswordHashInfo> {
  const salt = randomString(24, 'base64');

  const hashInfo: PasswordHashInfo = {
    algorithm: {
      parameters: encryptionParameters,
      type: HASH_ALGORITHM,
    },
    passwordHash: await hashPassword(password, salt),
    passwordSalt: salt,
  };

  return hashInfo;
}

async function hashPassword(
  password: string,
  salt: string | Buffer,
): Promise<string> {
  const promise = new PromiseHolder<Buffer>();

  crypto.scrypt(
    password,
    salt,
    64,
    encryptionParameters,
    promise.nodeCallback,
  );

  const hashBuffer = await promise;

  return toBase64(hashBuffer);
}

/**
 * Is the given clear-text password matching the passwordHash information ?
 */
async function isValidPassword(
  /**
   * Clear text password to test
   */
  passwordClearText: string,
  passHashInfo: PasswordHashInfo,
) {
  if ((passHashInfo.algorithm.type as string) !== HASH_ALGORITHM) {
    return false;
  }

  const { N, p, r } = passHashInfo.algorithm.parameters;

  if (N !== encryptionParameters.N || p !== encryptionParameters.p || r !== encryptionParameters.r) {
    return false;
  }

  const hashedPassword = await hashPassword(
    passwordClearText,
    passHashInfo.passwordSalt,
  );

  return hashedPassword === passHashInfo.passwordHash;
}

// ============================================================
// Types
type PasswordHashInfo = {
  algorithm: AlgorithmInfo
  passwordHash: string,
  passwordSalt: string,
};

type AlgorithmInfo = {
  parameters: {
    N: number,
    maxmem: number,
    p: number,
    r: number,
  },
  type: typeof HASH_ALGORITHM,
};

// ============================================================
// Exports
export type {
  PasswordHashInfo,
};

export {
  generateAuthInfo,
  isValidPassword,
};
