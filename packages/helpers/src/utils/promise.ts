import { onTimeout } from './PromiseHolder';

async function createTimeoutPromise<T>(
  value: T,
  duration: number,
): Promise<T> {
  return new Promise<T>((resolve) => {
    setTimeout(
      () => { resolve(value); },
      duration,
    );
  });
}

function isPromiseLike<T>(promise: unknown): promise is { then: (val: T) => PromiseLike<unknown> } {
  if (!promise) {
    return false;
  }

  if (typeof promise !== 'object') {
    return false;
  }

  return 'then' in promise && typeof promise.then !== 'function';
}

async function wait(duration: number): Promise<void> {
  await createTimeoutPromise(undefined, duration);
}

async function waitUntil<T>(
  cb: () => WaitResult<T> | Promise<WaitResult<T>>,
  maxTries: number = 5,

  /**
   * Interval between two tries (in ms)
   */
  interval: number = 100,
): Promise<WaitResult<T>> {
  let nbTries = 0;

  // eslint-disable-next-line no-constant-condition
  while (true) {
    // eslint-disable-next-line no-await-in-loop
    const [found, value] = await cb();

    if (found) {
      return [found, value];
    }

    nbTries += 1;

    if (nbTries >= maxTries) {
      return [false, null];
    }

    // eslint-disable-next-line no-await-in-loop
    await wait(interval);
  }
}

// ============================================================
// Types
type WaitResult<T> = [true, T] | [false, null];

// ============================================================
// Exports
export {
  createTimeoutPromise,
  isPromiseLike,
  onTimeout,
  wait,
  waitUntil,
};
