import PromiseHolder from './PromiseHolder';

describe('PromiseHolder', () => {
  it('act as a promise', async () => {
    const timeout = 100;
    const expectedValue = Symbol('value');

    const startDate = new Date();

    const p = new PromiseHolder<symbol>((resolve) => {
      setTimeout(() => { resolve(expectedValue); }, timeout);
    });

    const value = await p;
    const endDate = new Date();

    const duration = endDate.getTime() - startDate.getTime();

    expect(value).toBe(expectedValue);
    expect(duration).toBeGreaterThanOrEqual(timeout);
    expect(duration).toBeLessThanOrEqual(timeout * 1.1);
  });
});
