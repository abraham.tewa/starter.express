// ============================================================
// Functions
/**
 * Reduce function with async callbacks.
 */
async function reduceAsync<T, U>(
  array: T[],
  callbackfn: (previousValue: U, currentValue: T, currentIndex: number, array: T[]) => Promise<U>,
  initialValue: U,
): Promise<U> {
  let previousValue: U = initialValue;

  for (let i = 0; i < array.length; i += 1) {
    const currentValue = array[i];
    // eslint-disable-next-line no-await-in-loop
    previousValue = await callbackfn(previousValue, currentValue, i, array);
  }

  return previousValue;
}

function createArray<T = undefined>(length: number, fill: T): T[] {
  const array = new Array(length).fill(fill) as T[];

  return array;
}

function intersects<T>(a: T[], b: T[]): T[] {
  const intersection = a.filter((value: T) => b.includes(value));

  b.forEach((value: T) => {
    if (!a.includes(value)) {
      return;
    }

    if (intersection.includes(value)) {
      return;
    }

    intersection.push(value);
  });

  return intersection;
}

function removeElement<T>(list: T[], element: T) {
  const index = list.indexOf(element);

  if (index === -1) {
    return;
  }

  list.splice(index, 1);
}

/**
 * Loop on the list until a non-undefined value is returned
 * by the callback
 */
function until<T, U>(
  list: T[],
  cb: (item: T, index: number, list: T[]) => (U | undefined),
): U | undefined {
  for (let i = 0; i < list.length; i += 1) {
    const value = cb(list[i], i, list);

    if (value !== undefined) {
      return value;
    }
  }

  return undefined;
}

// ============================================================
// Exports
export {
  createArray,
  intersects,
  reduceAsync,
  removeElement,
  until,
};
