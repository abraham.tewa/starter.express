/* eslint-disable import/first */
// ============================================================
// Imports
import dotenv from 'dotenv';

dotenv.config();

import nodeConfig from 'config';

import {
  isAccessible,
} from './redis';
import type { ConnectInfo } from './redis';

// ============================================================
// Tests
describe('.redis', () => {
  describe('isAccessible()', () => {
    it('return true if instance is accessible', async () => {
      const config = getConfig();

      const accessible = await isAccessible(config.session.store);

      expect(accessible).toBe(true);
    });

    it('return false if port is invalid', async () => {
      const connectInfo = getConfig().session.store;

      connectInfo.port = connectInfo.port
        ? connectInfo.port + 1
        : 1234;

      const accessible = await isAccessible(
        connectInfo,
        500,
      );

      expect(accessible).toBe(false);
    });

    it('return false if hostname not exists', async () => {
      const connectInfo = getConfig().session.store;

      connectInfo.hostname = connectInfo.hostname
        ? `${connectInfo.hostname}x`
        : 'example.com';

      const accessible = await isAccessible(
        connectInfo,
        500,
      );

      expect(accessible).toBe(false);
    });
  });
});

// ============================================================
// Helpers
function getConfig(): Configuration {
  return nodeConfig as unknown as Configuration;
}

type Configuration = {
  session: {
    store: ConnectInfo
  },
};
