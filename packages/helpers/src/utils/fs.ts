// ============================================================
// Imports
import * as fs from 'node:fs';
import fsPromise from 'node:fs/promises';
import path from 'node:path';
import yaml from 'js-yaml';

import type { JSONObject } from '../types';

// ============================================================
// Functions
async function findUp(
  cwd: string,
  fileOrDirectory: string,
  returnType: 'fullpath' | 'directory' = 'fullpath',
): Promise<string | undefined> {
  const fullPath = path.resolve(cwd, fileOrDirectory);

  const s = await stat(fullPath);

  if (s) {
    return returnType === 'fullpath' ? fullPath : path.dirname(fullPath);
  }

  if (cwd === '/') {
    return undefined;
  }

  const parentFolder = path.resolve(cwd, '..');

  return findUp(parentFolder, fileOrDirectory);
}

function findUpSync(
  cwd: string,
  fileOrDirectory: string,
  returnType: 'fullpath' | 'directory' = 'fullpath',
): string | undefined {
  const fullPath = path.resolve(cwd, fileOrDirectory);

  const s = statSync(fullPath);

  if (s) {
    return returnType === 'fullpath' ? fullPath : path.dirname(fullPath);
  }

  if (cwd === '/') {
    return undefined;
  }

  const parentFolder = path.resolve(cwd, '..');

  return findUpSync(parentFolder, fileOrDirectory, returnType);
}

async function readJson<T extends JSONObject = JSONObject>(
  file: string,
  encoding: BufferEncoding = 'utf-8',
): Promise<T> {
  const content = await fsPromise.readFile(file, encoding);

  return JSON.parse(content) as T;
}

async function readYaml<T extends JSONObject = JSONObject>(
  file: string,
  encoding: BufferEncoding = 'utf-8',
): Promise<T> {
  const content = await fsPromise.readFile(file, encoding);

  const doc = yaml.load(
    content,
    {
      filename: file,
    },
  ) as T;

  return doc;
}

/**
 * Permissive stat function: if the file doesn't exists, the function will return undefined instead
 * of throwing an error
 */
async function stat(file: string): Promise<fs.Stats | undefined> {
  try {
    const s = await fsPromise.lstat(file);

    return s;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  } catch (err: any) {
    if ('code' in err && (err as { code: string }).code === 'ENOENT') {
      return undefined;
    }

    throw err;
  }
}

/**
 * Permissive stat sync function: if the file doesn't exists, the function will return undefined instead
 * of throwing an error
 */
function statSync(file: string): fs.Stats | undefined {
  try {
    const s = fs.lstatSync(file);

    return s;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  } catch (err: any) {
    if ('code' in err && (err as { code: string }).code === 'ENOENT') {
      return undefined;
    }

    throw err;
  }
}

// ============================================================
// Exports
export {
  findUp,
  findUpSync,
  readJson,
  readYaml,
  stat,
  statSync,
};
