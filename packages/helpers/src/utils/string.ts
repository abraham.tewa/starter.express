import crypto from 'node:crypto';

function randomString(
  length: number,
  format: 'hex' | 'base64' = 'base64',
) {
  const buffer = crypto.randomBytes(length);
  const str = buffer.toString(format).replace('=', '');

  return str.substring(0, length);
}

function toBase64(buffer: Buffer) {
  return buffer
    .toString('base64')
    .replaceAll('=', '');
}

export {
  randomString,
  toBase64,
};
