import { removeElement } from './array';

describe('array', () => {
  describe('removeElement()', () => {
    it('do not affect the list if it\'s empty', () => {
      const list = [] as number[];
      const refList = list;

      removeElement(list, 123);

      expect(list).toEqual([]);
      expect(list).toBe(refList);
    });

    it('remove the expected element', () => {
      const list = ['a', 'b', 'c'];
      const refList = list;

      removeElement(list, 'b');

      expect(list).toEqual(['a', 'c']);
      expect(list).toBe(refList);
    });

    it('do nothing if the element is not found', () => {
      const list = ['a', 'b', 'c'];
      const refList = list;

      removeElement(list, 'd');

      expect(list).toEqual(['a', 'b', 'c']);
      expect(list).toBe(refList);
    });
  });
});
