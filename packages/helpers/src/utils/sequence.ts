// ============================================================
// Functions
function numberSeq(
  initial = 0,
): Sequence<number> {
  return build(
    (prev) => (prev as number) + 1,
    initial,
  );
}

function build<T>(
  buildNew: (prev?: T) => T,
  initial?: T,
): Sequence<T> {
  let current : T = initial ?? buildNew();

  return {
    get current() {
      return current;
    },

    next() {
      current = buildNew(current);

      return current;
    },
  };
}

// ============================================================
// Types
type Sequence<T> = {
  current: T,
  next(): T
};

// ============================================================
// Exports
export {
  build,
  numberSeq as number,
};

export type {
  Sequence,
};
