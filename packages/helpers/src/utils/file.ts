// ============================================================
// Imports
import fs from 'node:fs/promises';
import path from 'node:path';
import type { Stats } from 'node:fs';

// ============================================================
// Functions
async function findUp(cwd: string, fileOrDirectory: string): Promise<string | undefined> {
  const fullPath = path.resolve(cwd, fileOrDirectory);

  const s = await stat(fullPath);

  if (s) {
    return fullPath;
  }

  if (cwd === '/') {
    return undefined;
  }

  const parentFolder = path.resolve(cwd, '..');

  return findUp(parentFolder, fileOrDirectory);
}

/**
 * Permissive stat function: if the file doesn't exists, the function will return undefined instead
 * of throwing an error
 */
async function stat(file: string): Promise<Stats | undefined> {
  try {
    const s = await fs.lstat(file);

    return s;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  } catch (err: any) {
    if ('code' in err && (err as { code: string }).code === 'ENOENT') {
      return undefined;
    }

    throw err;
  }
}

// ============================================================
// Exports
export {
  findUp,
  stat,
};
