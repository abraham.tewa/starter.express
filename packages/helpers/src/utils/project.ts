import nodePath from 'path';
import finder from 'find-package-json';

let packageJsonRoot: string | undefined;

function getRoot() {
  return nodePath.dirname(getPackageJSON());
}

function getPackageJSON() {
  if (!packageJsonRoot) {
    const next = finder().next();

    if (next.done) {
      throw new Error('No package.json found');
    }

    packageJsonRoot = next.filename;
  }

  return packageJsonRoot;
}

function path(...paths: string[]) {
  return nodePath.resolve(getRoot(), ...paths);
}

export {
  getRoot,
  path,
};
