// ============================================================
// Imports
import Joi from 'joi';
import {
  MongoClient,
  ObjectId,
} from 'mongodb';
import type {
  Collection,
  MongoClientOptions,
} from 'mongodb';
import { createTimeoutPromise } from './promise';

// ============================================================
// Functions

/**
 *
 */
async function cloneCollection(
  collection: Collection,
  to: string,
): Promise<void> {
  const cursor = collection.aggregate(
    [{ $match: {} }, { $out: to }],
  );

  await cursor.toArray();
}

/**
 * Create a MongoDb client
 */
function createClient(
  connect: ConnectInfo | string,
  options: MongoClientOptions = {},
): MongoClient {
  const connectString = getConnectString(connect, [], true);

  const connectOptions: MongoClientOptions = {
    ...(typeof connect === 'string' ? undefined : connect.options),
    ...options,
  };

  const client = new MongoClient(
    connectString,
    connectOptions,
  );

  return client;
}

function getConnectString(
  connect: ConnectInfo | string,
  override: ConnectInfo[] = [],
  includeAuthInfo: boolean = false,
): string {
  if (typeof connect === 'object') {
    const { url, ...connectInfo } = connect;
    const connectURL = new URL('mongodb://localhost:27017');

    return getConnectString(
      connectURL.href,
      [
        connectInfo,
        ...override,
      ],
      includeAuthInfo,
    );
  }

  const config = mergeConnectInfo(...override);

  const connectURL = new URL(connect);

  connectURL.password = config.password ?? connectURL.password;
  connectURL.host = config.hostname ?? connectURL.host;
  connectURL.port = config.port?.toString() ?? connectURL.port;
  connectURL.protocol = config.protocol ?? connectURL.protocol;
  connectURL.username = config.username ?? connectURL.username;

  let database: string = config.database ?? connectURL.pathname.slice(1);

  if (config.dbPrefix && database) {
    database = `${config.dbPrefix}${database}`;
  }

  connectURL.pathname = database;

  if (config.options) {
    Object
      .entries(config.options)
      .forEach(([name, value]) => {
        connectURL.searchParams.set(name, value);
      });
  }

  if (!includeAuthInfo) {
    connectURL.password = '';
    connectURL.username = '';
  }

  return connectURL.href;
}

function mergeConnectInfo(...args: ConnectInfo[]): ConnectInfo {
  const info = Object.assign(
    {},
    ...args,
  ) as ConnectInfo;

  const options = args.map(({ options: o }) => o) as MongoClientOptions[];

  info.options = Object.assign({}, ...options) as MongoClientOptions;

  return info;
}

/**
 * Determine if the given MongoDB instance is accessible.
 * The instance is accessible if:
 *    - A MongoDB instance is listening to the given url
 *    - The authentication information are valid
 */
async function isAccessible(
  /**
   * Connection information to access the MongoDB instance
   */
  connect: ConnectInfo | string,

  /**
   * Time (in milliseconds) to way before considering the instance
   * not accessible
   */
  timeoutMS: number = 500,
): Promise<boolean> {
  const client = createClient(
    connect,
    {
      connectTimeoutMS: timeoutMS,
      serverSelectionTimeoutMS: timeoutMS,
      socketTimeoutMS: timeoutMS,
      waitQueueTimeoutMS: timeoutMS,
    },
  );

  try {
    const data = await Promise.race([
      client.connect(),
      createTimeoutPromise(false, timeoutMS),
    ]);

    return Boolean(data);
  } catch (err) {
    return false;
  } finally {
    await client.close(true);
  }
}

/**
 * Remove the authentication information from the given connect information.
 */
function removeAuthInfo<T extends ConnectInfo | string>(
  connect: T,
): T {
  if (typeof connect === 'string') {
    const url = new URL(connect);

    url.password = '';
    url.username = '';

    return url.href as T;
  }

  const clone: ConnectInfo = {
    ...connect,
  };

  delete clone.password;
  delete clone.username;

  if (clone.url) {
    clone.url = removeAuthInfo(clone.url);
  }

  return clone as T;
}

function toConnectInfo(
  connect: ConnectInfo | string,
): ConnectInfo {
  if (typeof connect === 'object') {
    return { ...connect };
  }

  const url = new URL(connect);

  const connectInfo: ConnectInfo = {
    database: url.pathname.substring(1),
    hostname: url.hostname,
    password: url.password,
    port: url.port
      ? Number(url.port)
      : 27017,
    protocol: url.protocol,
    username: url.username,
  };

  return connectInfo;
}

function toHexString(
  id: string | ObjectId,
): string {
  return typeof id === 'string'
    ? id
    : id.toHexString();
}

function toObjectId(
  id: string | ObjectId,
): ObjectId {
  return new ObjectId(id);
}

// ============================================================
// Types
type ConnectInfo = {
  database?: string,
  dbPrefix?: string,
  hostname?: string,
  options?: {
    authSource?: string
  },
  password?: string,
  port?: number,
  protocol?: string,
  url?: string,
  username?: string,
};

const Schema = Joi.object<ConnectInfo>({
  database: Joi.string().optional(),
  dbPrefix: Joi.string().optional(),
  hostname: Joi.string().hostname().optional(),
  options: Joi.object<ConnectInfo['options']>({
    authSource: Joi.string().optional(),
  }).optional(),
  password: Joi.string().optional(),
  port: Joi.number().port().optional(),
  protocol: Joi.string().allow('mongodb').optional(),
  url: Joi.string().optional(),
  username: Joi.string().optional(),
});

// ============================================================
// Exports
export {
  Schema,
  cloneCollection,
  createClient,
  getConnectString,
  isAccessible,
  removeAuthInfo,
  toConnectInfo,
  toHexString,
  toObjectId,
};

export type {
  ConnectInfo,
};
