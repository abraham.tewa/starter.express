// ============================================================
// Imports
import Redis from 'ioredis';
import Joi from 'joi';
import type { RedisOptions } from 'ioredis';
import PromiseHolder from './PromiseHolder';

// ============================================================
// Functions
function createClient(
  connect: ConnectInfo,
  options: ConnectOptions = {},
): Redis {
  const redis = new Redis({
    ...options,
    db: 0,
    host: connect.hostname,
    password: connect.password,
    port: connect.port,
    username: connect.username,
  });

  return redis;
}

async function isAccessible(
  connect: ConnectInfo,
  timeout = 500,
): Promise<boolean> {
  const redis = createClient(
    connect,
    {
      connectTimeout: timeout,
      disconnectTimeout: timeout,
      name: 'isAccessible',
    },
  );

  const promise = new PromiseHolder<boolean>();

  redis.once('error', () => {
    redis.disconnect();
    redis.once('end', () => {
      promise.resolve(false);
    });
  });

  redis.once('connect', () => {
    redis.disconnect();
    promise.resolve(true);
  });

  const connected = await promise;

  return connected;
}

function getConnectString(
  connect: ConnectInfo,
  includeAuthInfo: boolean = false,
): string {
  const url = new URL(`redis://${connect.hostname ?? 'localhost'}:${connect.port}`);

  url.pathname = connect.database !== undefined
    ? String(connect.database)
    : '';

  if (includeAuthInfo) {
    url.password = connect.password ?? '';
    url.username = connect.username ?? '';
  }

  return url.href;
}

// ============================================================
// Exports
const Schema = Joi.object<ConnectInfo>({
  database: Joi
    .number()
    .integer()
    .min(0)
    .max(10),
  hostname: Joi
    .alternatives()
    .try(
      Joi.string().domain(),
      Joi.string().ip(),
      Joi.string().allow('localhost'),
    )
    .default('localhost'),
  password: Joi.string(),
  port: Joi
    .number()
    .port()
    .default(6379),
  username: Joi.string(),
});

type ConnectInfo = {
  database?: number,
  hostname?: string,
  password?: string,
  port?: number,
  username?: string,
};

type ConnectOptions = Omit<RedisOptions, 'host' | 'password' | 'port' | 'username'>;

export type {
  ConnectInfo,
};

export {
  Schema,
  createClient,
  getConnectString,
  isAccessible,
};
