import throwError from './throwError';

describe('throwError', () => {
  it('throw the given object', () => {
    const error = 'fake error';

    expect(() => {
      throwError(error);
    }).toThrow(error);
  });
});
