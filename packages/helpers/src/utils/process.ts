// ============================================================
// Imports
import { utils } from '@internal/helpers';
import type { ChildProcessWithoutNullStreams } from 'node:child_process';

// ============================================================
// Functions
function waitForText(
  proc: ChildProcessWithoutNullStreams,
  waitText?: string,
): utils.PromiseHolder<string> {
  const promiseHolder = new utils.PromiseHolder<string>();

  const listener = (buffer: Buffer) => {
    const data = buffer.toString();

    if (waitText !== undefined && !data.includes(waitText)) {
      return;
    }

    proc.removeListener('data', listener);

    promiseHolder.resolve(buffer.toString());
  };

  proc.stdout.addListener('data', listener);

  return promiseHolder;
}

// ============================================================
// Exports
export {
  waitForText,
};
