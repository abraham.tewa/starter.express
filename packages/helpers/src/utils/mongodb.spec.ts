/* eslint-disable import/first */
// ============================================================
// Imports
import dotenv from 'dotenv';

dotenv.config();

import nodeConfig from 'config';
import { ObjectId } from 'mongodb';
import {
  getConnectString,
  isAccessible,
  removeAuthInfo,
  toConnectInfo,
  toHexString,
  toObjectId,
} from './mongodb';
import type {
  ConnectInfo,
} from './mongodb';

// ============================================================
// Tests
describe('.mongodb', () => {
  describe('getConnectString', () => {
    it('return connect string if no override', () => {
      const connectString = 'mongodb://admin:passw0rd@example.com/fake-db';
      const value = getConnectString(connectString, [], true);

      expect(value).toEqual(connectString);
    });

    it('connect string can be override', () => {
      const connectString = 'mongodb://admin:passw0rd@example.com/fake-db';
      const expectedValue = 'mongodb://app:passw0rd@example.com/fake-db';
      const value = getConnectString(
        connectString,
        [{
          username: 'app',
        }],
        true,
      );

      expect(value).toEqual(expectedValue);
    });

    it('no changes if override is empty', () => {
      const connectString = 'mongodb://admin:passw0rd@example.com/fake-db';
      const value = getConnectString(
        connectString,
        [{}],
        true,
      );

      expect(value).toEqual(connectString);
    });

    it('all information can be override', () => {
      const connectString = 'mongodb://admin:passw0rd@example.com/fake-db';
      const expectedValue = 'mongodb://app:hello@domain.org/my-db';
      const value = getConnectString(
        connectString,
        [{
          database: 'my-db',
          hostname: 'domain.org',
          password: 'hello',
          username: 'app',
        }],
        true,
      );

      expect(value).toEqual(expectedValue);
    });
  });

  describe('isAccessible()', () => {
    it('return true if instance is accessible', async () => {
      const config = getConfig();

      const accessible = await isAccessible(config.db);

      expect(accessible).toBe(true);
    });

    it('return false if instance is not accessible', async () => {
      const connectInfo = toConnectInfo(getConfig().db);

      connectInfo.password += 'xxx';

      const accessible = await isAccessible(
        connectInfo,
        500,
      );

      expect(accessible).toBe(false);
    });
  });

  describe('removeAuthInfo()', () => {
    it('remove information from connect string', () => {
      const connectString = 'mongodb://admin:password@example.com:27017/fake-db';
      const expectedValue = 'mongodb://example.com:27017/fake-db';

      const value = removeAuthInfo(connectString);

      expect(value).toEqual(expectedValue);
    });

    it('do nothing if no authentication information', () => {
      const connectString = 'mongodb://example.com:27017/fake';

      const value = removeAuthInfo(connectString);

      expect(value).toEqual(connectString);
    });

    it('remove information from connect info', () => {
      const connectInfo = toConnectInfo('mongodb://admin:passw0rd@example.com:27017/fake-db');
      const expectedValue: ConnectInfo = {
        database: 'fake-db',
        hostname: 'example.com',
        port: 27017,
        protocol: 'mongodb:',
      };

      const value = removeAuthInfo(connectInfo);

      expect(value).toEqual(expectedValue);
    });
  });

  describe('toHexString()', () => {
    it('return a string if entry is a string', () => {
      const value = 'some-id';
      const expected = value;
      const result = toHexString(value);

      expect(result).toEqual(expected);
    });

    it('return a string if entry is an ObjectId', () => {
      const expectedValue = '0123456789abcdef12345678';
      const value = new ObjectId(expectedValue);
      const result = toHexString(value);
      expect(result).toEqual(expectedValue);
    });
  });

  describe('toObjectId()', () => {
    it('return an object id if entry is a string', () => {
      const hexString = '0123456789abcdef12345678';
      const result = toObjectId(hexString);
      expect(result.toHexString()).toEqual(hexString);
    });

    it('return an object id if entry is a string', () => {
      const hexString = '0123456789abcdef12345678';
      const objectValue = new ObjectId(hexString);
      const refObjectValue = objectValue;
      const result = toObjectId(objectValue);
      expect(result).toEqual(refObjectValue);
      expect(result.toHexString()).toEqual(hexString);
    });
  });
});

// ============================================================
// Helpers
function getConfig(): Configuration {
  return nodeConfig as unknown as Configuration;
}

type Configuration = {
  db: ConnectInfo,
};
