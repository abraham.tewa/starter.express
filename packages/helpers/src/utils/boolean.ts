// ============================================================
// Module's constants and variables
const truthyValues = ['1', 'on', 'enabled', 'true', 'yes', 'y'];
const falsyValues = ['0', 'off', 'disabled', 'false', 'no', 'n'];

// ============================================================
// Functions
function isTruthyConfigValue(value: string | boolean): boolean {
  if (typeof value === 'boolean') {
    return value;
  }

  return truthyValues.includes(value.toLowerCase());
}

function isFalsyConfigValue(value: string | boolean): boolean {
  if (typeof value === 'boolean') {
    return value;
  }

  return falsyValues.includes(value.toLowerCase());
}

// ============================================================
// Exports
export {
  isFalsyConfigValue,
  isTruthyConfigValue,
};
