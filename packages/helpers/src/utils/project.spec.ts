import path from 'path';
import { getRoot } from './project.js';

const rootPath = path.resolve(__dirname, '..', '..', '..', '..');

describe('project', () => {
  describe('getRoot()', () => {
    it('return root path', () => {
      const value = getRoot();
      const expectedValue = rootPath;

      expect(value).toEqual(expectedValue);
    });
  });

  describe('path()', () => {
    it('return root path by default', () => {
      const value = getRoot();
      const expectedValue = rootPath;

      expect(value).toEqual(expectedValue);
    });
  });
});
