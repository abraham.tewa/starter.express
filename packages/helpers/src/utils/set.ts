// ============================================================
// Functions
function every<T>(set: Set<T>, cb: (value: T, set: Set<T>) => boolean): boolean {
  const cursor = set.values();

  for (const item of cursor) {
    const value = cb(item, set);

    if (!value) {
      return false;
    }
  }

  return true;
}

function filter<T>(set: Set<T>, cb: (value: T, set: Set<T>) => boolean): Set<T> {
  const cursor = set.values();
  const newSet = new Set<T>();

  for (const item of cursor) {
    const include = cb(item, set);

    if (include) {
      newSet.add(item);
    }
  }

  return newSet;
}

function find<T>(set: Set<T>, cb: (value: T, set: Set<T>) => boolean): T | undefined {
  const cursor = set.values();

  for (const item of cursor) {
    const value = cb(item, set);

    if (value) {
      return item;
    }
  }

  return undefined;
}

function addSet<T>(into: Set<T>, ...sets: [Set<T> | T[], ...(Set<T> | T[])[]]) {
  sets.forEach((set) => {
    set.forEach((v) => into.add(v));
  });

  return into;
}

function union<T>(...sets: [Set<T> | T[], ...(Set<T> | T[])[]]) {
  return addSet(new Set(), ...sets);
}

function some<T>(set: Set<T>, cb: (value: T, set: Set<T>) => boolean): boolean {
  const cursor = set.values();

  for (const item of cursor) {
    const value = cb(item, set);

    if (value) {
      return true;
    }
  }

  return false;
}

// ============================================================
// Exports
export {
  addSet,
  every,
  filter,
  find,
  some,
  union,
};
