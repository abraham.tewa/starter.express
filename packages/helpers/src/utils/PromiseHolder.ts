/**
 * A promise holder is an promise-like object that will provide a set of functions easing the
 * managing of the promise.
 */
class PromiseHolder<T = void, R = unknown> {
  readonly catch: Promise<T>['catch'];

  readonly createdAt = new Date();

  readonly finally: Promise<T>['finally'];

  readonly nodeCallback: NodeCallback<T>;

  readonly onTimeout: (
    cb: () => void,
    timeout: number,
  ) => Promise<void>;

  readonly promise: Promise<T>;

  readonly reject: (reason: R) => void = resolveUnknownError;

  readonly resolve: (value: T) => void;

  readonly then: Promise<T>['then'];

  constructor(
    callback?: (
      resolve: (value: T) => void,
      reject: (reason: R) => void,
      holder: PromiseHolder<T, R>,
    ) => void,
  ) {
    let resolve = resolveUnknownError<T>;
    let reject = resolveUnknownError;

    this.promise = new Promise<T>((resolveCb, rejectCb) => {
      resolve = resolveCb as ResolveCb<T>;
      reject = rejectCb;
    });
    this.resolve = resolve;
    this.reject = reject;
    this.then = this.promise.then.bind(this.promise);
    this.catch = this.promise.catch.bind(this.promise);
    this.finally = this.promise.finally.bind(this.promise);

    this.nodeCallback = ((err: R, value: T) => {
      if (err) {
        this.reject(err);
      } else {
        this.resolve(value);
      }
    }) as NodeCallback<T>;

    this.onTimeout = async (
      cb: () => void,
      timeout: number,
    ) => {
      await onTimeout(this, cb, timeout);
    };

    if (callback) {
      callback(this.resolve, this.reject, this);
    }
  }
}

async function onTimeout<T = void>(
  promise: PromiseLike<T>,
  cb: () => void,
  timeout: number,
): Promise<void> {
  const endPromise = new PromiseHolder<T | undefined>();
  let resolved = false;

  const timer = setTimeout(
    () => {
      if (resolved) {
        return;
      }

      try {
        cb();
        endPromise.resolve(undefined);
      } catch (err) {
        endPromise.reject(err);
      }
    },
    timeout,
  );

  void promise.then((value: T) => {
    endPromise.resolve(value);
    clearTimeout(timer);
    resolved = true;
  });

  await endPromise;
}

type NodeCallback<T> = T extends undefined
  ? (err: unknown) => void
  : (err: unknown, value: T) => void;

type ResolveCb<T> = T extends undefined
  ? () => void
  : (value: T) => void;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function resolveUnknownError<T>(value: T): void {
  throw new Error('Unexpected error');
}

export default PromiseHolder;
export {
  onTimeout,
};
