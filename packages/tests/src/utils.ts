// ============================================================
// Import modules
import { http } from '@internal/helpers';
import type {
  JSONValue,
  Safe,
} from '@internal/helpers';

// ============================================================
// Functions

function assertApi<
  T extends http.ApiDeclaration,
  S extends http.AllStatus<T>,
>(
  safe: http.SafeApi<T>,
  status: S,
): asserts safe is [http.ApiCall<T, [S]>, null] {
  const [apiCall, error] = safe;
  assertIsNull(error);

  expect(apiCall.status).toEqual(status);
}

function assertHttpHeaderExists(
  safeOrResponse: http.SafeApi<http.ApiDeclaration> | Response,
  header: string,
  value?: string,
) {
  const response = Array.isArray(safeOrResponse)
    ? safeOrResponse[0]?.res
    : safeOrResponse;

  assertIsDefined(response);

  expect(response.headers.has(header)).toBe(true);

  if (value !== undefined) {
    expect(response.headers.get(header)).toEqual(value);
  }
}

function assertHttpHeaderNotExists(
  safeOrResponse: http.SafeApi<http.ApiDeclaration> | Response,
  header: string,
) {
  const response = Array.isArray(safeOrResponse)
    ? safeOrResponse[0]?.res
    : safeOrResponse;

  assertIsDefined(response);

  expect(response.headers.has(header)).toBe(false);
}

function assertApiError<
  T extends http.ApiDeclaration,
  S extends http.AllStatus<T>,
>(
  safe: http.SafeApi<T>,
  status: S,
  message?: string | true,
): asserts safe is [http.ApiCall<T, [S]>, null] {
  const [apiCall, error] = safe;
  assertIsNull(error);

  expect(apiCall.status).toEqual(status);

  const httpError = http.error.create(status as number, message === true ? undefined : message);

  if (!httpError) {
    throw new Error(`Not a status error: ${status.toString()}`);
  }

  assertIsDefined(httpError);

  const { body } = (apiCall as { body: { code: string, message?: string, status: number } });

  assertIsDefined(body);

  expect(body.code).toEqual(httpError.code);
  expect(body.status).toEqual(httpError.status);

  if (message) {
    expect(body.message).toEqual(httpError.message);
  }
}

function assertFetchError<T extends JSONValue>(
  safe: http.SafeFetch<T>,
): asserts safe is [
  http.FetchResultError<T>,
  null,
] {
  const [result, safeError] = safe;

  assertIsNull(safeError);

  expect(result.error).not.toBeNull();
}

function assertNoFetchError<T extends JSONValue>(
  safe: http.SafeFetch<T>,
): asserts safe is [
  http.FetchResultSuccess<T>,
  null,
] {
  const [result, safeError] = safe;

  assertIsNull(safeError);

  expect(result.error).toBeNull();
}

function assertNoError<T>(
  safe: Safe<T>,
): asserts safe is [
  T,
  null,
] {
  const [, safeError] = safe;

  assertIsNull(safeError);
}

function assertError(
  safe: Safe<unknown>,
): asserts safe is [null, Error] {
  const [, safeError] = safe;

  assertIsNotNull(safeError);
  assertInstanceOf(safeError, Error);
}

function assertInstanceOf<T>(
  object: unknown,
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  instance: new (...args: any[]) => T,
): asserts object is T {
  expect(object).toBeInstanceOf(instance);
}

function assertIsNotNull<T>(
  value: T | null,
): asserts value is T {
  expect(value).not.toBeNull();
}

function assertIsDefined<T>(
  value: T | undefined,
): asserts value is T {
  expect(value).toBeDefined();
}

function assertIsNull<T>(
  value: T | null,
): asserts value is null {
  expect(value).toBeNull();
}

function unsafeFetchAsserted<T extends JSONValue>(
  safe: http.SafeFetch<T>,
): http.FetchResultSuccess<T> {
  assertNoFetchError(safe);
  return safe[0];
}

function unsafeAsserted<T>(safe: Safe<T>): T {
  assertNoError(safe);
  return safe[0];
}

function assertObject(val: unknown): asserts val is object {
  expect(typeof val).toEqual('object');
}

function assertProperty<T extends string, B extends boolean | undefined = undefined>(
  obj: unknown,
  property: T,
  optionnal?: B,
): asserts obj is (
  B extends true ? { [key in T]?: unknown } : { [key in T]: unknown }
) {
  assertObject(obj);

  const val = (obj as Record<T, unknown>)[property];

  if (optionnal) {
    if (val !== undefined) {
      expect(typeof val).toEqual('string');
    }
  } else {
    expect(typeof val).toEqual('string');
  }
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type Constructor<T> = new (...args: any[]) => T;

function assertPropertyInstanceOf<
C,
T extends string = string,
B extends boolean | undefined = undefined,
>(
  obj: unknown,
  property: T,
  type: Constructor<C>,
  optionnal?: B,
): asserts obj is (
  B extends true ? { [key in T]?: C } : { [key in T]: C }
) {
  assertObject(obj);

  const val = (obj as Record<T, unknown>)[property];

  if (optionnal) {
    if (val !== undefined) {
      expect(val).toBeInstanceOf(type);
    }
  } else {
    expect(typeof val).toBeInstanceOf(type);
  }
}

function assertProperties<T extends string>(
  val: unknown,
  properties: T[],
): asserts val is Record<T, unknown> {
  assertObject(val);

  properties.forEach((property) => {
    expect(val).toHaveProperty(property);
  });
}

function assertString(val: unknown): asserts val is string {
  expect(typeof val).toEqual('string');
}

function assertNonEmptyString(val: unknown): asserts val is string {
  assertString(val);

  expect(val).not.toEqual('');
}

/**
 * Assert that the given property is a string
 */
function assertPropertyString<T extends string>(
  obj: unknown,
  property: T,
): asserts obj is { [key in T]?: string } {
  assertProperty(obj, property);

  const val = obj[property];

  expect(typeof val).toEqual('string');
}

function assertPropertyNotEmptyString<T extends string>(
  obj: unknown,
  property: T,
): asserts obj is { [key in T]: string } {
  assertPropertyString(obj, property);

  expect(obj[property]?.length).not.toEqual('');
}

function assertPropertyOptionalOrNonEmptyString<T extends string>(
  obj: unknown,
  property: T,
): asserts obj is { [key in T]?: string | undefined } {
  assertObject(obj);

  const val = (obj as Record<T, unknown>)[property];

  expect(
    ['string', 'undefined'].includes(typeof val),
  ).toBe(true);
}

function assertArray(val: unknown): asserts val is unknown[] {
  expect(val).toBeInstanceOf(Array);
}

function assertArrayType<T>(
  array: unknown,
  cb: (val: unknown, array: unknown[]) => asserts val is T,
): asserts array is T[] {
  assertArray(array);

  array.forEach(
    (val) => {
      cb(val, array);
    },
  );
}

// ============================================================
// Exports
export {
  assertApi,
  assertApiError,
  assertArray,
  assertArrayType,
  assertError,
  assertFetchError,
  assertHttpHeaderExists,
  assertHttpHeaderNotExists,
  assertInstanceOf,
  assertIsDefined,
  assertIsNotNull,
  assertIsNull,
  assertNoError,
  assertNoFetchError,
  assertNonEmptyString,
  assertObject,
  assertProperties,
  assertProperty,
  assertPropertyInstanceOf,
  assertPropertyNotEmptyString,
  assertPropertyOptionalOrNonEmptyString,
  assertPropertyString,
  assertString,
  unsafeAsserted,
  unsafeFetchAsserted,
};
