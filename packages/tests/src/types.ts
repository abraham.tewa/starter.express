import type { DeepPartial, EnvironmentVariables, utils } from '@internal/helpers';
import type {
  ProcControl,
  ProcManager,
  log,
} from '@internal/runtime';

// ==============================
// Config
type Action<
  Args,
  C extends Config,
  R extends ProcControl | undefined,
> = (
  args: Args,
  configOverride?: DeepPartial<C>,
) => Promise<R>;

type Command<
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  Args = any,
  C extends Config = Config,
  R extends ProcControl | undefined = ProcControl | undefined,
> = {
  action: Action<Args, C, R>,
  name: string,
  procManager?: ProcManager,
};

type CliTree = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [key: string]: CliTree | ((...args: any[]) => Promise<ProcControl | undefined>),
};

type BuildCliOpts = {
  buildCommand<T extends Command>(
    command: T,
  ): (
    args: InferActionParameters<T>,
    configOverride?: InferDeepPartialConfig<T>,
  ) =>InferActionReturn<T>,

  runCommand<T extends Command>(
    command: T,
    args: InferActionParameters<T>,
    configOverride?: InferDeepPartialConfig<T>,
  ): InferActionReturn<T>,
};

type Cmds = {
  [key: string]: Cmds | Command,
};

type TestCmds<T extends Cmds> = {
  [key in keyof T]: T[key] extends Command
    ? T[key]['action']
    : (
      T[key] extends Cmds
        ? TestCmds<T[key]>
        : never
    )
};

type InferActionParameters<T> = T extends Command<infer Params> ? Params : never;
// eslint-disable-next-line @typescript-eslint/no-unused-vars
type InferConfig<T> = T extends Command<infer A, infer C> ? C : never;
// eslint-disable-next-line @typescript-eslint/no-unused-vars
type InferDeepPartialConfig<T> = T extends Command<infer A, infer C> ? DeepPartial<C> : never;

type InferCommand<T> = T extends {
  action: (params: infer Params, configOverride?: infer C) => infer Return,
  name: string,
  procManager?: ProcManager,
}
  ? {
    action: (params: Params, configOverride?: C) => Return,
    name: string,
    procManager?: ProcManager
  }
  : never;

// eslint-disable-next-line @typescript-eslint/no-unused-vars
type InferActionReturn<T> = T extends Command<infer P, infer C, infer Return>
  ? Awaited<Return>
  : never;

type InferAction<T> = T extends Command<infer P, infer C, infer R>
  ? Action<P, C, R>
  : never;

// ==============================
// Config
type Config = {
  log: {
    output: log.Output,
  },
};

type MinimalConfig = {
  log: log.Config,
};

type BuildNewConfig<T extends Config> = (
  contextId: string,
  ...overrides: DeepPartial<T>[]
) => (T | Promise<T>);

type ConfigBuilder<T extends Config, C extends Cmds> = {
  bin: {
    build: string,
    source: string,
  },
  buildNewConfig: BuildNewConfig<T>
  checkServiceAccess: CheckServiceAccessFunction<T>,
  cmds: C,
  contextId: string,
  mergeConfig: (...configs: DeepPartial<T>[]) => DeepPartial<T>,
  projectRoot: string,
  /**
   * Environment variables to inject in spawn environment
   * when running CLI commands in "spawn:build" and "spawn:import" mode.
   * In most case, you will prefer to pass the following parameters:
   *
   * {
   *  files: [
   *    `${projectRoot}/config/custom-environment-variables.json`
   *  ]
   * }
   *
   * So you forward to you cli the same variables than your current test context.
   *
   * Acceptable values:
   *  "false":
   *    No environment variables injected in the process.
   *    Notice that you may encounter an error because of the absence of the "PATH" variable.
   *    See https://maxschmitt.me/posts/error-spawn-node-enoent-node-js-child-process
   *
   *  "default":
   *    Will inject the following environment variables:
   *      NODE_ENV=test
   *      NODE_CONFIG_ENV=test
   *      PATH (copy of the current $PATH variable)
   *
   *  other:
   *    Will pass the value to the function "env.load" of the "@internal/helpers".
   *    The default values will still be passed to the shell, unless they are override.
   *
   * IMPORTANT: Do not set the "NODE_CONFIG" variable: this variable will be used to
   *            set custom parameters (such as log output) and pass the override configuration.
   *            Any prior "NODE_CONFIG" will be overwrite.
   *
   * If no value provided or if 'default', these variables will be injected:
   */
  spawnEnv?: Parameters<typeof utils.env.load>[0] | false | 'default',
};

type ServiceAccessInformation = {
  accessible: boolean,
  displayableAddress?: string,
  remarks?: string,
  serviceName?: string,
  type: string,
};

type ServiceAccessResult = Record<string, ServiceAccessInformation>;

type CheckServiceAccessFunction<T extends Config> = (config: T) => Promise<ServiceAccessResult>;

// ==============================
// Env
type EnvInfo<T extends Config> = {
  config: T,
  nodeConfigEnv: string,
};

// ==============================
// Test runs
type RunParam = {
  projectRoot: string,
  type: RunType,
};

enum RunType {
  // Run the commands by importing the source
  import = 'import',

  // Run the command by spawning a node process running the build/cli file
  spawnBuild = 'spawn:build',

  // Run the command by spawning a ts-node process running the src/cli file
  spawnSrc = 'spawn:source',
}

// ============================================================
// Exports
export type {
  Action,
  BuildCliOpts,
  BuildNewConfig,
  CheckServiceAccessFunction,
  CliTree,
  Cmds,
  Command,
  Config,
  ConfigBuilder,
  EnvInfo,
  EnvironmentVariables,
  InferAction,
  InferActionParameters,
  InferActionReturn,
  InferCommand,
  InferConfig,
  InferDeepPartialConfig,
  MinimalConfig,
  ProcControl,
  ProcManager,
  RunParam,
  ServiceAccessInformation,
  ServiceAccessResult,
  TestCmds,
};

export {
  RunType,
};
