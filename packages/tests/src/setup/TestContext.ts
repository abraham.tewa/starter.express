// ============================================================
// Imports
import { v4 as uuidv4 } from 'uuid';
import type { DeepPartial } from '@internal/helpers';
import Sequence from './Sequence';
import {
  buildCmd,
  runCmd,
} from '../cli';
import {
  type BuildCliOpts,
  type Cmds,
  type Command,
  type Config,
  type ConfigBuilder,
  type EnvInfo,
  type RunParam,
  RunType,
  type ServiceAccessResult,
  type TestCmds,
} from '../types';

// ============================================================
// Class
class TestContext<
  T extends Config,
  Cli extends Cmds,
> {
  readonly cli: TestCmds<Cli>;

  readonly config: T;

  readonly contextId: string;

  readonly env: EnvInfo<T>;

  readonly jestWorkerId = process.env.JEST_WORKER_ID
    ? Number(process.env.JEST_WORKER_ID)
    : 0;

  readonly seq: Record<string | symbol, Sequence<number>>;

  readonly uuidSeq = new Sequence<string>(
    uuidv4(),
    () => uuidv4(),
  );

  #configBuilder: ConfigBuilder<T, Cli>;

  #runParams: RunParam;

  #serviceAccessPromise?: Promise<ServiceAccessResult>;

  constructor(
    env: EnvInfo<T>,
    runParams: RunParam,
    configBuilder: ConfigBuilder<T, Cli>,
  ) {
    this.contextId = configBuilder.contextId;
    this.config = env.config;
    this.env = env;
    this.#runParams = runParams;

    const buildCliCb = {
      buildCommand: buildCmd.bind(undefined, runParams, configBuilder as unknown as ConfigBuilder<Config, Cli>),
      runCommand: runCmd.bind(undefined, runParams, configBuilder as unknown as ConfigBuilder<Config, Cli>),
    } as unknown as BuildCliOpts;

    this.cli = buildTestCli(configBuilder.cmds, buildCliCb);
    this.#configBuilder = configBuilder;

    this.seq = new Proxy(
      {} as Record<string | symbol, Sequence<number>>,
      {
        get(target, prop): Sequence<number> {
          if (!(prop in target)) {
            // eslint-disable-next-line no-param-reassign
            target[prop] = new Sequence(0, (curr) => curr + 1);
          }

          return target[prop];
        },
      },
    );
  }

  async checkServiceAccess(): Promise<ServiceAccessResult> {
    if (!this.#serviceAccessPromise) {
      const promise: Promise<ServiceAccessResult> = this.#configBuilder
        .checkServiceAccess(this.config)
        .then(
          (serviceAccess) => {
            this.#serviceAccessPromise = undefined;
            return serviceAccess;
          },
          (reason: unknown) => {
            this.#serviceAccessPromise = undefined;
            throw reason;
          },
        );

      this.#serviceAccessPromise = promise;
    }

    const serviceAccess = this.#serviceAccessPromise;

    return serviceAccess;
  }

  async createNew(
    contextId: string,
    override?: DeepPartial<T> | undefined,
  ): Promise<TestContext<T, Cli>> {
    const testContext = await TestContext.create<T, Cli>(
      this.#runParams.type,
      {
        ...this.#configBuilder,
        contextId,
      },
      override,
    );

    return testContext;
  }

  static async create<
    T extends Config,
    Cli extends Cmds,
  >(
    runType: RunType,
    configBuilder: ConfigBuilder<T, Cli>,
    configOverride?: DeepPartial<T>,
  ): Promise<TestContext<T, Cli>> {
    const runParams: RunParam = {
      projectRoot: runType === RunType.spawnBuild
        ? configBuilder.bin.build
        : configBuilder.bin.source,
      type: runType,
    };

    const override: DeepPartial<T>[] = [];

    if (configOverride) {
      override.push(configOverride);
    }

    override.push();

    const env: EnvInfo<T> = {
      config: await configBuilder.buildNewConfig(
        configBuilder.contextId,
        ...override,
      ),
      nodeConfigEnv: 'test',
    };

    return new TestContext(env, runParams, configBuilder);
  }
}

function buildTestCli<T extends Cmds>(
  cmds: T,
  buildCli: BuildCliOpts,
): TestCmds<T> {
  const aa: Record<string, unknown> = {};

  Object
    .entries(cmds)
    .forEach(([name, obj]) => {
      const val = isCommand(obj)
        ? buildCli.buildCommand(obj as Command)
        : buildTestCli(obj as Cmds, buildCli);

      aa[name] = val;
    });

  return aa as TestCmds<T>;
}

function isCommand(obj: Cmds | Command): boolean {
  return 'action' in obj && typeof obj.action === 'function';
}

// ============================================================
// Exports
export default TestContext;
