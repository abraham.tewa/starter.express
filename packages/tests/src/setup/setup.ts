// ============================================================
// Imports
import { utils } from '@internal/helpers';

import TestContext from './TestContext';
import {
  type Cmds,
  type Config,
  type ConfigBuilder,
  RunType,
} from '../types';

// ============================================================
// Module's constants and variables
const mapTimeouts: Record<RunType, number> = {
  [RunType.import]: 5_000,
  [RunType.spawnBuild]: 10_000,
  [RunType.spawnSrc]: 30_000,
};

// ============================================================
// Functions
async function setup<Conf extends Config, Cli extends Cmds>(
  configBuilder: ConfigBuilder<Conf, Cli>,
): Promise<TestContext<Conf, Cli>> {
  const {
    runType,
  } = getRunParameters();

  const ctx = await TestContext.create<Conf, Cli>(runType, configBuilder);

  await checkServiceAccess(ctx);

  return ctx;
}

async function checkServiceAccess<Conf extends Config, Cli extends Cmds>(
  ctx: TestContext<Conf, Cli>,
): Promise<void> {
  const services = await ctx.checkServiceAccess();

  const errorMessages = Object
    .entries(services)
    .filter(([, { accessible }]) => !accessible)
    .map(([name, { displayableAddress, remarks, type }]) => {
      let msg = `Service "${name}" (type: ${type}) is not accessible`;

      if (displayableAddress) {
        msg += ` (address: "${displayableAddress}")`;
      }

      if (remarks) {
        msg += `\n      ${remarks}`;
      }

      return msg;
    });

  if (errorMessages.length) {
    const message = `Or or several services are not accessible: \n - ${errorMessages.join('\n - ')}`;
    throw new Error(message);
  }
}

function getRunParameters(): {
  runType: RunType,
  timeout: number,
} {
  const runType = (process.env.TEST_RUN_TYPE as RunType | undefined) ?? RunType.import;

  const runModeLabels: Record<RunType, string> = {
    [RunType.import]: 'import source',
    [RunType.spawnBuild]: 'spawn build',
    [RunType.spawnSrc]: 'spawn source',
  };

  const label = runModeLabels[runType];

  const TEST_RUN_TYPE = getEnv('TEST_RUN_TYPE');
  const timeout = mapTimeouts[runType];

  const displayDebugInfo = utils.boolean.isTruthyConfigValue(
    process.env.TEST_DEBUG_INFO ?? 'N',
  );

  if (displayDebugInfo) {
    console.log(
      // eslint-disable-next-line @typescript-eslint/no-unnecessary-condition
      `Running mode: ${label ?? runType} (TEST_RUN_TYPE=${TEST_RUN_TYPE})`,
      `\nTimeout: ${timeout}`,
    );
  }

  if (!label) {
    throw new Error('Unknown run type');
  }

  jest.setTimeout(timeout);

  return {
    runType,
    timeout,
  };
}

function getEnv(name: string): string {
  return process.env[name] ?? '';
}

// ============================================================
// Exports
export default setup;
