// ============================================================
// Imports
import { http } from '@internal/helpers';

import type {
  Config,
  ProcControl,
} from '../types';

// ============================================================
// Class
class Server<T extends Config> {
  readonly config: T;

  #stop: () => Promise<void>;

  #url: URL;

  constructor(
    port: number,
    config: T,
    cmdControl: ProcControl,
    hostname = 'localhost',
  ) {
    this.#url = new URL(`http://${hostname}:${port}`);

    this.config = config;
    this.#stop = cmdControl.stop;
  }

  createClient(as?: http.Auth): http.ApiClient {
    return new http.ApiClient(this.#url, as);
  }

  async stop(): Promise<void> {
    await this.#stop();
  }

  get url(): URL {
    // Cloning URL
    return new URL(this.#url.href);
  }
}

// ============================================================
// Exports
export default Server;
