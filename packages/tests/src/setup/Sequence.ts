// ============================================================
// Class
class Sequence<T> {
  #current: T;

  #next: GetNextValueCb<T>;

  constructor(
    initialValue: T,
    next: GetNextValueCb<T>,
  ) {
    this.#current = initialValue;
    this.#next = next;
  }

  get current() : T {
    return this.#current;
  }

  next(): T {
    this.#current = this.#next(this.current);
    return this.#current;
  }
}

// ============================================================
// Types
type GetNextValueCb<T> = (current: T) => T;

// ============================================================
// Exports
export default Sequence;
