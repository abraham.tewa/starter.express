// ============================================================
// Imports
import runImport from './runImport';
import runSpawn from './runSpawn';
import {
  type Cmds,
  type Command,
  type Config,
  type ConfigBuilder,
  type InferActionParameters,
  type InferActionReturn,
  type InferCommand,
  type InferConfig,
  type ProcControl,
  type RunParam,
  RunType,
} from '../types';

// ============================================================
// Functions
function buildCmd<T extends Command>(
  runParams: RunParam,
  configBuilder: ConfigBuilder<InferConfig<T>, Cmds>,
  cmd: InferCommand<T>,
): ActionCb<T> {
  const runType = runParams.type;

  const cb = runType === RunType.import
    ? runImport.bind(undefined, cmd.action)
    : runSpawn.bind(
      undefined,
      {
        command: cmd.name,
        configBuilder: configBuilder as unknown as ConfigBuilder<Config, Cmds>,
        jsFile: runParams.type === RunType.spawnBuild
          ? configBuilder.bin.build
          : configBuilder.bin.source,
        procManager: cmd.procManager,
      },
    );

  return cb;
}

function runCmd<T extends Command>(
  runParams: RunParam,
  configBuilder: ConfigBuilder<InferConfig<T>, Cmds>,
  cmd: InferCommand<T>,
  args: InferActionParameters<T>,
  configOverride?: InferConfig<T>,
): Promise<InferActionReturn<T>> {
  const cb = buildCmd(runParams, configBuilder, cmd);

  const ret = cb(args, configOverride) as Promise<InferActionReturn<T>>;

  return ret;
}

// ============================================================
// Types
type ActionCb<T extends Command> = (
  args: InferActionParameters<T>,
  configOverride?: InferConfig<T>,
) => Promise<ProcControl | undefined>;

type RunCmd<T extends Command = Command> = typeof runCmd<T>;
type BuildCmd = typeof buildCmd;

// ============================================================
// Exports
export {
  buildCmd,
  runCmd,
};

export type {
  BuildCmd,
  RunCmd,
};
