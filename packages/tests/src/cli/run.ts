// ============================================================
// Import packages
import * as child_process from 'child_process';
import {
  utils,
} from '@internal/helpers';
import type { JSONAble } from '@internal/helpers';
import type { ChildProcessWithoutNullStreams } from 'child_process';

import {
  type EnvironmentVariables,
} from '../types';

// ============================================================
// Module's constants and variables

const DEBUG_SPAWN = utils.boolean.isTruthyConfigValue(process.env.DEBUG_SPAWN ?? '');

const DEBUG_SPAWN_COMMAND_NAME = process.env.DEBUG_SPAWN_COMMAND_NAME
  ? process.env.DEBUG_SPAWN_COMMAND_NAME
    .split(',')
    .map((command) => command.trim().toLowerCase())
  : undefined;

const DEBUG_SPAWN_FILTER = process.env.DEBUG_SPAWN_FILTER
  ? new RegExp(process.env.DEBUG_SPAWN_FILTER)
  : undefined;

const DEBUG_SPAWN_EVENT = process.env.DEBUG_SPAWN_EVENT
  ? process.env.DEBUG_SPAWN_EVENT
    .split(',')
    .map((event) => event.trim().toLowerCase())
  : undefined;

const DEBUG_SPAWN_STREAM = process.env.DEBUG_SPAWN_STREAM
  ? process.env.DEBUG_SPAWN_STREAM
    .split(',')
    .map((event) => event.trim().toLowerCase())
  : undefined;

const TS_NODE_PATH = 'node_modules/ts-node/dist/bin.js';

// ============================================================
// Functions
/**
 * Spawn a node or ts-node process.
 */
async function spawn({
  args,
  command,
  cwd,
  envVariables,
  index,
}: {
  args: JSONAble[],
  command: string,
  cwd: string,
  envVariables?: EnvironmentVariables,
  index: string,
}): Promise<ChildProcessWithoutNullStreams> {
  const procArgs = [
    index,
    ...command.split(' '),
    ...toCliArguments(args),
  ];

  if (index.endsWith('.ts')) {
    procArgs.unshift(TS_NODE_PATH);
  }

  const proc = child_process.spawn(
    'node',
    procArgs,
    {
      cwd,
      env: envVariables as NodeJS.ProcessEnv,
    },
  );

  displayRun(proc, procArgs, envVariables);

  const display = displayMessage.bind(
    undefined,
    proc,
    procArgs,
    command,
  );

  proc.on('close', display.bind(undefined, 'proc', 'close'));
  proc.on('disconnect', display.bind(undefined, 'proc', 'disconnect'));
  proc.on('error', display.bind(undefined, 'proc', 'error'));
  proc.on('exit', display.bind(undefined, 'proc', 'exit'));
  proc.on('message', display.bind(undefined, 'proc', 'message'));
  proc.on('spawn', display.bind(undefined, 'proc', 'spawn'));

  proc.stdout.on('close', display.bind(undefined, 'stdout', 'close'));
  proc.stdout.on('data', display.bind(undefined, 'stdout', 'data'));
  proc.stdout.on('end', display.bind(undefined, 'stdout', 'end'));
  proc.stdout.on('error', display.bind(undefined, 'stdout', 'error'));
  proc.stdout.on('pause', display.bind(undefined, 'stdout', 'pause'));
  // proc.stdout.on('resume', display.bind(undefined, 'stdout', 'resume'));

  proc.stderr.on('close', display.bind(undefined, 'stderr', 'close'));
  proc.stderr.on('data', display.bind(undefined, 'stderr', 'data'));
  proc.stderr.on('end', display.bind(undefined, 'stderr', 'end'));
  proc.stderr.on('error', display.bind(undefined, 'stderr', 'error'));
  proc.stderr.on('pause', display.bind(undefined, 'stderr', 'pause'));
  // proc.stderr.on('resume', display.bind(undefined, 'stderr', 'resume'));

  return proc;
}

/**
 * Convert an JSONable value to bash CLI arguments.
 * The goal is to provide a final string with CLI values.
 * For example
 *
 *  toCliArguments(['localhost', 3000, { timeout: 10, w: ['a', 'b', 'c'] }])
 *
 *  $ start "localhost" 3000 --timeout 10 -w "a" "b" "c"
 *
 */
function toCliArguments(
  val: JSONAble,
): string[] {
  switch (typeof val) {
    case 'object':
      if (Array.isArray(val)) {
        return val.map(toCliArguments).flat();
      }

      if (val === null) {
        return [];
      }

      return Object
        .entries(val)
        .map(([name, v]) => [
          name.length === 1 ? `-${name}` : `--${name}`,
          ...toCliArguments(v),
        ]).flat();
    case 'string':
      return [val];
    default:
      return [JSON.stringify(val)];
  }
}

function displayRun(
  proc: ChildProcessWithoutNullStreams,
  procArgs: string[],
  envVariables?: EnvironmentVariables,
) {
  if (!DEBUG_SPAWN) {
    return;
  }

  const cmd = procArgs.join(' ');

  const matchFilter = DEBUG_SPAWN_FILTER
    ? cmd.match(DEBUG_SPAWN_FILTER)
    : true;

  if (!matchFilter) {
    return;
  }

  const envToDisplay: EnvironmentVariables = {
    ...(envVariables ?? {}),
  };

  delete envToDisplay.NODE_CONFIG;

  const values = Object
    .entries(envToDisplay)
    .filter(([name]) => name)
    .map(([name, value]) => `${name} = "${value}"`);

  values.sort((a: string, b: string) => a.localeCompare(b));

  console.log(
    'Running:',
    `\nPID: ${proc.pid ? String(proc.pid) : '<none>'}`,
    // `\ncwd: ${CWD}`,
    '\nEnv:\n',
    values.join('\n  '),
    '\nCommand: node',
    ...procArgs,
  );
}

function displayMessage(
  proc: ChildProcessWithoutNullStreams,
  args: string[],
  command: string,
  source: 'stdout' | 'stderr' | 'proc',
  type: string,
  data?: string | Error,
): void {
  if (!DEBUG_SPAWN) {
    return;
  }

  if (DEBUG_SPAWN_STREAM && !DEBUG_SPAWN_STREAM.includes(source)) {
    return;
  }

  if (DEBUG_SPAWN_EVENT && !DEBUG_SPAWN_EVENT.includes(type.toLowerCase())) {
    return;
  }

  if (DEBUG_SPAWN_COMMAND_NAME) {
    const displayed = DEBUG_SPAWN_COMMAND_NAME.some(
      (cmd) => command.startsWith(cmd),
    );

    if (!displayed) {
      return;
    }
  }

  const cmd = ['node', ...args].join(' ');

  if (DEBUG_SPAWN_FILTER && !cmd.match(DEBUG_SPAWN_FILTER)) {
    return;
  }

  const msg = data instanceof Error
    ? `error: ${data.message}`
    : data ?? '<none>';

  const logType = source === 'stderr' ? 'warn' : 'info';

  console[logType](
    `Process ${source} ${type.toUpperCase()}`,
    `\nPID: ${proc.pid ? String(proc.pid) : '<none>'}`,
    `\nCommand: ${cmd}`,
    `\nData:\n${msg}`,
    '\n========================================================',
  );
}

// ============================================================
// Exports
export {
  spawn as run,
};
