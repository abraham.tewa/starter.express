// ============================================================
// Imports
import type {
  Command,
  InferActionParameters,
  InferActionReturn,
  InferDeepPartialConfig,
} from '../types';

// ============================================================
// Function
async function runImport<T extends Command>(
  cb: T['action'],
  args: InferActionParameters<T>,
  configOverride?: InferDeepPartialConfig<T>,
): Promise<InferActionReturn<T>> {
  const procControl = await cb(args, configOverride);

  return procControl as unknown as InferActionReturn<T>;
}

// ============================================================
// Exports
export default runImport;
