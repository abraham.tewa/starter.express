// ============================================================
// Types
type CliUser = {
  delete: () => Promise<void>,
  password: string,
  username: string,
};

type CommandInfo = {
  kill: () => Promise<void>,
  promises: {
    end: Promise<void>,
    initialized?: Promise<void>,
  },
  stop: () => Promise<void>,
};

// ============================================================
// Exports
export type {
  CliUser,
  CommandInfo,
};
