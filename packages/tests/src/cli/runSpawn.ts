// ============================================================
// Imports
import { utils } from '@internal/helpers';
import { log } from '@internal/runtime';
import type { DeepPartial, EnvironmentVariables } from '@internal/helpers';
import type { ChildProcessWithoutNullStreams } from 'child_process';

import { run as spawn } from './run';
import type {
  Cmds,
  Command,
  ConfigBuilder,
  InferActionParameters,
  InferActionReturn,
  InferConfig,
  InferDeepPartialConfig,
  ProcControl,
  ProcManager,
} from '../types';

const DEFAULT_ENV: EnvironmentVariables = {
  NODE_CONFIG_ENV: 'test',
  NODE_ENV: 'test',

  /**
     * Passing the "PATH" env variable to child process
     * in order to avoid "spawn node ENOENT" error.
     * See https://maxschmitt.me/posts/error-spawn-node-enoent-node-js-child-process
     */
  PATH: process.env.PATH,
};

// ============================================================
// Functions
async function runSpawn<T extends Command>(
  {
    command,
    configBuilder,
    jsFile,
    procManager,
  }: {
    command: string,
    configBuilder: ConfigBuilder<InferConfig<T>, Cmds>
    jsFile: string,
    procManager: ProcManager | undefined,
  },
  params: InferActionParameters<T>,
  configOverride?: InferDeepPartialConfig<T>,
): Promise<InferActionReturn<T>> {
  // Building config to pass as override
  // The final config needs to ensure that the log outputs are redirected to console
  const config = getFinalConfig<T>(
    configBuilder,
    configOverride,
  );

  // Loading environment variables that will be pass to the spawn process
  let envVars: EnvironmentVariables = {};
  if (configBuilder.spawnEnv === 'default') {
    envVars = {
      ...DEFAULT_ENV,
    };
  } else if (configBuilder.spawnEnv) {
    envVars = {
      ...DEFAULT_ENV,
      ...await utils.env.load(configBuilder.spawnEnv),
    };
  }

  envVars = {
    ...envVars,
    NODE_CONFIG: JSON.stringify(config),
  };

  // Spawning the process
  const proc = await spawn({
    args: [
      ...params,
    ],
    command,
    cwd: configBuilder.projectRoot,
    envVariables: envVars,
    index: jsFile,
  });

  if (!procManager?.detectEndInitialization) {
    await waitSyncCommand(proc);
    return undefined as InferActionReturn<T>;
  }

  const procControl = await waitAsyncCommandInitialization(
    proc,
    procManager.detectEndInitialization,
    command,
    procManager.initializationTimeout,
  );

  return procControl as InferActionReturn<T>;
}

async function waitSyncCommand(proc: ChildProcessWithoutNullStreams) {
  const endPromise = new utils.PromiseHolder();

  const errors: Error[] = [];

  proc.stderr.on('data', (error: Error) => {
    errors.push(error);
  });

  proc.addListener('close', (code) => {
    if (code === 1) {
      const msg = errors.map(({ message }) => message).join('\n\t');
      const error = new Error(
        `Proc closed with code 1\n\t${msg}`,
      );
      endPromise.reject(error);
    }
    endPromise.resolve();
  });

  await endPromise;
}

/**
 * Run the command and wait for initialization to finish
 * before returning to main process.
 */
async function waitAsyncCommandInitialization(
  proc: ChildProcessWithoutNullStreams,
  detectEndInitialization: (
    proc: ChildProcessWithoutNullStreams,
    timeout?: number,
  ) => Promise<void>,
  command: string,
  timeout?: number,
): Promise<ProcControl> {
  const endPromise = new utils.PromiseHolder();
  const initPromise = new utils.PromiseHolder();
  const promise = detectEndInitialization(proc, timeout);

  let isReady = false;

  void promise.then(
    () => {
      initPromise.resolve();
      isReady = true;
    },
    (err: unknown) => {
      const msg = `Error while running the command:\n\t${JSON.stringify(err)}`;
      initPromise.reject(msg);
      proc.kill();
      throw err;
    },
  );

  // Event: end
  proc.addListener('close', () => {
    if (!isReady) {
      (initPromise).reject(`Command ends before being ready: "${command}"`);
    }
    endPromise.resolve();
  });

  if (timeout !== undefined) {
    await initPromise.onTimeout(
      () => {
        proc.kill();
        const msg = `Command "${command}" initialization exceed timeout (${timeout}ms)`;
        endPromise.reject(msg);
        console.error(msg);
        throw new Error(msg);
      },
      timeout,
    );
  } else {
    try {
      await initPromise;
    } catch (err) {
      proc.kill();
      endPromise.reject(err);
      console.error(err);
      throw err;
    }
  }

  return {
    endPromise: endPromise.promise,
    kill: async () => {
      proc.kill('SIGKILL');
      await endPromise;
    },
    stop: async () => {
      proc.kill('SIGTERM');
      await endPromise;
    },
  };
}

/**
 * Create a new config where log outputs are redirected to
 * console. This enable proc manager features (like detecting
 * the starts of the server).
 */
function getFinalConfig<T extends Command>(
  configBuilder: ConfigBuilder<InferConfig<T>, Cmds>,
  configOverride?: InferDeepPartialConfig<T>,
) {
  const configs: DeepPartial<InferConfig<T>>[] = [
    {
      log: {
        output: log.Output.console,
      },
    } as DeepPartial<InferConfig<T>>,
  ];

  if (configOverride) {
    configs.push(configOverride as unknown as DeepPartial<InferConfig<T>>);
  }

  return configBuilder.mergeConfig(
    ...configs,
  );
}

// ============================================================
// Exports
export default runSpawn;
