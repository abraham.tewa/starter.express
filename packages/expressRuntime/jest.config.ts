import type { Config } from 'jest';

const config : Config = {
  displayName: '@internal/expressRuntime',
  preset: '../../jest.config.base.js',
};

export default config;
