// ============================================================
// Import
import * as ExpressOpenApiValidator from 'express-openapi-validator';
import type { LogEntry } from '@internal/runtime';
import type { OpenApiValidatorError } from './types';

// ============================================================
// Class
class OpenApiValidationError {
  error: OpenApiValidatorError;

  constructor(
    error: OpenApiValidatorError,
  ) {
    this.error = error;
  }

  /**
   * Convert the given error to a log entry
   */
  toLogEntry<S extends string>(): Omit<LogEntry<S>, 'level' | 'category'> {
    return {
      label: 'OpenApi validation error',
      message: `Error: ${this.error.name}`,
      moreInfo: {
        errors: this.error.errors,
        headers: this.error.headers,
        name: this.error.name,
        path: this.error.path,
        status: this.error.status,
      },
    };
  }

  static isOpenApiValidatorError(error: unknown): error is OpenApiValidatorError {
    return error instanceof ExpressOpenApiValidator.error.BadRequest
    || error instanceof ExpressOpenApiValidator.error.Forbidden
    || error instanceof ExpressOpenApiValidator.error.InternalServerError
    || error instanceof ExpressOpenApiValidator.error.MethodNotAllowed
    || error instanceof ExpressOpenApiValidator.error.NotFound
    || error instanceof ExpressOpenApiValidator.error.RequestEntityTooLarge
    || error instanceof ExpressOpenApiValidator.error.Unauthorized
    || error instanceof ExpressOpenApiValidator.error.UnsupportedMediaType;
  }
}

// ============================================================
// Exports
export default OpenApiValidationError;
