// ============================================================
// Imports
import { utils } from '@internal/helpers';
import RedisStore from 'connect-redis';
import express from 'express';
import expressSession from 'express-session';
import type {
  SessionConfig,
} from '@internal/runtime';
import type { Application } from 'express';
import type * as http from 'http';
import type { Redis } from 'ioredis';

// ============================================================
// Class
class HttpServer {
  readonly app: Application;

  readonly host: string;

  readonly port: number;

  #redisStoreClient?: Redis;

  #server: http.Server | undefined;

  constructor(
    port: number,
    hostname: string,
    session?: SessionConfig,
  ) {
    this.port = port;
    this.host = hostname;

    this.app = express();
    this.app.use(express.json());

    // Session
    if (session) {
      this.#redisStoreClient = utils.redis.createClient(session.store);

      this.app.use(
        expressSession({
          cookie: {
            maxAge: session.maxAge * 1000,
          },
          name: session.cookieName,
          resave: false,
          saveUninitialized: false,
          secret: session.secret,
          store: new RedisStore({
            client: this.#redisStoreClient,
          }),
        }),
      );
    }
  }

  get server(): http.Server {
    return this.#server ?? utils.throwError('Server not started');
  }

  async start() {
    const promise = new utils.PromiseHolder();
    this.#server = this.app.listen(this.port, this.host, promise.resolve);

    await promise;
  }

  async stop() {
    if (!this.#server) {
      await this.#redisStoreClient?.quit();
      return;
    }

    const promise = new utils.PromiseHolder();
    this.#server.close(() => { promise.resolve(); });

    await Promise.all([
      promise,
      this.#redisStoreClient?.quit(),
    ]);
  }
}

// ============================================================
// Exports
export default HttpServer;
