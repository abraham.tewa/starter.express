// ============================================================
// Import

import fs from 'fs/promises';
import {
  type IRuntime,
  type LogConfig,
  Runtime,
  type RuntimeContext,
  type SessionConfig,
  type TaskConfig,
} from '@internal/runtime';
import * as ExpressOpenApiValidator from 'express-openapi-validator';
import yaml from 'js-yaml';
import swaggerUi from 'swagger-ui-express';
import type { JSONObject } from '@internal/helpers';
import type { Request, Response } from 'express';
import HttpServer from './HttpServer';
import * as middlewares from './middlewares';
import routeWrapper from './middlewares/routeWrapper';
import TaskManager from './TaskManager';
import type {
  GetTaskQueueName,
  Job,
  JobHandler,
  ToolingRoutes,
} from './types';

const DEFAULT_TOOLING: ToolingConfig = {
  dashboard: { enabled: true, route: '/dashboard' },
  openapi: {
    json: { enabled: true, route: '/openapi.json' },
    ui: { enabled: true, route: '/openapi' },
    yml: { enabled: true, route: '/openapi.yml' },
  },
  root: '/_',
  taskAdmin: { enabled: true, route: '/admin/queue' },
};

// ============================================================
// Class
abstract class ExpressRuntime<
  Context extends RuntimeContext<LogCategory>,
  LogCategory extends string = string,
  J extends Job = Job,
> extends Runtime<Context, LogCategory> implements IRuntime<LogCategory> {
  readonly httpServer: HttpServer;

  readonly task: TaskManager<Context, J, LogCategory>;

  readonly #defaultLogCategory: LogCategory;

  readonly #openapi: {
    fileEncoding: BufferEncoding,
    filePath: string,
  };

  #swStats?: { stop: () => void };

  readonly #tooling: ToolingConfig;

  constructor({
    handle,
    http,
    log,
    meta,
    openapi,
    tasks = false,
    tooling,
  }: {
    handle: ConstructorParameters<typeof Runtime>[0]['handle'],
    http: {
      defaultLogCategory: LogCategory,
      host: string,
      port: number,
      session?: SessionConfig,
    },
    log: LogConfig,
    meta: MetaInfo,
    openapi: {
      fileEncoding: BufferEncoding,
      filePath: string,
      validateApiSpec: boolean,
      validateFormats: boolean,
      validateRequests: boolean,
      validateResponses: boolean,
      validateSecurity: boolean,
    },
    tasks: {
      config: TaskConfig,
      getQueueName: GetTaskQueueName<Context, J, LogCategory>,
      jobHandler: JobHandler<Context, J>,
    } | false,
    tooling: ToolingParam,
  }) {
    super({
      defaultCategory: http.defaultLogCategory,
      handle,
      log,
      meta,
    });

    this.#defaultLogCategory = http.defaultLogCategory;

    this.#tooling = getToolingConfig(tooling);

    this.#openapi = {
      fileEncoding: openapi.fileEncoding,
      filePath: openapi.filePath,
    };

    this.task = new TaskManager(
      this,
      tasks,
    );

    this.httpServer = new HttpServer(
      http.port,
      http.host,
      http.session,
    );

    this.httpServer.app.use(
      ExpressOpenApiValidator.middleware({
        apiSpec: this.#openapi.filePath,
        ignorePaths: new RegExp(this.#tooling.root),
        validateApiSpec: openapi.validateApiSpec,
        validateFormats: openapi.validateFormats,
        validateRequests: openapi.validateRequests,
        validateResponses: openapi.validateResponses,
        validateSecurity: openapi.validateSecurity,
      }),
      middlewares.openApiMiddleware(),
    );
  }

  override async cleanup() {
    await this.task.stop();

    await super.cleanup();
  }

  override async initialize() {
    await this.#setupTooling();

    await super.initialize();

    this.httpServer.app.use(
      middlewares.endCatcher(this.logger),
    );
  }

  override async stop() {
    this.#swStats?.stop();

    await this.httpServer.stop();

    // Stopping mongo client after all sub-runtime have ended
    await super.stop();
  }

  /**
   * Path to all tooling routes.
   * This object is always defined (even if tooling disabled).
   */
  get toolingRoutes(): ToolingRoutes {
    return {
      dashboard: this.#tooling.dashboard.route,
      openapi: {
        json: this.#tooling.openapi.json.route,
        ui: this.#tooling.openapi.ui.route,
        yaml: this.#tooling.openapi.yml.route,
      },
      taskAdmin: this.#tooling.taskAdmin.route,
    };
  }

  /**
   * Wrap an Express route handler.
   * The route handler will received a runtime context.
   * If an async error occurs during the run of the function,
   * it will be cached by the wrapper.
   */
  w(cb: (
    ctx: Context,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    req: Request<any, any, any, any, any>,
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    res: Response<any, any>,
  ) => Promise<void>) {
    return routeWrapper(this, this.#defaultLogCategory, cb);
  }

  /**
   * Setup the tooling apps.
   * Do nothing if tooling is disabled.
   */
  async #setupTooling() {
    const loadOpenApiFile = this.#tooling.openapi.json.enabled
      || this.#tooling.openapi.ui.enabled
      || this.#tooling.openapi.yml.enabled
      || this.#tooling.dashboard.enabled;

    if (loadOpenApiFile) {
      // ====================
      // Server dashboard
      const openApiContent = await fs.readFile(
        this.#openapi.filePath,
        this.#openapi.fileEncoding,
      );

      const openapiSpec = yaml.load(
        openApiContent,
        {
          filename: this.#openapi.filePath,
        },
      ) as JSONObject;

      // ====================
      // Dashboard
      if (this.#tooling.dashboard.enabled) {
        const swStats = (await import('swagger-stats')).default;
        this.#swStats = swStats;
        this.httpServer.app.use(
          swStats.getMiddleware({
            name: this.meta.service.name,
            swaggerSpec: openapiSpec,
            uriPath: this.toolingRoutes.dashboard,
            version: this.meta.service.version,
          }),
        );
      }

      // ====================
      // Swagger UI
      if (this.#tooling.openapi.ui.enabled) {
        this.httpServer.app.use(this.toolingRoutes.openapi.ui, swaggerUi.serve);
        this.httpServer.app.get(
          this.toolingRoutes.openapi.ui,
          swaggerUi.setup(openapiSpec),
        );
      }

      // ====================
      // openapi.json
      if (this.#tooling.openapi.json.enabled) {
        this.httpServer.app.get(
          this.toolingRoutes.openapi.json,
          (req, res) => {
            res.json(openapiSpec);
          },
        );
      }

      // ====================
      // openapi.yml
      if (this.#tooling.openapi.yml.enabled) {
        this.httpServer.app.get(
          this.toolingRoutes.openapi.yaml,
          (req, res) => {
            res.setHeader('Content-Type', 'application/yaml');
            res.send(openApiContent);
          },
        );
      }
    }

    // Tasks admin dashboard
    const router = this.task.initializeAdmin(this.toolingRoutes.taskAdmin);
    this.httpServer.app.use(this.toolingRoutes.taskAdmin, router);
  }
}

function getToolingConfig(
  params: ToolingParam,
): ToolingConfig {
  const root = typeof params === 'boolean'
    ? DEFAULT_TOOLING.root
    : params.routePrefix || DEFAULT_TOOLING.root;

  const enabled = typeof params === 'boolean'
    ? params
    : true;

  let { dashboard } = DEFAULT_TOOLING;
  let { taskAdmin } = DEFAULT_TOOLING;
  let { openapi } = DEFAULT_TOOLING;

  if (typeof params === 'object') {
    dashboard = getToolingInfo(root, enabled, params.dashboard, DEFAULT_TOOLING.dashboard);
    taskAdmin = getToolingInfo(root, enabled, params.taskAdmin, DEFAULT_TOOLING.taskAdmin);

    let jsonRoute: undefined | string | boolean;
    let yamlRoute: undefined | string | boolean;
    let uiRoute: undefined | string | boolean;

    if (typeof params.openapi === 'object') {
      jsonRoute = params.openapi.json;
      yamlRoute = params.openapi.yml;
      uiRoute = params.openapi.ui;
    } else if (typeof params.openapi === 'string') {
      jsonRoute = `${params.openapi}.json`;
      yamlRoute = `${params.openapi}.yml`;
      uiRoute = params.openapi;
    }

    openapi = {
      json: getToolingInfo(root, enabled, jsonRoute, DEFAULT_TOOLING.openapi.json),
      ui: getToolingInfo(root, enabled, uiRoute, DEFAULT_TOOLING.openapi.ui),
      yml: getToolingInfo(root, enabled, yamlRoute, DEFAULT_TOOLING.openapi.yml),
    };
  }

  return {
    dashboard,
    openapi,
    root,
    taskAdmin,
  };
}

function getToolingInfo(
  routePrefix: string,
  enabled: boolean | undefined,
  value: undefined | string | boolean,
  defaultValue: ToolInfo,
): ToolInfo {
  let isEnabled: boolean;
  let route: string;

  if (enabled) {
    isEnabled = true;
    route = defaultValue.route;
  }

  if (value === undefined || typeof value === 'boolean') {
    isEnabled = value !== false;
    route = defaultValue.route;
  } else {
    isEnabled = true;
    route = value;
  }

  return {
    enabled: isEnabled,
    route: routePrefix + route,
  };
}

// ============================================================
// Types
type MetaInfo = {
  service: {
    name: string,
    version: string,
  }
};

type ToolInfo = {
  enabled: boolean,
  route: string,
};

type ToolingParam = boolean | {
  dashboard?: boolean | string,
  openapi?: string | boolean | {
    json?: string | boolean,
    ui?: string | boolean,
    yml?: string | boolean,
  },
  routePrefix: string,
  taskAdmin?: string | boolean,
};

type ToolingConfig = {
  dashboard: ToolInfo,
  openapi: {
    json: ToolInfo,
    ui: ToolInfo,
    yml: ToolInfo,
  },
  root: string,
  taskAdmin: ToolInfo,
};

// ============================================================
// Exports
export default ExpressRuntime;
