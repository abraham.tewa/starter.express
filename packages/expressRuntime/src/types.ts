// ============================================================
// Imports
import type { JSONValue } from '@internal/helpers';
import type { RuntimeContext } from '@internal/runtime';
import type * as ExpressOpenApiValidator from 'express-openapi-validator';
import type ExpressRuntime from './ExpressRuntime';

// ============================================================
// Types
type ToolingRoutes = {
  dashboard: string,
  openapi: {
    json: string,
    ui: string,
    yaml: string,
  },
  taskAdmin: string,
};

type JobHandler<Context, J extends Job> = (
  data: J['dataType'],
  context: Context,
) => Promise<J['returnType']>;

type GetTaskQueueName<
  Context extends RuntimeContext<LogCategory>,
  J extends Job,
  LogCategory extends string,
> = (options: {
  data: J['dataType'],
  jobName: string,
  runtime: ExpressRuntime<Context, LogCategory, J>,
}) => string;

type Job = {
  dataType: JSONValue,
  returnType: JSONValue | undefined | Promise<JSONValue | undefined>,
};

type OpenApiValidatorError = InstanceType<typeof ExpressOpenApiValidator.error.BadRequest>
| InstanceType<typeof ExpressOpenApiValidator.error.Forbidden>
| InstanceType<typeof ExpressOpenApiValidator.error.InternalServerError>
| InstanceType<typeof ExpressOpenApiValidator.error.MethodNotAllowed>
| InstanceType<typeof ExpressOpenApiValidator.error.NotFound>
| InstanceType<typeof ExpressOpenApiValidator.error.RequestEntityTooLarge>
| InstanceType<typeof ExpressOpenApiValidator.error.Unauthorized>
| InstanceType<typeof ExpressOpenApiValidator.error.UnsupportedMediaType>;

// ============================================================
// Exports
export type {
  GetTaskQueueName,
  Job,
  JobHandler,
  OpenApiValidatorError,
  ToolingRoutes,
};
