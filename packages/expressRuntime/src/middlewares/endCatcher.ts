// ============================================================
// Imports
import { http } from '@internal/helpers';
import * as ExpressOpenValidatorError from 'express-openapi-validator';
import type { Logger } from '@internal/runtime';
import type {
  NextFunction,
  Request,
  Response,
} from 'express';
import OpenApiValidationError from '../OpenApiValidationError';

// ============================================================
// Functions
function endCatcher<S extends string>(
  logger: Logger<S>,
): (err: unknown, req: Request, res: Response, next: NextFunction) => void {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  return (err: unknown, req: Request, res: Response, next: NextFunction) => {
    if (err instanceof OpenApiValidationError) {
      onOpenApiValidationError({
        err, logger, req, res,
      });
      return;
    }

    if (err instanceof http.error.HttpError) {
      err.send(res);
      return;
    }

    const error = new http.error.InternalServerError(
      'An uncaught error occurred',
      err,
      {
        method: req.method,
        path: req.path,
      },
    );
    console.log(err);

    logger.error(error);

    error.send(res);
  };
}

function onOpenApiValidationError<S extends string>({
  err, logger, req, res,
}: {
  err: OpenApiValidationError,
  logger: Logger<S>
  req: Request,
  res: Response,
}) {
  const entry = err.toLogEntry();

  const data = {
    error: err.error.errors.map(({ errorCode, message, path }) => ({
      errorCode,
      message,
      path,
    })),
    headers: { Alow: err.error.headers?.Allow },
    method: req.method,
    name: err.error.name,
    path: err.error.path,
    status: err.error.status,
  };

  const httpError = isNotFoundError(err)
    ? new http.error.NotFound(undefined, data)
    : new http.error.InvalidRequest(entry.label, data);

  logger.error(httpError);

  httpError.send(res);
}

function isNotFoundError(err: OpenApiValidationError): boolean {
  return err.error instanceof ExpressOpenValidatorError.error.NotFound
    || err.error instanceof ExpressOpenValidatorError.error.MethodNotAllowed;
}

// ============================================================
// Exports
export default endCatcher;
