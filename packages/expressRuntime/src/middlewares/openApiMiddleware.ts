// ============================================================
// Import packages
import { http } from '@internal/helpers';
import type { NextFunction, Request, Response } from 'express';
import OpenApiValidationError from '../OpenApiValidationError';

// ============================================================
// Functions
function openApiMiddleware() {
  return (
    err: { errors: unknown, message: string, status?: number },
    req: Request,
    res: Response,
    next: NextFunction,
  ) => {
    if (OpenApiValidationError.isOpenApiValidatorError(err)) {
      next(new OpenApiValidationError(err));
      return;
    }

    next(new http.error.InternalServerError(
      undefined,
      err,
    ));
  };
}

// ============================================================
// Exports
export default openApiMiddleware;
