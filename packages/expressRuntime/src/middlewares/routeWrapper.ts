// ============================================================
// Imports
import { http } from '@internal/helpers';
import type { Logger } from '@internal/runtime';
import type { Request, Response } from 'express';
import type ExpressRuntime from '../ExpressRuntime';
import type { Job } from '../types';

// ============================================================
// Middleware
/**
 * Middleware to use to wrap service routes.
 */
function routeWrapper<
Context extends { logger: Logger<LogCategory> },
J extends Job = Job,
LogCategory extends string = string,
>(
  runtime: ExpressRuntime<Context, LogCategory, J>,
  logCategory: LogCategory,
  cb: (
    ctx: Context,
    req: Request,
    res: Response
  ) => Promise<void>,
) {
  return (req: Request, res: Response) => {
    const startDate = new Date();
    const ctx = runtime.createContext();

    void cb(ctx, req, res).then(
      () => {
        const endDate = new Date();
        ctx.logger.info({
          category: logCategory,
          label: `[${req.method}] ${req.path}`,
          message: `${res.statusCode} ${endDate.getTime() - startDate.getTime()}ms`,
        });
      },
      (error: unknown) => {
        const endDate = new Date();
        let httpError: http.error.HttpError;

        if (error instanceof http.error.HttpError) {
          httpError = error;
          ctx.logger.error(
            error,
          );
        } else {
          const route = `${req.method} ${req.path}`;
          ctx.logger.error({
            // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment, @typescript-eslint/no-explicit-any
            category: 'api' as any,
            error,
            label: `${route}: unexpected error`,
            message: `An error occurs on route ${route}`,
          });
          httpError = new http.error.InternalServerError(undefined, error);
        }

        ctx.logger.info({
          category: logCategory,
          label: `[${req.method}] ${req.path}`,
          message: `${res.statusCode} ${endDate.getTime() - startDate.getTime()}ms`,
        });

        if (res.headersSent) {
          return;
        }

        httpError.send(res);
      },
    );
  };
}

// ============================================================
// Exports
export default routeWrapper;
