/* eslint-disable max-classes-per-file */
// ============================================================
// Imports

import { createBullBoard } from '@bull-board/api';
import { BullAdapter } from '@bull-board/api/bullAdapter';
import { ExpressAdapter } from '@bull-board/express';
import { utils } from '@internal/helpers';
import {
  type Job as BullMQJob,
  Queue,
  Worker,
} from 'bullmq';
import type {
  RuntimeContext,
  TaskConfig,
} from '@internal/runtime';
import type { Router } from 'express';
import type IORedis from 'ioredis';

import type ExpressRuntime from './ExpressRuntime';
import type {
  GetTaskQueueName,
  Job,
  JobHandler,
} from './types';

// ============================================================
// Class
class TaskManager<Context extends RuntimeContext<LogCategory>, J extends Job, LogCategory extends string> {
  readonly runtime: ExpressRuntime<Context, LogCategory, J>;

  #admin?: AdminTaskManager;

  #getTaskQueueName?: GetTaskQueueName<Context, J, LogCategory>;

  #handler?: JobHandler<Context, J>;

  #redisConnection?: IORedis;

  #storeConfig?: utils.redis.ConnectInfo;

  #tasksQueues: Record<string, Queue> = {};

  #workers: Worker[] = [];

  constructor(
    runtime: ExpressRuntime<Context, LogCategory, J>,
    info: {
      config: TaskConfig,
      getQueueName: GetTaskQueueName<Context, J, LogCategory>,
      jobHandler: JobHandler<Context, J>,
    } | false,
  ) {
    this.runtime = runtime;

    if (info === false) {
      return;
    }

    this.#storeConfig = info.config.store;
    this.#redisConnection = this.#createRedisConnection();
    this.#getTaskQueueName = info.getQueueName;
    this.#handler = info.jobHandler;
  }

  async addJob(
    jobName: string,
    data: J['dataType'],
  ) {
    if (!this.#getTaskQueueName) {
      throw new Error('Task manager disabled');
    }

    const queueName = this.#getTaskQueueName({
      data,
      jobName,
      runtime: this.runtime,
    });

    const queue = this.getTaskQueue(queueName);

    await queue.add(
      jobName,
      data,
    );
  }

  getTaskQueue(
    name: string,
  ): Queue {
    let queue = this.#tasksQueues[name] as Queue | undefined;

    if (!queue) {
      queue = new Queue(
        name,
        {
          connection: this.#redisConnection,
        },
      );

      this.#tasksQueues[name] = queue;

      if (this.#admin) {
        this.#admin.addQueue(queue);
      }
    }

    return queue;
  }

  initializeAdmin(basePath = '/admin/queue'): Router {
    if (this.#admin) {
      throw new Error('Admin already initialized');
    }

    const admin = new AdminTaskManager(basePath);
    this.#admin = admin;

    Object
      .values(this.#tasksQueues)
      .forEach((queue: Queue) => { admin.addQueue(queue); });

    return admin.router;
  }

  /**
   * Create workers that will handle jobs.
   */
  startWorkers(
    queue: string,
    number = 1,
  ): Worker<J['dataType'], J['returnType']>[] {
    // Creating workers
    const workers = new Array(number)
      .fill(undefined)
      .map(() => {
        const connection = this.#createRedisConnection();

        const worker = new Worker(
          queue,
          this.#runJob.bind(this),
          {
            connection,
            limiter: {
              duration: 2000,
              max: 1,
            },
          },
        );
        return worker;
      });

    this.#workers.push(...workers);

    return workers;
  }

  async stop() {
    await this.stopAllWorkers();
    await this.#redisConnection?.quit();
  }

  async stopAllWorkers() {
    const workers = this.#workers.slice(0);
    const promises = workers.map(async (worker) => {
      await worker.close();
      await worker.disconnect();

      utils.array.removeElement(this.#workers, worker);
    });

    await Promise.all(promises);
  }

  #createRedisConnection(): IORedis {
    if (!this.#storeConfig) {
      throw new Error('Task manager disabled');
    }

    const connection = utils.redis.createClient(
      this.#storeConfig,
      {
        name: 'TaskManager',
      },
    );
    return connection;
  }

  async #runJob(
    job: BullMQJob<J['dataType']>,
  ) {
    if (!this.#handler) {
      throw new Error('Task manager disabled');
    }

    const context = this.runtime.createContext();
    const res = await this.#handler(job.data, context);

    return res;
  }
}

class AdminTaskManager {
  router: Router;

  #addQueue: (queue: BullAdapter) => void;

  constructor(basePath: string) {
    const serverAdapter = new ExpressAdapter();
    serverAdapter.setBasePath(basePath);
    const { addQueue } = createBullBoard({
      queues: [],
      serverAdapter,
    });

    this.router = serverAdapter.getRouter() as Router;

    this.#addQueue = addQueue;
  }

  addQueue(queue: Queue) {
    this.#addQueue(new BullAdapter(queue));
  }
}

// ============================================================
// Exports
export default TaskManager;
