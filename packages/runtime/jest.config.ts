import type { Config } from 'jest';

const config : Config = {
  displayName: '@internal/runtime',
  preset: '../../jest.config.base.js',
};

export default config;
