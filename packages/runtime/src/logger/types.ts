import type { MetaInfo } from '../types';

enum LogOutput {
  console = 'console',
  none = 'none',
}

enum LogFormat {
  realtime = 'realtime',
  summary = 'summary',
}

enum LogLevel {
  critical = 'critical',
  error = 'error',
  warning = 'warning',
  success = 'success',
  info = 'info',
  notice = 'notice',
  trace = 'trace',
  debug = 'debug',
}

const LogLevelsSeverity = [
  LogLevel.debug,
  LogLevel.trace,
  LogLevel.notice,
  LogLevel.info,
  LogLevel.success,
  LogLevel.warning,
  LogLevel.error,
  LogLevel.critical,
];

type LogTag = Record<string, string | number | boolean | null>;

type LogEntry<T extends string> = {
  category: T,
  extraTags?: LogTag
  label: string,
  level: LogLevel,
  message: string,
  moreInfo?: unknown,
};

type ErrorLogEntry<T extends string> = LogEntry<T> & {
  error?: unknown,
};

type LogFormatter<T extends string> = (
  entries: LogEntry<T>[],
  tags: LogTag,
  meta: MetaInfo,
) => string[];

type Config = {
  format: LogFormat,
  level: LogLevel,
  output: LogOutput,
};

export type {
  Config,
  ErrorLogEntry,
  LogEntry,
  LogFormatter,
  LogTag,
};

export {
  LogFormat,
  LogLevel,
  LogLevelsSeverity,
  LogOutput,
};
