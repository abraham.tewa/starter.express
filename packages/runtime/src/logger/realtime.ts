// ============================================================
// Imports

import { utils } from '@internal/helpers';

import chalk from 'chalk';
import {
  type ErrorLogEntry,
  type LogEntry,
  type LogFormatter,
  LogLevel,
  type LogTag,
} from './types';
import type {
  MetaInfo,
} from '../types';

// ============================================================
// Functions
function realtime<T extends string>() : {
  disable: () => void,
  enable: () => void,
  formatter: LogFormatter<T>,

  /**
   * Toggle the enable state.
   * @param enable - If defined, force the log state
   * @returns The previous enable state
   */
  toggle: (enable?: boolean) => boolean
} {
  let enabled = true;

  return {
    disable: () => {
      enabled = false;
    },
    enable() {
      enabled = true;
    },
    formatter: (entries: LogEntry<T>[], tags: LogTag, meta: MetaInfo) => {
      if (!enabled) {
        return [];
      }

      return entries.map((entry) => formatEntry(entry, tags, meta));
    },
    toggle(enable?: boolean) {
      const wasEnabled = enabled;
      if (typeof enable === 'boolean') {
        enabled = enable;
      } else {
        enabled = !enabled;
      }

      return wasEnabled;
    },
  };
}

// ============================================================
// Module's constants and variables
const colors: { [key in LogLevel]: (str: string) => string } = {
  [LogLevel.critical]: chalk.red,
  [LogLevel.debug]: chalk.gray,
  [LogLevel.error]: chalk.red,
  [LogLevel.info]: (str) => chalk.bold(chalk.gray(str)),
  [LogLevel.notice]: chalk.gray,
  [LogLevel.success]: chalk.green,
  [LogLevel.trace]: chalk.gray,
  [LogLevel.warning]: chalk.bgMagenta,
};

function formatEntry<T extends string>(
  entry: ErrorLogEntry<T>,
  tags: LogTag,
  meta: MetaInfo,
): string {
  const color = colors[entry.level];

  const allTags = {
    ...entry.extraTags,
    ...tags,
  };

  const service = `${meta.service.name}@${meta.service.version}`;
  const category = `[${entry.category}]`;
  const { label } = entry;
  const message = chalk.bold(entry.message);
  const details = entry.moreInfo
    ? `\n${JSON.stringify(entry.moreInfo, undefined, 2)}`
    : '';

  const tagStr = utils.object.isEmpty(allTags)
    ? ''
    : ` ${chalk.grey(JSON.stringify(allTags))}`;

  const newLine = details.length + tagStr.length > 0
    ? `\n${details}${tagStr}`
    : '';

  let msg = color(`${service} ${category} ${label} - ${message}${newLine}`);

  if (entry.level === LogLevel.critical) {
    msg = chalk.bold(msg);
  }

  return msg;
}

// ============================================================
// Exports
export default realtime;
