// ============================================================
// Imports

import { http } from '@internal/helpers';

import LogError from './Error';
import realtime from './realtime';
import summary from './summary';
import {
  LogFormat,
  LogLevel,
  LogLevelsSeverity,
  LogOutput,
} from './types';
import type {
  Config,
  ErrorLogEntry,
  LogEntry,
  LogFormatter,
  LogTag,
} from './types';
import type { MetaInfo } from '../types';

// ============================================================
// Module's constants and variables
function consoleLogOneEntry({ error, message }: { error?: unknown, message: string }) {
  console.log(message);

  if (error) {
    console.error(error);
  }
}

const logOutput: LogOutputMap = {
  [LogOutput.console]: (messages: { error?: unknown, message: string }[]) => {
    messages.forEach(consoleLogOneEntry);
  },
  [LogOutput.none]: () => undefined,
};

// ============================================================
// Functions

function buildLogger<T extends string>(
  formatter: LogFormatter<T>,
  output: LogOutputCb,
  tags: LogTag,
  meta: MetaInfo,
  entries: ErrorLogEntry<T>[],
): undefined {
  entries.forEach(({ error, ...entry }) => {
    const [message] = formatter([entry], tags, meta);
    output([{ error, message }]);
  });
}

function build<T extends string>(
  config: Config,
  apiCategory: T,
  meta: MetaInfo,
  tags: LogTag = {},
) {
  const output = logOutput[config.output];

  const formatter: LogFormatter<T> = config.format === LogFormat.summary
    ? summary().formatter
    : realtime().formatter;

  const logger = buildLogger.bind<
  undefined, [LogFormatter<T>, LogOutputCb, LogTag, MetaInfo], [LogEntry<T>[]], undefined
  >(
    undefined,
    formatter,
    output,
    tags,
    meta,
  );

  const minLevelIndex = LogLevelsSeverity.indexOf(config.level);

  const shouldPerformLog = (level: LogLevel) => {
    const levelIndex = LogLevelsSeverity.indexOf(level);
    return levelIndex >= minLevelIndex;
  };

  const levels = Object
    .entries(LogLevel)
    .filter(([, level]) => level !== LogLevel.error)
    .map(([name, level]) => [
      name,
      (...entries: Omit<LogEntry<T>, 'level'>[]) => {
        if (!shouldPerformLog(level)) {
          return;
        }

        const newEntries: LogEntry<T>[] = entries.map((entry) => ({ ...entry, level }));
        logger(newEntries);
      },
    ]);

  const error = (...entries: LogErrorEntry<T>[]) => {
    const newEntries = entries.map((entry) => {
      if (entry instanceof http.error.HttpError) {
        return {
          category: apiCategory,
          entry: 'error' in entry ? entry.error : undefined,
          label: entry.name,
          level: LogLevel.error,
          message: entry.message,
          moreInfo: {
            ...(entry.data || {}),
          },
        };
      }

      return {
        ...entry,
        level: LogLevel.error,
      };
    });

    if (shouldPerformLog(LogLevel.error)) {
      logger(newEntries);
    }

    return new LogError(newEntries);
  };

  const all: Logger<T> = {
    log: (...entries: LogEntry<T>[]) => {
      const toLog = entries.filter(({ level }) => shouldPerformLog(level));
      logger(toLog);
    },

    ...Object.fromEntries(levels) as {
      [key in LogLevel]: (...entries: Omit<LogEntry<T>, 'level'>[]) => void
    },

    child(extraTags: LogTag = {}) {
      return build(
        config,
        apiCategory,
        meta,
        {
          ...extraTags,
          ...tags,
        },
      );
    },

    dev(...str: unknown[]) {
      console.log(...str);
    },

    error,

    async httpError(
      res: http.FetchResponse<unknown>,
      entry: Omit<LogEntry<T>, 'level'>,
    ) {
      const err = error({
        ...entry,
        moreInfo: {
          ...(entry.moreInfo ?? undefined),
          res: {
            status: res.status,
            text: await res.text(),
          },
        },
      });

      return err;
    },
  };

  return {
    ...all,
  };
}

// ============================================================
// Types

type LogOutputCb = (messages: { error?: unknown, message: string }[]) => void;

type LogOutputMap = {
  [K in LogOutput]: LogOutputCb
};

type Logger<T extends string> = {
  [key in Exclude<LogLevel, 'error'>]: (...entries: Omit<LogEntry<T>, 'level'>[]) => void
} & {
  child: (
    tags?: LogTag,
  ) => Logger<T>,
  dev: (...string: unknown[]) => void,
  error: (...entries: LogErrorEntry<T>[]) => LogError<T>,
  httpError: (
    res: http.FetchResponse<unknown>,
    entry: Omit<LogEntry<T>, 'level'>,
  ) => Promise<LogError<T>>,
  log: (...entries: LogEntry<T>[]) => void,
};

type LogErrorEntry<T extends string> = http.error.HttpError | Omit<ErrorLogEntry<T>, 'level'>;

// ============================================================
// Exports
export default build;

export type {
  LogFormatter,
  Logger,
};
