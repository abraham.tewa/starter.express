// ============================================================
// Imports
import chalk from 'chalk';

import {
  type LogEntry,
  type LogFormatter,
  LogLevel,
  LogOutput,
  type LogTag,
} from './types';

// ============================================================
// Module's constants and variables
const logIcons: {
  [key in LogLevel]: string
} = {
  critical: chalk.bold(chalk.red('✘')),
  debug: chalk.grey('⇒'),
  error: chalk.red('✘'),
  info: chalk.blue('i'),
  notice: chalk.grey('…'),
  success: chalk.green('✔'),
  trace: chalk.grey(' '),
  warning: chalk.yellow('⚠'),
};

// ============================================================
// Functions
function summary<T extends string>(): {
  clear: () => void,
  display: () => void,
  formatter: LogFormatter<T>,
  setOutput: (output: LogOutput) => void,
  stats: () => Stats,
} {
  let allEntries: Array<{ entry: LogEntry<T>, tags: LogTag }> = [];
  let output: LogOutput | undefined;

  return {
    clear: () => {
      allEntries = [];
    },
    display: () => { log(output, allEntries); },
    formatter: (entries: LogEntry<T>[], tags: LogTag) => {
      allEntries.push(...entries.map((entry) => ({ entry, tags })));
      return [];
    },
    setOutput(newOutput: LogOutput) {
      output = newOutput;
    },
    stats: () => stats(allEntries),
  };
}

/**
 * Pretty log operation results
 */
function log<T extends string>(
  output: LogOutput | undefined,
  entries: Array<{ entry: LogEntry<T>, tags: LogTag }>,
) {
  if (!output) {
    throw new Error('Log output not defined');
  }

  if (output === LogOutput.none) {
    return;
  }

  const str = toString(entries);
  console.log(str);
}

function stats<T extends string>(
  entries: Array<{ entry: LogEntry<T>, tags: LogTag }>,
): Stats {
  return entries.reduce(
    (acc, { entry }) => {
      switch (entry.level) {
        case LogLevel.error:
          acc.errors += 1;
          break;
        case LogLevel.success:
          acc.successes += 1;
          break;
        case LogLevel.warning:
          acc.warnings += 1;
          break;
        default:
      }

      return acc;
    },
    {
      errors: 0,
      successes: 0,
      warnings: 0,
    },
  );
}

function toString<T extends string>(
  entries: Array<{ entry: LogEntry<T>, tags: LogTag }>,
): string {
  return entries.reduce<{ prev: { category: string }, str: string }>(
    (acc, { entry, tags }) => {
      if (acc.prev.category !== entry.category) {
        acc.str += `\n\n${entry.category}:`;
      }

      const icon = logIcons[entry.level];
      acc.str += `\n  ${icon} ${entry.message} · ${chalk.grey(`[${entry.label}]`)}`;

      acc.str += chalk.gray(` · ${JSON.stringify(tags)}`);

      return {
        prev: entry,
        str: acc.str,
      };
    },
    { prev: { category: '' }, str: '' },
  ).str;
}

// ============================================================
// Types
type Stats = {
  errors: number,
  successes: number,
  warnings: number,
};

// ============================================================
// Exports
export default summary;
export {
  toString,
};
