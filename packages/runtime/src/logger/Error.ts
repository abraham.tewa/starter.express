// ============================================================
// Import modules
import { LogLevel } from './types';
import type { LogEntry } from './types';
import type { AppError } from '../error';

// ============================================================
// Class
class LogError<T extends string> extends Error implements AppError<T> {
  entries: LogEntryStr<T>[];

  constructor(entries: LogEntry<T>[]) {
    let { message } = entries[0];
    message += `\nDetails:\n${JSON.stringify(entries[0].moreInfo)}`;
    super(message);

    this.entries = entries.map((entry) => {
      if (!('details' in entry)) {
        return entry as LogEntryStr<T>;
      }

      const details = JSON.stringify(entry.moreInfo);

      return {
        ...entry,
        details,
      };
    });
  }

  // eslint-disable-next-line @typescript-eslint/require-await
  async log(
    category: T,
    label: string,
    message: string,
  ): Promise<LogError<T>> {
    const entry: LogEntry<T> = {
      category,
      label,
      level: LogLevel.error,
      message,
      moreInfo: this,
    };

    return new LogError([entry]);
  }
}

// ============================================================
// Types
type LogEntryStr<T extends string> = Omit<LogEntry<T>, 'details'> & {
  details?: string
};

// ============================================================
// Exports
export default LogError;
