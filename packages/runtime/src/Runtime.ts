// ============================================================
// Import

import { utils } from '@internal/helpers';
import { RunStatus } from './enums';
import {
  type Config as LogConfig,
  type Logger, build,
} from './logger';
import type {
  IRuntime,
  RuntimeContext,
} from './types';

// ============================================================
// Class
abstract class Runtime<
  Context extends RuntimeContext<LogCategory>,
  LogCategory extends string = string,
> implements IRuntime<LogCategory> {
  readonly endPromise: Promise<void>;

  readonly logger: Logger<LogCategory>;

  readonly meta: MetaInfo;

  readonly #defaultLogCategory: LogCategory;

  readonly #endPromise: utils.PromiseHolder;

  #runStatus: RunStatus = RunStatus.initializing;

  #setup?: () => Promise<void>;

  #teardown?: () => Promise<void>;

  constructor({
    defaultCategory,
    handle,
    log,
    meta,
  }: {
    defaultCategory: LogCategory,
    handle?: {
      setup?: () => Promise<void>,
      teardown?: () => Promise<void>,
    },
    log: LogConfig,
    meta: MetaInfo,
  }) {
    this.meta = utils.object.cloneDeep(meta);
    this.#setup = handle?.setup;

    this.#teardown = handle?.teardown;
    this.#defaultLogCategory = defaultCategory;
    this.logger = build(log, this.#defaultLogCategory, this.meta);

    this.#endPromise = new utils.PromiseHolder();
    this.endPromise = this.#endPromise.promise;
  }

  async cleanup() {
    await this.#teardown?.();
  }

  async initialize() {
    await this.#setup?.();
  }

  get runStatus() {
    return this.#runStatus;
  }

  async stop() {
    // Stopping mongo client after all sub-runtime have ended
    await this.cleanup();

    this.#endPromise.resolve();
  }

  abstract createContext(): Context;
}

// ============================================================
// Types
type MetaInfo = {
  service: {
    name: string,
    version: string,
  }
};

// ============================================================
// Exports
export default Runtime;
