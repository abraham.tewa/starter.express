enum RunStatus {
  initializing = 'initializing',
  stopped = 'stopped',
  running = 'running',
}

export {
  RunStatus,
};
