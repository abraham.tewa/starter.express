// ============================================================
// Import
import { utils } from '@internal/helpers';
import { type ClientSession, type MongoClient } from 'mongodb';
import type * as log from './logger';

// ============================================================
// Class
class MongoDbContext<T extends string> {
  readonly client: MongoClient;

  readonly logger: log.Logger<T>;

  readonly ownedSession: boolean;

  readonly parent: MongoDbContext<T> | undefined;

  readonly session: ClientSession | undefined;

  constructor({
    client,
    logger,
    parent,
    session,
  }: {
    client: MongoClient,
    logger: log.Logger<T>,
    parent?: MongoDbContext<T>,
    session?: ClientSession,
  }) {
    this.logger = logger;
    this.client = client;

    this.session = session ?? parent?.session;
    this.parent = parent;
    this.ownedSession = Boolean(session);
  }

  async abortOwnedSession() {
    if (!this.ownedSession) {
      return;
    }

    await this.session?.abortTransaction();
  }

  /**
   * Parameter: session
   *  if "keep":
   *    Keep the same session as the current one.
   *    If current context doesn't have a session, then no session is created.
   *
   *  if "new":
   *    Create a new session, regardless the existing parent session
   *
   *  if "ensure":
   *    Reuse the parent session if exists or create a new one
   *
   * @param sessionManagement
   */
  child(
    sessionManagement: 'keep' | 'new' | 'ensure' | 'none' = 'keep',
    extraLogTags?: log.Tag,
  ) {
    let session: ClientSession | undefined;

    switch (sessionManagement) {
      case 'keep':
        session = this.session;
        break;
      case 'new':
        session = this.client.startSession();
        break;
      case 'ensure':
        session = this.session ?? this.client.startSession();
        break;
      case 'none':
        session = undefined;
        break;
      default:
        throw new Error(`Unknown argument value for "session" parameter: "${sessionManagement as string}"`);
    }

    return new MongoDbContext({
      client: this.client,
      logger: this.logger.child(extraLogTags),
      parent: this,
      session,
    });
  }

  async commitOwnedSession() {
    if (!this.ownedSession) {
      return;
    }

    await this.session?.commitTransaction();
  }

  /**
   * Return a context that hold a session or the current one if it's already have one.
   * The goal is to ensure that all given transactions are performed within a session.
   */
  withinSession() {
    let child: MongoDbContext<T>;
    if (this.session) {
      child = new MongoDbContext<T>({
        client: this.client,
        logger: this.logger,
        parent: this,
      });
    } else {
      child = new MongoDbContext({
        client: this.client,
        logger: this.logger,
        parent: this,
        session: this.client.startSession(),
      });
    }

    return child;
  }

  static async create<T extends string>(
    config: utils.mongodb.ConnectInfo,
    logger: log.Logger<T>,
  ) {
    const client = utils.mongodb.createClient(config);

    await client.connect();

    return new MongoDbContext<T>({
      client,
      logger,
    });
  }
}

// ============================================================
// Exports
export default MongoDbContext;
