interface AppError<T extends string> extends Error {
  log(
    category: T,
    label?: string,
    message?: string,
  ): Promise<Error>;
}

export type {
  AppError,
};
