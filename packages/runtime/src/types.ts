import type { ChildProcessWithoutNullStreams } from 'child_process';
import type * as ExpressOpenApiValidator from 'express-openapi-validator';
import type { Logger } from './logger';

interface IRequest {
  readonly method: string,
}

interface IRuntime<
  LogCategory extends string,
> {
  readonly createContext: () => RuntimeContext<LogCategory>,

  readonly logger: Logger<LogCategory>;
  readonly meta: MetaInfo;
}

type RuntimeContext<
  LogCategory extends string,
> = {
  logger: Logger<LogCategory>
};

type MetaInfo = {
  service: {
    name: string,
    version: string,
  }
};

type ProcControl = {
  endPromise: Promise<void>
  kill: () => Promise<void>,
  stop: () => Promise<void>,
};

type ProcManager = {
  detectEndInitialization?: (proc: ChildProcessWithoutNullStreams) => Promise<void>,
  initializationTimeout?: number,
};

type ToolingRoutes = {
  dashboard: string,
  openapi: {
    json: string,
    ui: string,
    yaml: string,
  },
  taskAdmin: string,
};

type OpenApiValidatorError = InstanceType<typeof ExpressOpenApiValidator.error.BadRequest>
| InstanceType<typeof ExpressOpenApiValidator.error.Forbidden>
| InstanceType<typeof ExpressOpenApiValidator.error.InternalServerError>
| InstanceType<typeof ExpressOpenApiValidator.error.MethodNotAllowed>
| InstanceType<typeof ExpressOpenApiValidator.error.NotFound>
| InstanceType<typeof ExpressOpenApiValidator.error.RequestEntityTooLarge>
| InstanceType<typeof ExpressOpenApiValidator.error.Unauthorized>
| InstanceType<typeof ExpressOpenApiValidator.error.UnsupportedMediaType>;

// ============================================================
// Exports
export type {
  IRequest,
  IRuntime,
  MetaInfo,
  OpenApiValidatorError,
  ProcControl,
  ProcManager,
  RuntimeContext,
  ToolingRoutes,
};
