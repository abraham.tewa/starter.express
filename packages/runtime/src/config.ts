// ============================================================
// Imports
import { utils } from '@internal/helpers';
import Joi from 'joi';

// ============================================================
// Schema
const SessionConfigSchema = Joi.object<SessionConfig>({
  cookieName: Joi
    .string()
    .required(),
  maxAge: Joi
    .number()
    .integer()
    .positive(),
  secret: Joi
    .string()
    .required(),
  store: utils.redis.Schema.required(),
});

const TaskConfigSchema = Joi.object<TaskConfig>({
  store: utils.redis.Schema.required(),
});

// ============================================================
// Types
type SessionConfig = {
  cookieName: string,
  maxAge: number,
  secret: string,
  store: utils.redis.ConnectInfo,
};

type TaskConfig = {
  store: utils.redis.ConnectInfo,
};

// ============================================================
// Exports
export {
  SessionConfigSchema,
  TaskConfigSchema,
};

export type {
  SessionConfig,
  TaskConfig,
};
