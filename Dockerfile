FROM node:20-alpine AS build

# Installation
# Docker cache optimisation: by doing installation first, we ensure
# to always reuse the same cache if only source files have been updated
# and not the dependencies
WORKDIR /.build
COPY package.json .
COPY package-lock.json .

RUN npm ci

COPY . .

# Packaging and renaming the archive to package.tgz
RUN npm pack && \
    NAME=$(npm pack --unsafe-perm | tail -1) && \
    mv $NAME package.tgz

FROM node:20-alpine

WORKDIR /usr
COPY --from=build /.build/package.tgz /usr/package.tgz

RUN tar -xvzf package.tgz && \
    rm package.tgz && \
    mv package app && \
    cd app && \
    set ENV=production && \
    npm ci

WORKDIR /usr/app

# Configurationx
RUN mkdir /config && \
    ln -s /config ./config

ENV NODE_ENV=production

# Configuration directory
VOLUME /config

ENTRYPOINT ["/usr/local/bin/node", "usr/src/app/dist/cli.js"]
CMD ["start", "--port", "80", "--hostname", "0.0.0.0"]
EXPOSE 80
