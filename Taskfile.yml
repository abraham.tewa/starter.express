# https://taskfile.dev

version: '3'

silent: true

vars:
  mongodb_image: mongodb.starter
  mongodb_username: rootAdmin
  mongodb_password: rootPassword
  mongodb_database: starter
  mongodb_port: 27017
  mongodb_host: localhost
  mongodb_url: mongodb://localhost:27017/{{ .mongodb_database }}

tasks:
  build:
    cmds:
      - npm run build

  default:
    aliases:
      - "dev"
    desc: Start the development server
    cmds:
      - task: docker
      - "echo 'Started services:'"
      - "echo '    Mongo Express: http://localhost:8081'"
      - "echo '    RedisInsight: http://localhost:8001'"
      - npm run --silent dev
    silent: true

  docker:
    desc: Start docker
    cmds:
      - docker compose up -d

  clean:
    desc: Clean the project
    cmds:
      - task: clean:build
      - task: clean:db
      - task: clean:redis

  clean:build:
    desc: Clean build files
    cmds:
      - rm -rf packages/*/build/
      - rm -rf services/*/dist/

  clean:db:
    desc: Clean the databases
    cmds:
      - task: run_db_scripts
        vars:
          mode: cleanup

  codeclimate:
    summary: >
      Run Code Climate (https://codeclimate.com) command on the project.
      This will create an HTML at reports/codeclimate.html
    desc: Run Code Climate on the project
    env:
      OUTPUT_FILE: ./reports/codeclimate.html
    cmds:
      - codeclimate analyze --format html > $OUTPUT_FILE
      - echo $OUTPUT_FILE

  init:
    desc: Initialize project
    cmds:
      - task: init:db

  init:db:
    cmds:
      - task: run_db_scripts
        vars:
          mode: init

  init:service:
    desc: Initialize database with user sources
    env:
      NODE_ENV: development
      NODE_CONFIG_ENV: development
      APP_DEVTOOLS: "*"
    cmds:
      - npm run cli:dev -- user create admin@pupil-app.dev admin --admin
      - npm run cli:dev -- user create user@pupil-app.dev user --user

  import:
    desc: Import user data file
    cmds:
      - task: docker
      - npm run import -w service -- {{.CLI_ARGS}}

  lint:
    desc: Lint the project
    cmds:
      - npm run lint

  list:
    desc: List all tasks (shortcut for task --list-all)
    cmds:
      - task --list-all

  reset:
    desc: Clean the project
    cmds:
      - task: clean:docker
      - task: clean:build

  run_db_scripts:
    requires:
      vars:
        - mode
    cmds:
      - echo "Running \"service\" script..."
      - task: run_db_script_unique
        vars:
          mode: "{{ .mode }}"
          script: "/docker-entrypoint-initdb.d/service.js"

  run_db_script_unique:
    requires:
      vars:
        - script
        - mode
    cmds:
      - echo "Running script "{{ .script }}"..."
      - >
        docker exec -it {{ .mongodb_image }} mongosh \
          mongodb://{{ .mongodb_host }}:{{ .mongodb_port }}/admin \
          --username {{ .mongodb_username }} \
          --password {{ .mongodb_password }} \
          --quiet \
          --eval "mode='{{ .mode }}'; load('{{ .script }}')"

  test:build:
    env:
      TEST_RUN_TYPE: spawn:build
    cmds:
      - npm run build
      - npm run test:spawn:build

  validate:dev:
    desc: Validate the project
    aliases:
      - validate
      - val
    cmds:
      - npm run lint
      - npm run build
      - npm run validate:type
      - npm run test

  validate:full:
    desc: Ensure that all commands in the projects works properly
    cmds:
      - rm -rf ./reports
      - rm -rf ./dist
      # Linting the project
      - npm run lint
      - npm run validate:type

      # Running import tests before the build
      # (ensuring the perfect isolation with the build)
      - npm run test:import
      - npm run test

      # Building the project
      - rm -rf ./reports
      - npm run build
      - npm run test:spawn:build

      # Ensuring that nx commands works
      - rm -rf ./reports
      - rm -rf ./build
      - nx run-many -t lint --skipNxCache
      - nx run-many -t build --skipNxCache
      - nx run-many -t test --skipNxCache
