// ============================================================
// Imports
const { lazy } = require(__dirname + '/helpers.js');
const testUser = require(__dirname + '/test.js');

// ============================================================
// --eval variables
/**
 * @type {'cleanup' | 'init'}
 */
var mode;

// ============================================================
// Module's constants and variables
const dbName = 'starter_service';

const dbUsername = 'starter_service_dev_admin';
const dbPassword = 'starter_service_dev_password';

const serviceDb = db.getSiblingDB(dbName);

// ============================================================
// Main
switch(mode) {
  case 'cleanup':
    dropUsers();
    break;
  case 'init':
    createUsers();
    break;
  default:
    console.log(`Unknown mode: ${mode}`);
}

// ============================================================
// Functions
function createUsers() {
  console.log(`Creating user "${dbUsername}"`);
  serviceDb.createUser({
    user: dbUsername,
    pwd: dbPassword,
    roles: [
      { role: 'dbAdmin', db: dbName },
      { role: 'readWrite', db: dbName },
    ],
  });

  testUser.createUsers(db);
}

function dropUsers() {
  console.log(`Dropping user "${dbUsername}"`);
  lazy(() => serviceDb.dropUser(dbUsername));

  testUser.dropUsers(db);

  console.log(`Dropping db "${serviceDb._name}"`);
  lazy(() => serviceDb.dropDatabase());
}
