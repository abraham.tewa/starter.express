// ============================================================
// Imports
const { lazy } = require(__dirname + '/helpers.js');

// ============================================================
// --eval variables
var username;
var password;

/**
 * @type {'cleanup' | 'init'}
 */
var mode;

// ============================================================
// Module's constants and variables
const dbUsername = username ?? 'starter_service_test_admin';
const dbPassword = password ?? 'starter_service_test_password';

// ============================================================
// Main
switch(mode) {
  case 'cleanup':
    dropUsers();
    break;
  case 'init':
    createUsers();
    break;
  default:
    if (mode) {
      console.log(`Unknown mode: ${mode}`);
    }
}

// ============================================================
// Functions
function createUsers(dbClient = db) {
  console.log(`Creating user "${dbUsername}"`);
  dbClient.createUser({
    user: dbUsername,
    pwd: dbPassword,
    roles: [
      'readWriteAnyDatabase',
      'dbAdminAnyDatabase',
    ],
  });
}

function dropUsers(dbClient = db) {
  console.log(`Dropping user "${dbUsername}"`);
  lazy(() => dbClient.dropUser(dbUsername));
}

module.exports = {
  createUsers,
  dropUsers,
}
